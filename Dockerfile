FROM ubuntu:18.04
MAINTAINER Batunov Vyacheslav "mcgish@gmail.com"

RUN apt-get update -y && apt-get install -y python3.6 python3-pip python3-dev build-essential
ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8

COPY Pipfile Pipfile.lock ./
RUN pip3 install pipenv && pipenv install --system --deploy --ignore-pipfile

COPY . /app
WORKDIR /app/evema
RUN mkdir -p /app/logs/gunicorn/
CMD NEW_RELIC_CONFIG_FILE=newrelic.ini newrelic-admin run-program \
    gunicorn -b :8000 --threads 2 -w 10 evema.wsgi \
    --access-logfile /app/logs/gunicorn/access.log \
    --error-logfile /app/logs/gunicorn/errors.log
