from rest_framework import serializers

from universe.models import Region, Station, System


class RegionSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = Region


class StationSerializer(serializers.ModelSerializer):

    class Meta:
        exclude = ('access', )
        model = Station


class SystemSerializer(serializers.ModelSerializer):

    class Meta:
        model = System
        fields = '__all__'
