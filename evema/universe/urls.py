from django.urls import path

from universe.views import RegionDetail, RegionsList, StationDetail, StationsList, SystemDetail, SystemList

urlpatterns = [
    path('regions/', RegionsList.as_view(), name='regions'),
    path('regions/<int:pk>/', RegionDetail.as_view(), name='region'),
    path('stations/', StationsList.as_view(), name='stations'),
    path('stations/<int:pk>/', StationDetail.as_view(), name='station'),
    path('systems/', SystemList.as_view(), name='systems'),
    path('systems/<int:pk>/', SystemDetail.as_view(), name='system'),
]
