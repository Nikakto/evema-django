from django_filters.rest_framework import RangeFilter, FilterSet

from universe.models import System


class SystemFilter(FilterSet):

    security_status = RangeFilter()

    class Meta:
        exclude = ('stargates_ids', 'stations_ids', )
        model = System
