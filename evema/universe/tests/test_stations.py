from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from evema.utils.pagination import SmallResultsSetPagination
from universe.models import Region, Station, System


class TestStations(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',
        
        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    client = APIClient()
    url = reverse('universe:stations')

    def setUp(self):

        # in class django connected real database.
        self.pages_count = Station.objects.count() // SmallResultsSetPagination.page_size + 1

    # ==================================================================================================================
    # Main Case
    # ==================================================================================================================

    def test_stations__get__list_default(self):

        response = self.client.get(self.url)

        # check answer item
        station_answer = response.data[0]
        station_real = Station.objects.get(id=station_answer['id'])

        self.assertNotIn('access', station_answer)

        self.assertEqual(station_answer['name'], station_real.name)
        self.assertEqual(station_answer['item_type'], station_real.item_type_id)
        self.assertEqual(station_answer['region'], station_real.region_id)
        self.assertAlmostEqual(station_answer['structure'], station_real.structure)
        self.assertEqual(station_answer['system'], station_real.system_id)

        # check ids
        ids_answer = [Station['id'] for Station in response.data]
        self.assertEqual(len(set(ids_answer)), SmallResultsSetPagination.page_size)

        # check pagination
        self.assertEqual(response['x-page-current'], '1')
        self.assertEqual(response['x-pages'], str(self.pages_count))

    # ==================================================================================================================
    # Special cases
    # ==================================================================================================================

    def test_stations__get_page_last__list_page_last(self):

        data = {'page': self.pages_count}
        response = self.client.get(self.url, data)

        # check pagination
        self.assertEqual(response.data[-1]['id'], Station.objects.order_by('id').last().id)
        self.assertEquals(len(response.data), Station.objects.count() % SmallResultsSetPagination.page_size)

        # check pagination
        self.assertEqual(response['x-page-current'], str(self.pages_count))
        self.assertEqual(response['x-pages'], str(self.pages_count))

    def test_stations__get_page_invalid__404(self):

        data = {'page': self.pages_count + 1}
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_stations__get_page_size__list_all(self):

        data = {'page_size': 100}
        total_pages = Station.objects.count() // 100 + 1
        response = self.client.get(self.url, data)

        stations_ids_answer = [int(station['id']) for station in response.data]
        stations_real = list(Station.objects.filter(id__in=stations_ids_answer).values_list('id', flat=True))

        self.assertEqual(sorted(stations_real), sorted(stations_ids_answer))
        self.assertEqual(response['x-pages'], str(total_pages))

    def test_stations__get_search_special__get_valid_stations(self):

        search_word = 'search_012'

        data = {'page_size': 100, 'search': search_word}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        rank_prev = None
        for station in response.data:

            self.assertIn(search_word.lower(), station['name'].lower())

            if rank_prev:
                self.assertLessEqual(len(search_word) / len(station['name']), rank_prev)

            rank_prev = len(search_word) / len(station['name'])

    def test_stations__get_search_special_with_space__get_valid_stations(self):

        search_word = 'search me'

        data = {'page_size': 100, 'search': search_word}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        for Station in response.data:
            self.assertIn(search_word.lower(), Station['name'].lower())

    def test_stations__get_filtered_by_region__get_valid_Stations(self):

        region = Region.objects.first()

        data = {'page_size': 100, 'region': region.id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        for station in response.data:
            self.assertEqual(station['region'], region.id)

    def test_stations__get_filtered_by_system__get_valid_Stations(self):

        system = System.objects.first()

        data = {'page_size': 100, 'system': system.id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        for station in response.data:
            self.assertEqual(station['system'], system.id)

    def test_stations__get_filtered_structers_only__get_valid_Stations(self):

        data = {'page_size': 100, 'structure': True}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        for station in response.data:
            self.assertEqual(station['structure'], True)
