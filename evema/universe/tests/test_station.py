from django.db.models import Max
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from universe.models import Station


class TestStation(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    client = APIClient()

    def test_station__get_exist__station(self):

        station_expected = Station.objects.first()

        data = {'pk': station_expected.id}
        url = reverse('universe:station', kwargs=data)
        response = self.client.get(url)

        self.assertNotIn('access', response.data)

        self.assertEqual(response.data['name'], station_expected.name)
        self.assertEqual(response.data['item_type'], station_expected.item_type_id)
        self.assertEqual(response.data['region'], station_expected.region_id)
        self.assertAlmostEqual(response.data['structure'], station_expected.structure)
        self.assertEqual(response.data['system'], station_expected.system_id)

    def test_station__get_invalid__404(self):

        station_expected = Station.objects.all().aggregate(max_id=Max('id'))

        data = {'pk': station_expected['max_id'] + 1}
        url = reverse('universe:station', kwargs=data)
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
