from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from evema.utils.pagination import SmallResultsSetPagination
from universe.models import Region


class TestRegions(APITestCase):

    fixtures = ['regions']

    client = APIClient()
    url = reverse('universe:regions')

    # ==================================================================================================================
    # Main Case
    # ==================================================================================================================

    def test_regions__get__list_default(self):

        response = self.client.get(self.url)

        # check answer item
        region_answer = response.data[0]
        region_real = Region.objects.get(id=region_answer['id'])

        for key in region_answer:
            self.assertEqual(region_answer[key], getattr(region_real, key))

        # check ids
        ids_answer = [region['id'] for region in response.data]
        self.assertEqual(len(set(ids_answer)), SmallResultsSetPagination.page_size)

        # check pagination
        self.assertEqual(response['x-page-current'], '1')
        self.assertEqual(response['x-pages'], '2')

    # ==================================================================================================================
    # Special cases
    # ==================================================================================================================

    def test_regions__get_page_2__list_page_2(self):

        data = {'page': 2}
        response = self.client.get(self.url, data)

        # check pagination
        self.assertEqual(response.data[-1]['id'], Region.objects.last().id)
        self.assertEquals(len(response.data), 5)

        # check pagination
        self.assertEqual(response['x-page-current'], '2')
        self.assertEqual(response['x-pages'], '2')

    def test_regions__get_page_invalid__404(self):

        data = {'page': 3}
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_regions__get_page_size__list_all(self):

        data = {'page_size': 100}
        response = self.client.get(self.url, data)

        regions_ids_answer = [int(region['id']) for region in response.data]
        regions_real = list(Region.objects.filter(id__in=regions_ids_answer).order_by('id').values_list('id', flat=True))

        self.assertEqual(regions_real, regions_ids_answer, )
        self.assertEqual(response['x-pages'], '1')

    def test_regions__get_search_special__get_valid_regions(self):

        search_word = 'search_012'

        data = {'page_size': 100, 'search': search_word}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        rank_prev = None
        for region in response.data:

            self.assertIn(search_word.lower(), region['name'].lower())

            if rank_prev:
                self.assertLessEqual(len(search_word) / len(region['name']), rank_prev)

            rank_prev = len(search_word) / len(region['name'])

    def test_regions__get_search_special_with_space__get_valid_regions(self):

        search_word = 'search me'

        data = {'page_size': 100, 'search': search_word}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        for region in response.data:
            self.assertIn(search_word.lower(), region['name'].lower())
