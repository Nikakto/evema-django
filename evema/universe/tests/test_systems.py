from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from evema.utils.pagination import SmallResultsSetPagination
from universe.models import Region, System


class TestSystems(APITestCase):

    fixtures = [
        'regions',
        'constellations',
        'systems'
    ]

    client = APIClient()
    url = reverse('universe:systems')

    def setUp(self):

        # in class django connected real database.
        self.pages_count = System.objects.count() // SmallResultsSetPagination.page_size + 1

    # ==================================================================================================================
    # Main Case
    # ==================================================================================================================

    def test_systems__get__list_default(self):

        response = self.client.get(self.url)

        # check answer item
        system_answer = response.data[0]
        system_real = System.objects.get(id=system_answer['id'])

        self.assertEqual(system_answer['name'], system_real.name)
        self.assertEqual(system_answer['constellation'], system_real.constellation_id)
        self.assertEqual(system_answer['region'], system_real.region_id)
        self.assertAlmostEqual(float(system_answer['security_status']), float(system_real.security_status), 1)
        self.assertEqual(system_answer['security_class'], system_real.security_class)
        self.assertListEqual(system_answer['stargates_ids'], system_real.stargates_ids)
        self.assertListEqual(system_answer['stations_ids'], system_real.stations_ids)

        # check ids
        ids_answer = [system['id'] for system in response.data]
        self.assertEqual(len(set(ids_answer)), SmallResultsSetPagination.page_size)

        # check pagination
        self.assertEqual(response['x-page-current'], '1')
        self.assertEqual(response['x-pages'], str(self.pages_count))

    # ==================================================================================================================
    # Special cases
    # ==================================================================================================================

    def test_systems__get_page_last__list_page_last(self):

        data = {'page': self.pages_count}
        response = self.client.get(self.url, data)

        # check pagination
        self.assertEqual(response.data[-1]['id'], System.objects.last().id)
        self.assertEquals(len(response.data), System.objects.count() % SmallResultsSetPagination.page_size)

        # check pagination
        self.assertEqual(response['x-page-current'], str(self.pages_count))
        self.assertEqual(response['x-pages'], str(self.pages_count))

    def test_systems__get_page_invalid__404(self):

        data = {'page': self.pages_count + 1}
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_systems__get_page_size__list_all(self):

        data = {'page_size': 100}
        total_pages = System.objects.count() // 100 + 1
        response = self.client.get(self.url, data)

        systems_ids_answer = [int(system['id']) for system in response.data]
        systems_real = list(System.objects.filter(id__in=systems_ids_answer).order_by('id').values_list('id', flat=True))

        self.assertListEqual(systems_ids_answer, systems_real)
        self.assertEqual(response['x-pages'], str(total_pages))

    def test_systems__get_search_special__get_valid_systems(self):

        search_word = 'search_012'

        data = {'page_size': 100, 'search': search_word}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        rank_prev = None
        for station in response.data:

            self.assertIn(search_word.lower(), station['name'].lower())

            if rank_prev:
                self.assertLessEqual(len(search_word) / len(station['name']), rank_prev)

            rank_prev = len(search_word) / len(station['name'])

    def test_systems__get_search_special_with_space__get_valid_systems(self):

        search_word = 'search me'

        data = {'page_size': 100, 'search': search_word}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        for system in response.data:
            self.assertIn(search_word.lower(), system['name'].lower())

    def test_systems__get_filtered_by_region__get_valid_systems(self):

        region = Region.objects.first()

        data = {'page_size': 100, 'region': region.id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        for system in response.data:
            self.assertEqual(system['region'], region.id)

    def test_systems__get_filtered_by_max_min_security__get_valid_systems(self):

        security_min = -0.2
        security_max = 0.5

        data = {'page_size': 100, 'security_status_max': security_max, 'security_status_min': security_min}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        for system in response.data:
            self.assertGreaterEqual(float(system['security_status']), security_min)
            self.assertLessEqual(float(system['security_status']), security_max)
