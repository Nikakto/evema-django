from django.db.models import Max
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from universe.models import Region


class TestRegion(APITestCase):

    fixtures = ['regions']

    client = APIClient()

    def test_region__get_exist__region(self):

        region_expected = Region.objects.first()

        data = {'pk': region_expected.id}
        url = reverse('universe:region', kwargs=data)
        response = self.client.get(url)

        self.assertEqual(response.data['id'], region_expected.id)
        self.assertEqual(response.data['description'], region_expected.description)
        self.assertEqual(response.data['name'], region_expected.name)

    def test_region__get_invalid__404(self):

        region_expected = Region.objects.all().aggregate(max_id=Max('id'))

        data = {'pk': region_expected['max_id'] + 1}
        url = reverse('universe:region', kwargs=data)
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
