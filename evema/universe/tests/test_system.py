from django.db.models import Max
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from universe.models import System


class TestSystem(APITestCase):

    fixtures = [
        'regions',
        'constellations',
        'systems'
    ]

    client = APIClient()

    def test_system__get_exist__system(self):

        system_expected = System.objects.first()

        data = {'pk': system_expected.id}
        url = reverse('universe:system', kwargs=data)
        response = self.client.get(url)

        self.assertEqual(response.data['id'], system_expected.id)
        self.assertEqual(response.data['name'], system_expected.name)
        self.assertEqual(response.data['region'], system_expected.region_id)
        self.assertAlmostEqual(float(response.data['security_status']), system_expected.security_status, 1)
        self.assertEqual(response.data['security_class'], system_expected.security_class)
        self.assertEqual(response.data['stargates_ids'], system_expected.stargates_ids)
        self.assertEqual(response.data['stations_ids'], system_expected.stations_ids)

    def test_system__get_invalid__404(self):
        system_expected = System.objects.all().aggregate(max_id=Max('id'))

        data = {'pk': system_expected['max_id'] + 1}
        url = reverse('universe:system', kwargs=data)
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
