from django.db.models import ExpressionWrapper, FloatField
from django.db.models.functions import Length
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView, RetrieveAPIView

from universe.filters import SystemFilter
from universe.models import Region, Station, System
from universe.serializers import RegionSerializer, StationSerializer, SystemSerializer
from evema.utils.pagination import SmallResultsSetPagination


class RegionDetail(RetrieveAPIView):

    queryset = Region.objects.all()
    serializer_class = RegionSerializer


class RegionsList(ListAPIView):

    pagination_class = SmallResultsSetPagination
    serializer_class = RegionSerializer

    def get_queryset(self):

        queryset = Region.objects.order_by('id').all()

        search = self.request.query_params.get('search')
        if not search:
            return queryset

        search_rank_expression = ExpressionWrapper(float(len(search)) / Length('name'), output_field=FloatField())
        return queryset.annotate(rank=search_rank_expression).filter(rank__gt=0.20, name__icontains=search).order_by('-rank')


class StationDetail(RetrieveAPIView):

    queryset = Station.objects.all()
    serializer_class = StationSerializer


class StationsList(ListAPIView):

    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('region', 'structure', 'system')
    pagination_class = SmallResultsSetPagination
    serializer_class = StationSerializer

    def get_queryset(self):

        queryset = Station.objects.order_by('id').all()

        search = self.request.query_params.get('search')
        if not search:
            return queryset

        search_rank_expression = ExpressionWrapper(float(len(search)) / Length('name'), output_field=FloatField())
        return queryset.annotate(rank=search_rank_expression).filter(rank__gt=0.05, name__icontains=search).order_by('-rank')


class SystemDetail(RetrieveAPIView):

    queryset = System.objects.all()
    serializer_class = SystemSerializer


class SystemList(ListAPIView):

    filter_backends = (DjangoFilterBackend, )
    filterset_class = SystemFilter
    pagination_class = SmallResultsSetPagination
    serializer_class = SystemSerializer

    def get_queryset(self):

        queryset = System.objects.order_by('id').all()

        search = self.request.query_params.get('search')
        if not search:
            return queryset

        search_rank_expression = ExpressionWrapper(float(len(search)) / Length('name'), output_field=FloatField())
        return queryset.annotate(rank=search_rank_expression).filter(rank__gt=0.10, name__icontains=search).order_by('-rank')
