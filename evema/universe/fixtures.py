# function as this file should be called in evema.managment.fixtures command
# functions generating fixtures of models

from evema.utils.fixtures import make_fixture
from universe.models import Region, Constellation, Station, System


def generate_regions():

    # ==============================================================================================================
    # Regions
    # ==============================================================================================================

    # main regions
    region_description = 'region {id} for test'
    region_name = 'region_{id}'
    regions = [Region(id=i, description=region_description.format(id=i), name=region_name.format(id=i))
               for i in range(30)]

    # stations for search ranks
    regions_search_ranked = regions[-10:]
    for index, region in enumerate(regions_search_ranked):
        postfix = ''.join(str(i) for i in range(index))
        region.name = f'search_{postfix}'

    # stations for search with space
    region = regions[-1]
    region.name = 'SEARCH ME PLEASE WITH SPACE'

    path = './universe/fixtures/regions.json'
    make_fixture(regions, path)

    return regions


def generate_constellations(regions):

    constellation_name = 'constellation_{id}'

    constellation_id = 1
    constellations = []
    for region in regions:
        region_constellations = [
            Constellation(
                id=constellation_id,
                region=region,
                name=constellation_name.format(id=constellation_id)
            )
            for i in range(2)
        ]

        constellations.extend(region_constellations)
        constellation_id += 1

    path = './universe/fixtures/constellations.json'
    make_fixture(constellations, path)

    return constellations


def generate_systems(constellations):

    system_name = 'system_{id}'

    system_id = 1
    systems = []
    for constellation in constellations:

        for i in range(2):
            system_data = {
                'id': system_id,
                'name': system_name.format(id=system_id),
                'constellation_id': constellation.id,
                'region_id': constellation.region_id,
                'security_status': -1 + (constellation.region_id / 10 % 2),
                'stargates_ids': [constellation.region_id * 10000 + i for i in range(constellation.region_id % 5)],
                'stations_ids': [constellation.region_id * 10000000 + i for i in range(constellation.region_id % 5)],
            }

            systems.append(System(**system_data))
            system_id += 1

    # systems for search ranks
    systems_search_ranked = systems[-10:-3]
    for index, system in enumerate(systems_search_ranked):
        postfix = ''.join(str(i) for i in range(index))
        system.name =  'search_' + postfix

    # systems for search
    system = systems[-1]
    system.name = 'SEARCH ME PLEASE WITH SPACE'

    path = './universe/fixtures/systems.json'
    make_fixture(systems, path)

    return systems


def generate_stations(systems, item_type):

    def _station(system, station_id):

        return Station(
            id=station_id,
            access=is_citadel_and_access,
            name=f'station_{station_id}',
            item_type=item_type,
            region_id=system.region_id,
            structure=is_citadel,
            system=system,
        )

    station_id = 1
    stations = []
    for system in systems:

        # every odd station is citadel
        is_citadel = system.id % 2

        # every second odd station - citadel with access
        is_citadel_and_access = system.id // 2 % 2 if is_citadel else False

        stations.append(_station(system, station_id))
        station_id += 1

    # we need system with two instance of Station
    stations.append(_station(systems[0], station_id))
    station_id += 1

    # stations for search ranks
    stations_search_ranked = stations[-10:]
    for index, station in enumerate(stations_search_ranked):
        postfix = ''.join(str(i) for i in range(index))
        station.name = f'search_{postfix}'

    # stations for search with space
    station = stations[-1]
    station.name = 'SEARCH ME PLEASE WITH SPACE'

    path = './universe/fixtures/stations.json'
    make_fixture(stations, path)

    return stations
