from django.contrib.postgres.fields import ArrayField
from django.db import models


class Constellation(models.Model):

    id = models.BigAutoField(primary_key=True)

    name = models.CharField(max_length=32)
    region = models.ForeignKey('Region', on_delete=models.CASCADE, related_name='constellations')


class Region(models.Model):
    
    id = models.BigAutoField(primary_key=True)

    description = models.TextField()
    name = models.CharField(max_length=32)


class Station(models.Model):

    id = models.BigAutoField(primary_key=True)

    access = models.BooleanField(default=False)
    name = models.CharField(max_length=128)
    item_type = models.ForeignKey('assets.ItemType', on_delete=models.CASCADE)
    max_dockable_ship_volume = models.DecimalField(decimal_places=2, max_digits=32, null=True)
    office_rental_cost = models.DecimalField(decimal_places=2, max_digits=32, null=True)
    region = models.ForeignKey('Region', on_delete=models.CASCADE, related_name='stations')
    services = ArrayField(models.CharField(max_length=32), null=True)
    structure = models.BooleanField(default=False)
    system = models.ForeignKey('System', on_delete=models.CASCADE, related_name='stations')
    reprocessing_efficiency = models.DecimalField(decimal_places=2, max_digits=5, null=True)
    reprocessing_stations_take = models.DecimalField(decimal_places=2, max_digits=5, null=True)

    @property
    def image_url(self):
        return f'https://image.eveonline.com/Type/{self.id}_64.png'


class System(models.Model):
    
    id = models.BigAutoField(primary_key=True)

    name = models.CharField(max_length=32)
    constellation = models.ForeignKey('Constellation', on_delete=models.CASCADE, related_name='systems')
    region = models.ForeignKey('Region', on_delete=models.CASCADE, related_name='systems')
    security_status = models.DecimalField(decimal_places=1, max_digits=2, null=True)
    security_class = models.CharField(max_length=4)
    stargates_ids = ArrayField(models.BigIntegerField(), default=list)
    stations_ids = ArrayField(models.BigIntegerField(), default=list)
