import os

# ======================================================================================================================
# MAIN_APP
# ======================================================================================================================

DEBUG = os.environ.get('DEBUG', 'True').lower() == 'true'
TESTING = os.environ.get('TESTING', 'False').lower() == 'true'


# ======================================================================================================================
# EVE OAUTH
# ======================================================================================================================

env_error = 'Enviroment variable {var} not found'

EVE_OAUTH_CLIENT_ID = os.environ.get('EVE_OAUTH_CLIENT_ID', '')
EVE_OAUTH_SECRET_KEY = os.environ.get('EVE_OAUTH_SECRET_KEY', '')

# main settings
EVE_OAUTH_LOGIN_URL = os.environ.get('EVE_OAUTH_LOGIN_URL', 'https://login.eveonline.com/oauth/authorize/')
EVE_OAUTH_REDIRECT_URI = os.environ.get('EVE_OAUTH_REDIRECT_URI', f'http://127.0.0.1:8080/login/')
EVE_OAUTH_RESPONSE_TYPE = 'code'

# URL for tokens
EVE_OAUTH_REVOKE_URL = os.environ.get('EVE_OAUTH_REVOKE_URL', 'https://login.eveonline.com/oauth/revoke')
EVE_OAUTH_TOKEN_URL = os.environ.get('EVE_OAUTH_TOKEN_URL', 'https://login.eveonline.com/oauth/token')
EVE_OAUTH_VERIFY_URL = os.environ.get('EVE_OAUTH_VERIFY_URL', 'https://login.eveonline.com/oauth/verify')


# ======================================================================================================================
# EVE ESI API
# ======================================================================================================================

ESI_API_BASE_URL = 'https://esi.tech.ccp.is/latest'
ESI_API_ERRORS_LIMIT = 50

ESI_API_ENDPOINTS = {

    'ALLIANCES': {
        'ALLIANCE': ESI_API_BASE_URL + '/alliances/{alliance_id}/',
    },

    'CHARACTER': {
        'CHARACTER': ESI_API_BASE_URL + '/characters/{character_id}/',
        'PORTRAIT': ESI_API_BASE_URL + '/characters/{character_id}/portrait/',
    },

    'CORPORATION': {
        'CORPORATION': ESI_API_BASE_URL + '/corporations/{corporation_id}/',
        'ICONS': ESI_API_BASE_URL + '/corporations/{corporation_id}/icons/',
        'NAMES': ESI_API_BASE_URL + '/corporations/names/',
    },

    'LOCATION': {
        'LOCATION': ESI_API_BASE_URL + '/characters/{character_id}/location/',
        'ONLINE': ESI_API_BASE_URL + '/characters/{character_id}/online/',
    },

    'MARKET': {
        'CHARACTER_ORDERS': ESI_API_BASE_URL + '/characters/{character_id}/orders/',
        'CORPORATION_ORDERS': ESI_API_BASE_URL + '/corporations/{corporation_id}/orders/',
        'MARKET_GROUP': ESI_API_BASE_URL + '/markets/groups/{market_group_id}/',
        'MARKET_GROUPS': ESI_API_BASE_URL + '/markets/groups/',
        'ORDERS': ESI_API_BASE_URL + '/markets/{region_id}/orders/',
        'STRUCTURES': ESI_API_BASE_URL + '/markets/structures/{structure_id}/',
    },

    'META': {
        'VERIFY': 'https://esi.tech.ccp.is/verify/',
    },

    'UNIVERSE': {
        'CONSTELLATION': ESI_API_BASE_URL + '/universe/constellations/{constellation_id}/',
        'CONSTELLATIONS': ESI_API_BASE_URL + '/universe/constellations/',
        'CATEGORY': ESI_API_BASE_URL + '/universe/categories/{category_id}/',
        'CATEGORIES': ESI_API_BASE_URL + '/universe/categories/',
        'GROUP': ESI_API_BASE_URL + '/universe/groups/{group_id}/',
        'GROUPS': ESI_API_BASE_URL + '/universe/groups/',
        'REGION': ESI_API_BASE_URL + '/universe/regions/{region_id}/',
        'REGIONS': ESI_API_BASE_URL + '/universe/regions/',
        'STARGATE': ESI_API_BASE_URL + '/universe/stargates/{stargate_id}/',
        'STATION': ESI_API_BASE_URL + '/universe/stations/{station_id}/',
        'STRUCTURE': ESI_API_BASE_URL + '/universe/structures/{structure_id}/',
        'STRUCTURES': ESI_API_BASE_URL + '/universe/structures/',
        'SYSTEM': ESI_API_BASE_URL + '/universe/systems/{system_id}/',
        'SYSTEMS': ESI_API_BASE_URL + '/universe/systems/',
        'TYPE': ESI_API_BASE_URL + '/universe/types/{type_id}/',
        'TYPES': ESI_API_BASE_URL + '/universe/types/',
    },

    'WALLET': {
        'WALLET': ESI_API_BASE_URL + '/characters/{character_id}/wallet/',
    }
}

# ======================================================================================================================
# OTHER
# ======================================================================================================================

OTHER_LOG_FOLDER = os.environ.get('OTHER_LOG_FOLDER', '../logs')

OTHER_REQUEST_THREADS = int(os.environ.get('OTHER_REQUEST_THREADS', '25'))
OTHER_RETRY_LIMIT = int(os.environ.get('OTHER_RETRY_LIMIT', '5'))
