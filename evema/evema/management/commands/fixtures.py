from django.core.management.base import BaseCommand

from assets.fixtures import generate_item_categories, generate_item_groups, generate_item_types
from market.fixtures import generate_market_groups, generate_market_orders
from universe.fixtures import generate_regions, generate_constellations, generate_stations, generate_systems


class Command(BaseCommand):

    help = 'Generate fixtures for whole project'

    def handle(self, *args, **options):

        regions = generate_regions()
        constellations = generate_constellations(regions)
        systems = generate_systems(constellations)

        market_groups = generate_market_groups()

        item_categories = generate_item_categories()
        item_groups = generate_item_groups(item_categories)
        item_types = generate_item_types(item_groups, market_groups)

        stations = generate_stations(systems, item_types[0])

        orders = generate_market_orders(stations, item_types)
