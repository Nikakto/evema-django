from django.core.management.base import BaseCommand
from django.core.cache import caches


class Command(BaseCommand):

    def handle(self, *args, **kwargs):

        for cache in caches.all():
            cache.clear()

        self.stdout.write('Cleared cache\n')
