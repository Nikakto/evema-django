import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'evema.settings')

app = Celery('evema')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


# @app.task
# def test(arg):
#     print(arg)
#     from character.tasks import characters_update_from_esiapi
#     characters_update_from_esiapi(arg)
#
#
# @app.on_after_configure.connect
# def setup_periodic_tasks(sender, **kwargs):
#     # Calls test('hello') every 10 seconds.
#     sender.add_periodic_task(10, test.s('hello'), name='add every 10')
