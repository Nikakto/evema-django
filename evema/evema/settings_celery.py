import datetime
import os
from kombu import Exchange, Queue

# defined at setting.py for cache
REDIS_HOST = os.environ.get('REDIS_HOST', '127.0.0.1')
REDIS_PORT = os.environ.get('REDIS_PORT', '6379')


CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_BROKER_URL = f"redis://{REDIS_HOST}:{REDIS_PORT}/"

CELERY_RESULT_BACKEND = f"redis://{REDIS_HOST}:{REDIS_PORT}/"
CELERY_RESULT_SERIALIZER = 'json'

CELERY_TASK_ALWAYS_EAGER = os.environ.get('TESTING', 'False').lower() == 'true'
CELERY_TASK_EAGER_PROPAGATES = os.environ.get('TESTING', 'False').lower() == 'true'


CELERY_EAGER_PROPAGATES_EXCEPTIONS = os.environ.get('TESTING', 'False').lower() == 'true'


CELERY_TASK_IGNORE_RESULT = True
CELERY_TASK_SERIALIZER = 'json'
CELERYD_TASK_SOFT_TIME_LIMIT = 300
CELERYD_MAX_TASKS_PER_CHILD = 20


CELERY_DEFAULT_QUEUE = 'default'
CELERY_DEFAULT_EXCHANGE_TYPE = 'default'
CELERY_DEFAULT_ROUTING_KEY = 'default'


CELERY_BEAT_SCHEDULE = {

    # -- CHARACTER -- #

    'characters_update_from_esiapi': {
        'task': 'character.tasks.characters_update_from_esiapi',
        'schedule': datetime.timedelta(hours=1),
    },

    # -- CORPORATION -- #

    'alliances_update_from_esiapi': {
        'task': 'corporation.tasks.alliances_update_from_esiapi',
        'schedule': datetime.timedelta(hours=1),
    },

    'corporations_update_from_esiapi': {
        'task': 'corporation.tasks.corporations_update_from_esiapi',
        'schedule': datetime.timedelta(hours=1),
    },

    # -- MARKET --#

    'market_orders_characters': {
        'task': 'market.tasks.market_orders_characters',
        'schedule': datetime.timedelta(hours=1),
    },

    'market_orders_clean': {
        'task': 'market.tasks.market_orders_clean',
        'schedule': datetime.timedelta(minutes=15),
    },

    'market_orders_corporations': {
        'task': 'market.tasks.market_orders_corporations',
        'schedule': datetime.timedelta(hours=1),
    },

    'market_orders_regions': {
        'task': 'market.tasks.market_orders_regions',
        'schedule': datetime.timedelta(minutes=15),
    },

}


CELERY_TASK_ROUTES = {

    # -- CHARACTER -- #

    'character.tasks.character_update_from_esiapi': {
        'queue': 'harvest'
    },

    'character.tasks.characters_update_from_esiapi': {
        'queue': 'harvest',
    },

    # -- CORPORATION -- #

    'corporation.tasks.alliance_update_from_esiapi': {
        'queue': 'harvest'
    },

    'corporation.tasks.alliances_update_from_esiapi': {
        'queue': 'harvest',
    },

    'corporation.tasks.corporation_update_from_esiapi': {
        'queue': 'harvest'
    },

    'corporation.tasks.corporations_update_from_esiapi': {
        'queue': 'harvest',
    },

    # -- MARKET --#

    'market.tasks.market_orders_character': {
        'queue': 'orders',
    },

    'market.tasks.market_orders_characters': {
        'queue': 'orders',
    },

    'market.tasks.market_orders_clean': {
        'queue': 'orders',
    },

    'market.tasks.market_orders_corporation': {
        'queue': 'orders',
    },

    'market.tasks.market_orders_corporations': {
        'queue': 'orders',
    },

    'market.tasks.market_orders_region': {
        'queue': 'orders',
    },

    'market.tasks.market_orders_regions': {
        'queue': 'orders',
    },

}


CELERY_QUEUES = (
    Queue('default', exchange=Exchange('default'), routing_key='default'),
    Queue('orders', exchange=Exchange('orders'), routing_key='orders'),
    Queue('harvest', exchange=Exchange('harvest'), routing_key='harvest'),
)
