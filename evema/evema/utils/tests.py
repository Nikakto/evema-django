from django.contrib.auth.models import User
from django.utils import timezone

from rest_framework.authtoken.models import Token

from character.models import Character, CharacterToken, CharacterTokenScope
from corporation.models import Alliance, Corporation
from market.models import MarketOrder
from universe.models import Station


def alliance_create(alliance_id=0, creator_corporation_id=0, creator_id=0, name='test_alliance'):
    """
    In tests you don't want to write this every time. Use this function to create one alliance
    :param alliance_id: id of alliance
    :param creator_corporation_id: id of creator corporation
    :param creator_id: character creator id
    :param name: name of alliance
    :return: corporation.models.Alliance
    """

    alliance = Alliance.objects.create(
        id=alliance_id,
        creator_corporation_id=creator_corporation_id,
        creator_id=creator_id,
        name=name,
        ticker="AL",
    )

    return alliance


def character_create(character_id=None, character_name='test_character', username='test_username'):
    """
    Create user, user.auth_token, character and character token.
    :return: User, Token, Character, CharacterToken
    """

    user = User.objects.create(username=username)
    token = Token.objects.create(user=user)

    character = Character.objects.create(
        id=character_id,
        ancestry_id=0,
        birthday=timezone.now(),
        bloodline_id=0,
        corporation_id=0,
        gender='male',
        name=character_name,
        race_id=0,
        security_status=0,
        user=user,
    )

    character_token = CharacterToken.objects.create(
        access_token='test_token',
        character=character,
        refresh_token='test_token',
        scopes=list(CharacterTokenScope.objects.all().values_list('scope', flat=True)),
        token_type='test_token'
    )

    character.token = character_token
    character.save()

    user.save()

    return user, token, character, character_token


def character_remove(user, token, character, character_token):
    """
    Delete user, user.auth_token, character and character token.
    :return: User, Token, Character, CharacterToken
    """

    if character_token.id:
        character_token.delete()

    if character.id:
        character.delete()

    if token.key:
        token.delete()

    if user.id:
        user.delete()


def corporation_create(ceo_id=0, corporation_id=None, creator_id=0, name='default_name'):
    """
    In tests you don't want to write this every time. Use this function to create one corporation
    :param ceo_id: id of ceo_id
    :param corporation_id: set corporation id
    :param creator_id: id of creator_id
    :param name: name of corporation
    :return: corporation.models.Corporation
    """

    kwargs = dict(
        ceo_id=ceo_id,
        creator_id=creator_id,
        home_station_id=2,
        member_count=0,
        name=name,
        shares=1,
        tax_rate=1,
        ticker="N"
    )

    if corporation_id is not None:
        kwargs['id'] = corporation_id

    corporation = Corporation.objects.create(**kwargs)

    return corporation


def market_order_create(**kwargs):

    duration = kwargs.get('duration') or 90
    station = Station.objects.first()

    properties = {
        'corporation_id': kwargs.get('corporation_id') or None,
        'duration': duration,
        'expires_date': timezone.now().replace(microsecond=0) + timezone.timedelta(days=duration),
        'is_buy_order': True,
        'issued': timezone.now().replace(microsecond=0),
        'item_type_id': 10,
        'min_volume': 100,
        'price': 10000,
        'public': False,
        'range': 'region',
        'region_id': kwargs.get('region_id') or station and station.region_id or 1,
        'station_id': kwargs.get('station_id') or station and station.id or 1,
        'system_id': kwargs.get('system_id') or station and station.system_id or 1,
        'updated_at': kwargs.get('updated_at') or timezone.now() - timezone.timedelta(days=1),
        'volume_total': 777,
        'volume_remain': 666,
    }

    properties.update(kwargs)

    if kwargs.get('id'):
        properties['id'] = kwargs['id']

    return MarketOrder.objects.create(**properties)
