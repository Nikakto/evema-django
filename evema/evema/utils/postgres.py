from django.db.models import ExpressionWrapper, FloatField, QuerySet
from django.db.models.functions import Length
from django.contrib.postgres.search import SearchRank, SearchQuery, SearchVector

import re


def search_filter(queryset: QuerySet, field: str, search: str):
    """
    Temporary function to make search query to database
    :param queryset: to this queryset will be apply search filter
    :param field: field to search in
    :param search: word or phrase to search
    :return: queryset with ordering by rank
    """

    # regex = re.compile('[^a-zA-Z\s\d\-]')
    # search_string = ' <-> '.join(regex.sub('', word) for word in search.split())
    # search_query = SearchQuery(f'{search_string}:*', config='english', search_type='raw')
    #
    # search_vector = SearchVector(field, config='english', weight='A')
    #
    # search_rank = SearchRank(search_vector, search_query)
    # search_rank_expression = ExpressionWrapper(search_rank / Length('name'), output_field=FloatField())
    #
    # return queryset.annotate(rank=search_rank_expression).filter(rank__gt=0.01).order_by('-rank')

    search_rank_expression = ExpressionWrapper(float(len(search)) / Length('name'), output_field=FloatField())
    return queryset.annotate(rank=search_rank_expression).filter(rank__gt=0.10, name__icontains=search).order_by('-rank')