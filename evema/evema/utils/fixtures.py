from django.core import serializers


def make_fixture(itterable, path):

    json_data = serializers.serialize('json', itterable)

    with open(path, 'w') as f:
        f.write(json_data)
