from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


# ======================================================================================================================
# Main Class: page_size from settings ('PAGE_SIZE': 100)
# ======================================================================================================================

class MainPagination(PageNumberPagination):

    def get_paginated_response(self, data):

        response = Response(data)

        response['X-Data-Count'] = self.page.paginator.count
        response['X-Page-Current'] = self.page.number
        response['X-Pages'] = self.page.paginator.num_pages

        if self.page.has_next():
            response['X-Page-Next'] = self.page.next_page_number()

        if self.page.has_previous():
            response['X-Page-Previous'] = self.page.previous_page_number()

        return response


# ======================================================================================================================
# Pagination variants
# ======================================================================================================================


class SmallResultsSetPagination(MainPagination):
    page_size = 25
    page_size_query_param = 'page_size'
    max_page_size = 100


class LargeResultsSetPagination(MainPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 1000
