from django.contrib.auth.models import User
from django.db import transaction

from character.models import Character, CharacterToken
from character.tasks import character_update_from_esiapi
from esiapi.auth import obtain_character


class UserProxy(User):

    class Meta:
        proxy = True

    @classmethod
    def get_by_token_data(cls, token_data):

        character_token = obtain_character(token_data['access_token'])
        if not character_token:
            return None

        user = cls.get_by_character_token(character_token)
        if not user:
            return None

        if not UserProxy.update_character_token(user, token_data):
            return None

        return User.objects.get(id=user.id)

    @classmethod
    def get_by_character_token(cls, character_token):

        character_id, character_name = character_token['CharacterID'], character_token['CharacterName']

        character = \
            Character.objects.filter(id=character_id).first() or \
            character_update_from_esiapi(character_id, raise_error=False, retry=False)

        if not character:
            return None

        if not character.user:

            user, created = User.objects.get_or_create(username=character_name)

            character.user = user
            character.save()

        return cls.objects.get(character=character)

    @staticmethod
    def update_character_token(user, token_data):

        token = user.character.token or CharacterToken(character=user.character)
        token.update_from_json(token_data)

        token.scopes = token.scopes_verified
        if token.scopes is None:
            return None

        token.save()

        user.character.token = token
        user.character.save()

        return token
