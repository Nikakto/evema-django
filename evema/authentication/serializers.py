from rest_framework import serializers

from character.serializers import CharacterSerializer


class UserSerializer(serializers.Serializer):

    character = CharacterSerializer(read_only=True)
    token = serializers.CharField(source='auth_token')
