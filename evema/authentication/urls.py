from django.urls import path

from authentication.views.auth_info import AuthInfo
from authentication.views.auth_link import AuthLink
from authentication.views.auth_login import AuthLogin
from authentication.views.auth_logout import AuthLogout

urlpatterns = [
    path('info/', AuthInfo.as_view(), name='info'),
    path('link/', AuthLink.as_view(), name='link'),
    path('login/', AuthLogin.as_view(), name='login'),
    path('logout/', AuthLogout.as_view(), name='logout'),
]
