from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from character.serializers import CharacterSerializer


class AuthInfo(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):

        serializer = CharacterSerializer(request.user.character)
        return Response({'data': serializer.data})
