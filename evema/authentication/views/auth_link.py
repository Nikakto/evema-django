from rest_framework.response import Response
from rest_framework.views import APIView

import datetime

from esiapi.auth import auth_link


class AuthLink(APIView):

    def get(self, request, *args, **kwargs):

        result = {
            'created_at': datetime.datetime.now(),
            'link': auth_link(),
        }

        return Response(result)
