from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.response import Response
from rest_framework.views import APIView

import logging

from authentication.models import UserProxy
from authentication.serializers import UserSerializer
from corporation.models import Corporation
from esiapi import harvest
from esiapi.auth import verify_token

logger = logging.getLogger(__name__)


class AuthLogin(APIView):

    def post(self, request, *args, **kwargs):

        code = request.data.get('code')
        if not code:
            raise AuthenticationFailed()

        token_data = verify_token(code)
        if not token_data:
            raise AuthenticationFailed()

        user = UserProxy.get_by_token_data(token_data)
        if not user:
            raise AuthenticationFailed()

        orders_loaded = harvest.orders.harvest_character(character_id=user.character.id)
        if orders_loaded and 'error' in orders_loaded:
            raise AuthenticationFailed()

        corporation = Corporation.objects.filter(id=user.character.corporation_id).first() or Corporation(id=user.character.corporation_id)
        corporation_updated = corporation.update_from_esiapi()
        if not corporation_updated:
            raise AuthenticationFailed()

        Token.objects.get_or_create(user=user)

        serializer = UserSerializer(user)
        response = Response(serializer.data)
        response['Authorization'] = f'Bearer {user.auth_token.key}'

        return response
