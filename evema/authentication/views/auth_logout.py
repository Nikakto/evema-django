from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response


class AuthLogout(GenericAPIView):

    permission_classes = (IsAuthenticated, )
    queryset = Token.objects.all()
    serializer_class = None

    def post(self, request, *args, **kwargs):

        if request.user.is_authenticated:
            request.user.auth_token.delete()

        return Response({'logout': 'ok'})
