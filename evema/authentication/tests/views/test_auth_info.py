from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from evema.utils.tests import character_create, character_remove


class TestAuthInfo(APITestCase):

    url = reverse('authentication:info')

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.client.logout()
        self.user, self.token, self.character, self.character_token = character_create()

    def tearDown(self):
        self.client.logout()
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_auth_info___correct_token__info(self):

        # we want test login by AUTHORIZATION header. At other tests can login by force_authenticate
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token.key}')

        response = self.client.get(self.url)
        result = response.json()

        self.assertEqual(result['data']['name'], self.character.name)
        self.assertEqual(result['data']['corporation'], self.character.corporation_id)
        self.assertIn('portrait', result['data'])

    def test_auth_info___correct_token_scopes__info_valid_scopes(self):

        expected_scopes = ['scope_1', 'scope_2']

        self.character_token.scopes = expected_scopes
        self.character_token.save()

        self.client.force_authenticate(user=self.user, token=self.token)
        response = self.client.get(self.url)
        result = response.json()

        self.assertListEqual(result['data']['scopes'], self.character_token.scopes)

    def test_auth_info__token_not_provided__401(self):

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 401)

    def test_auth_info___token_wrong__401(self):

        self.client.credentials(HTTP_AUTHORIZATION='Bearer wrong_token')
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 401)
