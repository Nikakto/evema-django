from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from evema.utils.tests import character_create, character_remove


class TestAuthLogout(APITestCase):

    url = reverse('authentication:logout')

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.user, self.token, self.character, self.character_token = character_create()

    def tearDown(self):
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_auth_logout___correct_token__logout(self):

        self.client.force_authenticate(user=self.user, token=self.token)
        response = self.client.post(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(Token.objects.filter(key=self.token.key).count())

    def test_auth_logout__token_not_provided__401(self):

        response = self.client.post(self.url)

        self.assertEqual(response.status_code, 401)

    def test_auth_logout___token_wrong__401(self):

        self.client.credentials(HTTP_AUTHORIZATION='Bearer wrong_token')
        response = self.client.post(self.url)

        self.assertEqual(response.status_code, 401)
