from django.conf import settings

from rest_framework.reverse import reverse
from rest_framework.test import APIClient, APISimpleTestCase

import urllib.parse as urlparse


class TestAuthLink(APISimpleTestCase):

    client_class = APIClient
    url = reverse('authentication:link')

    def test_auth_link__get__auth_link(self):

        expected_params = {
            'client_id': settings.EVE_OAUTH_CLIENT_ID,
            'redirect_uri': settings.EVE_OAUTH_REDIRECT_URI,
            'response_type': settings.EVE_OAUTH_RESPONSE_TYPE,
        }

        response = self.client.get(self.url)
        result = response.json()

        url_parsed = urlparse.urlparse(result['link'])
        url_params = dict(urlparse.parse_qsl(url_parsed.query, keep_blank_values=True))

        self.assertDictEqual(url_params, expected_params)
