from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from unittest import mock

from corporation.models import Corporation
from evema.utils.tests import character_create, character_remove, corporation_create


class TestAuthLogin(APITestCase):

    url = reverse('authentication:login')

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.user, self.token, self.character, self.character_token = character_create()

    def tearDown(self):
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('authentication.models.UserProxy.get_by_token_data', mock.MagicMock(return_value=None))
    def test_auth_login__character_get_by_token_data_is_none__401(self):

        response = self.client.post(self.url, data={'code': 'valid_code'})
        self.assertEqual(response.status_code, 401)

    def test_auth_login__code_is_empty__401(self):

        response = self.client.post(self.url, data={'code': ''})
        self.assertEqual(response.status_code, 401)

    def test_auth_login__code_not_provided__401(self):

        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 401)

    @mock.patch('authentication.views.auth_login.verify_token', mock.MagicMock(return_value=True))
    @mock.patch('authentication.views.auth_login.UserProxy.get_by_token_data')
    @mock.patch('corporation.models.Corporation.update_from_esiapi', mock.MagicMock(return_value=True))
    @mock.patch('esiapi.harvest.orders.harvest_character', mock.MagicMock(return_value=None))
    def test_auth_login__logined__token_and_character(self, mock_get_by_token_data):

        expected_token_key = self.token.key

        mock_get_by_token_data.return_value = self.user

        response = self.client.post(self.url, data={'code': 'valid_code'})
        result = response.json()

        self.assertEqual(result['character']['id'], self.character.id)
        self.assertEqual(result['token'], expected_token_key)
        self.assertEqual(self.user.auth_token.key, expected_token_key)

    @mock.patch('authentication.views.auth_login.verify_token', mock.MagicMock(return_value=True))
    @mock.patch('authentication.views.auth_login.UserProxy.get_by_token_data')
    @mock.patch('corporation.models.Corporation.update_from_esiapi', mock.MagicMock(return_value=True))
    @mock.patch('esiapi.harvest.orders.harvest_character', mock.MagicMock(return_value=None))
    def test_auth_login__logined_token_not_exist__token_and_character(self, mock_get_by_token_data):

        self.character.user.auth_token.delete()

        mock_get_by_token_data.return_value = self.user

        response = self.client.post(self.url, data={'code': 'valid_code'})
        result = response.json()

        self.assertEqual(result['character']['id'], self.character.id)
        self.assertIsNotNone(self.user.auth_token)
        self.assertEqual(result['token'], self.user.auth_token.key)

        self.token = self.user.auth_token

    @mock.patch('authentication.views.auth_login.verify_token', mock.MagicMock(return_value=True))
    @mock.patch('authentication.views.auth_login.UserProxy.get_by_token_data')
    def test_auth_login__load_character_order_immediately_error__401(self, mock_get_by_token_data):

        mock_get_by_token_data.return_value = self.user

        with mock.patch('esiapi.harvest.orders.harvest_character') as mock_harvest_character:
            mock_harvest_character.return_value = {'error': 'test_error'}
            response = self.client.post(self.url, data={'code': 'valid_code'})
            mock_harvest_character.assert_called_once()

        self.assertEqual(response.status_code, 401)

    @mock.patch('authentication.views.auth_login.verify_token', mock.MagicMock(return_value=True))
    @mock.patch('authentication.views.auth_login.UserProxy.get_by_token_data')
    @mock.patch('esiapi.harvest.orders.harvest_character', mock.MagicMock(return_value=None))
    def test_auth_login__load_character_corporation_immediately_error__401(self, mock_get_by_token_data):

        mock_get_by_token_data.return_value = self.user

        with mock.patch('corporation.models.Corporation.update_from_esiapi') as mock_update_from_esiapi:
            mock_update_from_esiapi.return_value = None
            response = self.client.post(self.url, data={'code': 'valid_code'})
            mock_update_from_esiapi.assert_called_once()

        self.assertEqual(response.status_code, 401)

    @mock.patch('authentication.views.auth_login.verify_token', mock.MagicMock(return_value=True))
    @mock.patch('authentication.views.auth_login.UserProxy.get_by_token_data')
    @mock.patch('esiapi.harvest.orders.harvest_character', mock.MagicMock(return_value=None))
    def test_auth_login__load_character_corporation_immediately_error_corporation_exist__401(self, mock_get_by_token_data):

        mock_get_by_token_data.return_value = self.user

        corporation_id = self.user.character.corporation_id
        Corporation.objects.filter(id=corporation_id).first() or corporation_create(corporation_id=corporation_id)

        with mock.patch('corporation.models.Corporation.update_from_esiapi') as mock_update_from_esiapi:
            mock_update_from_esiapi.return_value = None
            response = self.client.post(self.url, data={'code': 'valid_code'})
            mock_update_from_esiapi.assert_called_once()

        self.assertEqual(response.status_code, 401)

    @mock.patch('authentication.views.auth_login.verify_token', mock.MagicMock(return_value=None))
    def test_auth_login__verify_token_error__401(self):

        response = self.client.post(self.url, data={'code': 'valid_code'})
        self.assertEqual(response.status_code, 401)
