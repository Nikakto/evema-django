from django.contrib.auth.models import User
from rest_framework.test import APITestCase

from unittest import mock

from authentication.models import UserProxy
from authentication.tests.models.user_proxy import CHARACTER_TOKEN, TOKEN_DATA
from evema.utils.tests import character_create, character_remove



class TestUserProxyGetByTokenData(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.user, self.token, self.character, self.character_token = character_create(character_id=CHARACTER_TOKEN['CharacterID'])

    def tearDown(self):
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('authentication.models.obtain_character', mock.MagicMock(return_value=CHARACTER_TOKEN))
    @mock.patch('authentication.models.UserProxy.get_by_character_token', mock.MagicMock(return_value=None))
    def test_get_by_token_data__get_by_character_token_error__None(self):

        result = UserProxy.get_by_token_data(TOKEN_DATA)

        self.assertIsNone(result)

    @mock.patch('authentication.models.UserProxy.get_by_character_token')
    @mock.patch('authentication.models.obtain_character', mock.MagicMock(return_value=CHARACTER_TOKEN))
    @mock.patch('authentication.models.UserProxy.update_character_token', mock.MagicMock(return_value=True))
    def test_get_by_token_data__no_error__User(self, mock_get_by_character_token):

        mock_get_by_character_token.return_value = UserProxy.objects.get(id=self.user.id)

        result = UserProxy.get_by_token_data(TOKEN_DATA)

        self.assertIsInstance(result, User)
        self.assertEqual(result.id, self.user.id)

    @mock.patch('authentication.models.obtain_character', mock.MagicMock(return_value=None))
    def test_get_by_token_data__obtain_character_error__None(self):

        result = UserProxy.get_by_token_data(TOKEN_DATA)

        self.assertIsNone(result)

    @mock.patch('authentication.models.UserProxy.get_by_character_token')
    @mock.patch('authentication.models.obtain_character', mock.MagicMock(return_value=CHARACTER_TOKEN))
    @mock.patch('authentication.models.UserProxy.update_character_token', mock.MagicMock(return_value=None))
    def test_get_by_token_data__update_character_token_error__None(self, mock_get_by_character_token):

        mock_get_by_character_token.return_value = UserProxy.objects.get(id=self.user.id)

        result = UserProxy.get_by_token_data(TOKEN_DATA)

        self.assertIsNone(result)
