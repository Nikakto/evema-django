from rest_framework.test import APITestCase

from unittest import mock

from authentication.models import UserProxy
from authentication.tests.models.user_proxy import CHARACTER_TOKEN
from character.models import Character
from evema.utils.tests import character_create, character_remove


class TestUserProxyUpdateCharacterToken(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.user, self.token, self.character, self.character_token = character_create(character_id=CHARACTER_TOKEN['CharacterID'])

    def tearDown(self):
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_get_by_character_token__character_has_user__User(self):

        result_user = UserProxy.get_by_character_token(CHARACTER_TOKEN)

        self.assertEqual(result_user, self.user)

    def test_get_by_character_token__character_has_not_user__User(self):

        self.user.delete()

        result_user = UserProxy.get_by_character_token(CHARACTER_TOKEN)

        self.assertEqual(result_user.character, self.character)

    def test_get_by_character_token__character_not_exist__User(self):

        character_remove(self.user, self.token, self.character, self.character_token)
        self.character.id = CHARACTER_TOKEN['CharacterID']
        self.character.token = None
        self.character.user = None

        self.assertIsNone(Character.objects.filter(id=CHARACTER_TOKEN['CharacterID']).first())

        with mock.patch('authentication.models.character_update_from_esiapi', mock.MagicMock(return_value=self.character)):
            result_user = UserProxy.get_by_character_token(CHARACTER_TOKEN)

        self.assertEqual(self.character.user, result_user)

    @mock.patch('character.tasks.character_update_from_esiapi.retry', mock.MagicMock())
    @mock.patch('character.models.Character.update_from_esiapi', mock.MagicMock(return_value=False))
    def test_get_by_character_token__character_not_exist_esiapi_character_error__None(self):

        character_remove(self.user, self.token, self.character, self.character_token)
        self.assertIsNone(Character.objects.filter(id=CHARACTER_TOKEN['CharacterID']).first())

        result_user = UserProxy.get_by_character_token(CHARACTER_TOKEN)

        self.assertIsNone(result_user)
