from rest_framework.test import APITestCase

from unittest import mock

from authentication.models import UserProxy
from authentication.tests.models.user_proxy import CHARACTER_TOKEN, TOKEN_DATA
from character.models import CharacterToken
from evema.utils.tests import character_create, character_remove


class TestUserProxyUpdateCharacterToken(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def tearDown(self):
        UserProxy.objects.filter(character__id=CHARACTER_TOKEN['CharacterID']).delete()

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_update_character_token__character_token_updated__token(self):

        user, token, character, character_token = character_create(character_id=CHARACTER_TOKEN['CharacterID'])

        with mock.patch('character.models.CharacterToken.scopes_verified',[]):
            token_result = UserProxy.update_character_token(user, TOKEN_DATA)

        self.assertEqual(token_result, character_token)

        character_remove(user, token, character, character_token)

    def test_update_character_token__character_token_created__token(self):

        user, token, character, character_token = character_create(character_id=CHARACTER_TOKEN['CharacterID'])
        character_token.delete()

        with mock.patch('character.models.CharacterToken.scopes_verified', []):
            token_result = UserProxy.update_character_token(user, TOKEN_DATA)

        character.refresh_from_db()

        self.assertIsInstance(token_result, CharacterToken)
        self.assertEqual(token_result, character.token)

        character_remove(user, token, character, character_token)

    def test_update_character_token__character_token_scopes_verified__User(self):

        user, token, character, character_token = character_create(character_id=CHARACTER_TOKEN['CharacterID'])
        expected_scopes = ['scope_1', 'scope_2']

        with mock.patch('character.models.CharacterToken.scopes_verified', expected_scopes):
            token_result = UserProxy.update_character_token(user, TOKEN_DATA)

        self.assertListEqual(token_result.scopes, expected_scopes)

        character_remove(user, token, character, character_token)

    def test_update_character_token__character_token_scopes_verified_error__None(self):

        user, token, character, character_token = character_create(character_id=CHARACTER_TOKEN['CharacterID'])
        character.token.delete()

        with mock.patch('character.models.CharacterToken.scopes_verified', None):
            token_result = UserProxy.update_character_token(user, TOKEN_DATA)

        self.assertIsNone(token_result)

        character_remove(user, token, character, character_token)
