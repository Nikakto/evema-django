from django.utils import timezone

ESI_API_CHARACTER_BIRTHDAY = timezone.now().replace(microsecond=0) - timezone.timedelta(days=100)
ESI_API_CHARACTER = {
    'ancestry_id': 100,
    'birthday': ESI_API_CHARACTER_BIRTHDAY.strftime('%Y-%m-%dT%H:%M:%SZ'),
    'bloodline_id': 666,
    'corporation_id': 879,
    'description': 'test_description',
    'gender': 'female',
    'name': 'test_name_esiapi',
    'race_id': 987,
    'security_status': -10,
}

CHARACTER_TOKEN = {
    'CharacterID': 100500,
    'CharacterName': 'non_regular_test_name',
    'CharacterOwnerHash': 'non_regular_test_hash',
    'ExpiresOn': timezone.now(),
}

TOKEN_DATA = {
    'access_token': 'non_regular_test_token',
    'refresh_token': 'non_regular_test_token',
    'token_type': 'non_regular',
}