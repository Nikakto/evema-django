from rest_framework.permissions import BasePermission


class ReadCharacterOrdersV1(BasePermission):

    def has_permission(self, request, view):
        user = request.user and request.user.is_authenticated
        has_permission = 'esi-markets.read_character_orders.v1' in request.user.character.token.scopes
        return user and has_permission


class ReadCorporationOrdersV1(BasePermission):

    def has_permission(self, request, view):
        user = request.user and request.user.is_authenticated
        has_permission = 'esi-markets.read_corporation_orders.v1' in request.user.character.token.scopes
        return user and has_permission
