from django.urls import path

from character.views import CharacterMarketOrdersView, CharacterTokenScopesGroupView

urlpatterns = [
    path('orders/', CharacterMarketOrdersView.as_view(), name='orders'),
    path('scopes/grouped/', CharacterTokenScopesGroupView.as_view(), name='scopes_grouped'),
]
