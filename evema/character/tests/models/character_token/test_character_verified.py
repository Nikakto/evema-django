from rest_framework.test import APITestCase

from unittest import mock

from evema.utils.tests import character_create, character_remove


class TestCharacterTokenCharacterVerified(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.user, self.token, self.character, self.character_token = character_create()

    def tearDown(self):
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('esiapi.endpoint.meta.get_verify', mock.MagicMock(return_value=({'error': 'error'}, {})))
    def test_character_verified__error__None(self):

        character = self.character_token.character_verified

        self.assertIsNone(character)

    @mock.patch('esiapi.endpoint.meta.get_verify')
    def test_character_verified__verified__token_data(self, mock_get_verify):

        expected_result = {'Scopes': 'scope_1 scope_2'}

        mock_get_verify.return_value = expected_result, {}

        character = self.character_token.character_verified

        self.assertEqual(character['Scopes'], expected_result['Scopes'])

    @mock.patch('esiapi.endpoint.meta.get_verify')
    def test_character_verified__verified_no_scopes__token_data(self, mock_get_verify):

        expected_result = {'CharacterID': 'character_id'}

        mock_get_verify.return_value = expected_result, {}

        character = self.character_token.character_verified

        self.assertEqual(character['CharacterID'], expected_result['CharacterID'])
