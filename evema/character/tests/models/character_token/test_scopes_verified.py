from rest_framework.test import APITestCase

from unittest import mock

from evema.utils.tests import character_create, character_remove


class TestCharacterTokenScopesVerified(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.user, self.token, self.character, self.character_token = character_create()

    def tearDown(self):
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('character.models.CharacterToken.character_verified', None)
    def test_scopes_verified__error__None(self):

        result = self.character_token.scopes_verified

        self.assertFalse(result)

    def test_scopes_verified__verified__scopes(self):

        expected_scopes = 'scope_1 scope_2'

        with mock.patch('character.models.CharacterToken.character_verified', {'Scopes': expected_scopes}):
            result = self.character_token.scopes_verified

        self.assertTrue(result)
        self.assertListEqual(sorted(result), sorted(expected_scopes.split()))

    def test_scopes_verified__verified_no_scopes__empty_list(self):

        with mock.patch('character.models.CharacterToken.character_verified', {'CharacterID': 'character_id'}):
            result = self.character_token.scopes_verified

        self.assertFalse(result)
        self.assertIsInstance(result, list)
