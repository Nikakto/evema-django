from django.utils import timezone
from rest_framework.test import APITestCase

from unittest import mock

from evema.utils.tests import character_create, character_remove

ESI_API_CHARACTER_BIRTHDAY = timezone.now().replace(microsecond=0) - timezone.timedelta(days=100)
ESI_API_CHARACTER = {
    'ancestry_id': 100,
    'birthday': ESI_API_CHARACTER_BIRTHDAY.strftime('%Y-%m-%dT%H:%M:%SZ'),
    'bloodline_id': 666,
    'corporation_id': 879,
    'description': 'test_description',
    'gender': 'female',
    'name': 'test_name_esiapi',
    'race_id': 987,
    'security_status': -10,
}


class TestCharacterUpdateFromEsiAPi(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.user, self.token, self.character, self.character_token = character_create()

    def tearDown(self):
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('esiapi.character.character')
    def test_character_update_from_esiapi__character__changed(self, mock_esiapi_character):

        mock_esiapi_character.return_value = ESI_API_CHARACTER, {}

        last_synced_at_unexpected = timezone.now()
        self.character.synced_at = last_synced_at_unexpected
        self.character.alliance_id = 10
        self.character.faction_id = 11
        self.character.save()

        last_synced_at_result = self.character.update_from_esiapi()
        self.character.refresh_from_db(fields=['synced_at'])

        self.assertIsInstance(last_synced_at_result, timezone.datetime)
        self.assertNotEqual(last_synced_at_result, last_synced_at_unexpected)
        self.assertEqual(last_synced_at_result, self.character.synced_at)

        self.assertEqual(self.character.ancestry_id, ESI_API_CHARACTER['ancestry_id'])
        self.assertEqual(self.character.birthday, ESI_API_CHARACTER_BIRTHDAY)
        self.assertEqual(self.character.bloodline_id, ESI_API_CHARACTER['bloodline_id'])
        self.assertEqual(self.character.corporation_id, ESI_API_CHARACTER['corporation_id'])
        self.assertEqual(self.character.description, ESI_API_CHARACTER['description'])
        self.assertEqual(self.character.gender, ESI_API_CHARACTER['gender'])
        self.assertEqual(self.character.name, ESI_API_CHARACTER['name'])
        self.assertEqual(self.character.race_id, ESI_API_CHARACTER['race_id'])
        self.assertEqual(self.character.security_status, ESI_API_CHARACTER['security_status'])

        self.assertIsNone(self.character.alliance_id)
        self.assertIsNone(self.character.faction_id)

    @mock.patch('esiapi.character.character')
    def test_character_update_from_esiapi__character_with_unrequired__changed(self, mock_esiapi_character):

        esiapi_character = dict(ESI_API_CHARACTER)
        esiapi_character.update({'alliance_id': 1001, 'faction_id': 2,})
        mock_esiapi_character.return_value = esiapi_character, {}

        last_synced_at_unexpected = timezone.now()
        self.character.last_update = last_synced_at_unexpected
        self.character.save()

        last_synced_at_result = self.character.update_from_esiapi()

        self.assertIsInstance(last_synced_at_result, timezone.datetime)
        self.assertNotEqual(last_synced_at_result, last_synced_at_unexpected)

        self.assertEqual(self.character.alliance_id, esiapi_character['alliance_id'])
        self.assertEqual(self.character.faction_id, esiapi_character['faction_id'])

    @mock.patch('esiapi.character.character', mock.MagicMock(return_value=({'error': 'test_error'}, {})))
    def test_character_update_from_esiapi__error__last_update_not_changed(self):

        last_synced_at_expected = timezone.now()
        self.character.last_update = last_synced_at_expected
        self.character.save()

        last_update_result = self.character.update_from_esiapi()

        self.assertIsNone(last_update_result)
