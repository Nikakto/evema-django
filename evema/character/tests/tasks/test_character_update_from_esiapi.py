from rest_framework.test import APITestCase

from unittest import mock

from character.models import Character
from character.tasks import character_update_from_esiapi
from character.tests.models.character.test_update_from_esiapi import ESI_API_CHARACTER
from evema.utils.tests import character_create, character_remove


class TestTaskCharacterUpdateFromEsiApi(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.user, self.token, self.character, self.character_token = character_create()

    def tearDown(self):
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('esiapi.character.character', mock.MagicMock(return_value=(ESI_API_CHARACTER, {})))
    def test_character_update_from_esiapi__character_does_not_exist__character(self):

        character_id = self.character.id + 10000
        self.assertIsNone(Character.objects.filter(id=character_id).first())

        character = character_update_from_esiapi(character_id)

        self.assertEqual(character.id, character_id)

        character.delete()

    @mock.patch('esiapi.character.character', mock.MagicMock(return_value=(ESI_API_CHARACTER, {})))
    def test_character_update_from_esiapi__character_exist__character(self):

        character = character_update_from_esiapi(self.character.id)

        self.assertEqual(self.character, character)

    @mock.patch('character.tasks.character_update_from_esiapi.retry', mock.MagicMock())
    @mock.patch('character.models.Character.update_from_esiapi', mock.MagicMock(return_value=False))
    def test_character_update_from_esiapi__character_update_error_max_retry__raise_ConnectionError(self):
        self.assertRaises(ConnectionError, character_update_from_esiapi, self.character.id)

    @mock.patch('character.models.Character.update_from_esiapi', mock.MagicMock(return_value=False))
    def test_character_update_from_esiapi__character_update_error_raise_error_False__None(self):

        result = character_update_from_esiapi(self.character.id, raise_error=False, retry=False)

        self.assertIsNone(result)

    @mock.patch('character.models.Character.update_from_esiapi', mock.MagicMock(return_value=False))
    def test_character_update_from_esiapi__character_update_error_retry_False__None(self):
        self.assertRaises(ConnectionError, character_update_from_esiapi, self.character.id, retry=False)
