from django.utils import dateparse, timezone

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from evema.utils.pagination import MainPagination
from evema.utils.tests import character_create, character_remove
from market.models import MarketOrder


class TestCharacterOrdersView(APITestCase):

    url = reverse('character:orders')

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',
        'market_orders',
    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):

        self.user, self.token, self.character, self.character_token = character_create()
        self.client.force_authenticate(user=self.user, token=self.token)

        MarketOrder.objects.update(character=self.character)

        self.pages_count = MarketOrder.objects.filter(character=self.character).count() // MainPagination.page_size + 1

    def tearDown(self):
        self.client.logout()
        MarketOrder.objects.update(character=None)
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_character_orders__get__character_orders(self):

        expected_ids = MarketOrder.objects.values_list('id', flat=True)[:5]
        MarketOrder.objects.exclude(id__in=expected_ids).update(character=None)

        response = self.client.get(self.url)
        
        # check answer item
        market_order_answer = response.data[0]
        market_order_real = MarketOrder.objects.get(id=market_order_answer['id'])

        self.assertEqual(market_order_answer['character'], market_order_real.character_id)
        self.assertEqual(market_order_answer['corporation'], market_order_real.corporation_id)
        self.assertEqual(market_order_answer['duration'], market_order_real.duration)
        self.assertEqual(market_order_answer['is_buy_order'], market_order_real.is_buy_order)
        self.assertEqual(market_order_answer['item_type'], market_order_real.item_type_id)
        self.assertEqual(market_order_answer['min_volume'], market_order_real.min_volume)
        self.assertEqual(market_order_answer['region'], market_order_real.system.region_id)
        self.assertEqual(market_order_answer['station'], market_order_real.station.id)
        self.assertEqual(market_order_answer['structure'], market_order_real.station.structure)
        self.assertEqual(market_order_answer['system'], market_order_real.system_id)
        self.assertEqual(market_order_answer['volume_total'], market_order_real.volume_total)
        self.assertEqual(market_order_answer['volume_remain'], market_order_real.volume_remain)

        self.assertAlmostEqual(float(market_order_answer['price']), market_order_real.price, 2)

        self.assertEqual(market_order_answer['region_name'], market_order_real.system.region.name)
        self.assertEqual(market_order_answer['station_name'], market_order_real.station.name)
        self.assertEqual(market_order_answer['system_name'], market_order_real.system.name)

        self.assertEqual(dateparse.parse_datetime(market_order_answer['expires_date']),
                         market_order_real.issued + timezone.timedelta(days=market_order_real.duration))

        # check reponse orders is expected_orders
        answer_ids = [order['id'] for order in response.data]
        self.assertListEqual(sorted(answer_ids), sorted(expected_ids))
        
        # check default sorting
        sorted_by_price = [order['id'] for order in sorted(response.data, key=lambda order: float(order['price']))]
        self.assertListEqual(answer_ids, sorted_by_price)

    def test_character_orders__get_no_orders__empty(self):

        MarketOrder.objects.update(character=None)

        response = self.client.get(self.url)
        self.assertIsInstance(response.data, list)
        self.assertFalse(response.data)

    def test_character_orders__token_not_provided__401(self):

        self.client.logout()  # clear token

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 401)

    def test_character_orders__get_page_last__list_page_last(self):

        orders_count = MarketOrder.objects.filter(character=self.character).count()
        expected_orders_count = orders_count % MainPagination.page_size

        data = {'page': self.pages_count}
        response = self.client.get(self.url, data)

        # check page items
        self.assertEqual(response.data[-1]['id'], MarketOrder.objects.order_by('price').last().id)
        self.assertEquals(len(response.data), expected_orders_count)

        # check pagination
        self.assertEqual(response['x-page-current'], str(self.pages_count))
        self.assertEqual(response['x-pages'], str(self.pages_count))

    def test_character_orders__get_page_invalid__404(self):

        data = {'page': self.pages_count + 1}
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_character_orders__filter_by_item_type__only_item_id(self):

        item_type_id = 1
        orders_count_expected = MarketOrder.objects.filter(character=self.character, item_type_id=item_type_id).count()

        data = {'item_type': item_type_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_character_orders__filter_by_order_is_buy__only_buy_orders(self):

        orders_count_expected = MarketOrder.objects.filter(character=self.character, is_buy_order=True).count()

        data = {'is_buy_order': True}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_character_orders__filter_by_order_is_not_buy__only_sell_orders(self):

        orders_count_expected = MarketOrder.objects.filter(character=self.character, is_buy_order=False).count()

        data = {'is_buy_order': False}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_character_orders__filter_by_region__only_region_id(self):

        region_id = 1

        orders_count_expected = MarketOrder.objects.filter(character=self.character, system__region__id=region_id).count()

        data = {'region': region_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_character_orders__filter_by_station__only_station_id(self):

        station_id = 1

        orders_count_expected = MarketOrder.objects.filter(character=self.character, station_id=station_id).count()

        data = {'station': station_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_character_orders__filter_by_system__only_system_id(self):

        system_id = 1

        orders_count_expected = MarketOrder.objects.filter(character=self.character, system_id=system_id).count()

        data = {'system': system_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_character_orders__ordering_volume_remain__ascending(self):
        # default is price, so choose another field

        data = {'ordering': 'volume_remain'}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)
        self.assertListEqual(response.data, sorted(response.data, key=lambda item: item['volume_remain']))

    def test_character_orders__ordering_region__descending(self):

        data = {'ordering': '-region'}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)
        self.assertListEqual(response.data, sorted(response.data, key=lambda item: item['region'], reverse=True))

    def test_character_orders__permission_denied__403(self):

        self.character_token.scopes = ()

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 403)
