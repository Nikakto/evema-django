from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from character.models import CharacterTokenScope, CharacterTokenScopesGroup


class TestCharacterScopes(APITestCase):

    url = reverse('character:scopes_grouped')

    def test_character_scopes_grouped__get__valid_scopes(self):

        response = self.client.get(self.url)

        for group in response.json():
            scopes_db = CharacterTokenScope.objects.filter(group__name=group['name']).values('description', 'scope')
            self.assertListEqual(group['scopes'], list(scopes_db))
