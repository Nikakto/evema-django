from rest_framework import serializers

from character.models import Character, CharacterTokenScope, CharacterTokenScopesGroup


class CharacterSerializer(serializers.ModelSerializer):

    scopes = serializers.ListField(source='token.scopes')

    class Meta:
        fields = ('id', 'corporation', 'name', 'portrait', 'scopes')
        model = Character


class CharacterTokenScopeSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('description', 'scope')
        model = CharacterTokenScope


class CharacterTokenScopesGroupSerializer(serializers.ModelSerializer):

    scopes = CharacterTokenScopeSerializer(many=True)

    class Meta:
        fields = ('name', 'scopes')
        model = CharacterTokenScopesGroup
