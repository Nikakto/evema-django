from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from character.models import CharacterTokenScopesGroup
from character.permissions import ReadCharacterOrdersV1
from character.serializers import CharacterTokenScopesGroupSerializer
from market.views import MarketOrdersView


class CharacterMarketOrdersView(MarketOrdersView):

    permission_classes = (IsAuthenticated, ReadCharacterOrdersV1)

    def get_queryset(self):
        return super().get_queryset().filter(character_id=self.request.user.character.id)


class CharacterTokenScopesGroupView(ListAPIView):

    queryset = CharacterTokenScopesGroup.objects.all().order_by('name')
    serializer_class = CharacterTokenScopesGroupSerializer
