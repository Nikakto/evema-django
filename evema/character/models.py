from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import dateparse, timezone
from django.utils.functional import cached_property

import datetime

import esiapi
from esiapi.auth import refresh_token


class Character(models.Model):

    id = models.BigAutoField(primary_key=True)

    alliance = models.ForeignKey('corporation.Alliance', db_constraint=False, null=True, on_delete=models.SET_NULL, related_name='characters')
    ancestry_id = models.IntegerField()
    birthday = models.DateTimeField()
    bloodline_id = models.IntegerField()
    corporation = models.ForeignKey('corporation.Corporation', db_constraint=False, on_delete=models.PROTECT, related_name='characters')
    description = models.TextField(blank=True)
    faction_id = models.IntegerField(null=True)
    gender = models.CharField(max_length=16)
    name = models.CharField(max_length=128)
    race_id = models.IntegerField()
    security_status = models.DecimalField(decimal_places=2, max_digits=4)
    synced_at = models.DateTimeField(null=True)
    token = models.OneToOneField('CharacterToken', null=True, on_delete=models.SET_NULL, related_name='+')
    user = models.OneToOneField(User, null=True, on_delete=models.SET_NULL, related_name='character')

    @property
    def portrait(self) -> dict:
        image_size = [32, 64, 128, 256, 512]
        return {size: f'https://image.eveonline.com/Character/{self.id}_{size}.jpg' for size in image_size}

    def update_from_esiapi(self):

        character_data, headers = esiapi.character.character(self.id)
        if 'error' in character_data:
            return None

        # required fields
        self.ancestry_id = character_data['ancestry_id']
        self.birthday = dateparse.parse_datetime(character_data['birthday'])
        self.bloodline_id = character_data['bloodline_id']
        self.corporation_id = character_data['corporation_id']
        self.description = character_data['description']
        self.gender = character_data['gender']
        self.name = character_data['name']
        self.race_id = character_data['race_id']
        self.security_status = character_data['security_status']

        # nullable fields
        self.alliance_id = character_data.get('alliance_id')
        self.faction_id = character_data.get('faction_id')

        self.synced_at = timezone.now()

        self.save()

        return self.synced_at


class CharacterToken(models.Model):

    access_token = models.CharField(max_length=128)
    character = models.ForeignKey('Character', on_delete=models.CASCADE, related_name='related_tokens')
    expires_in = models.DateTimeField(default=timezone.now)
    expired = models.BooleanField(default=False)
    public = models.BooleanField(default=False)
    refresh_token = models.CharField(max_length=128, null=True)
    scopes = ArrayField(models.CharField(max_length=128), blank=True, default=list, )
    token_type = models.CharField(max_length=16, default='Bearer')
    # user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='related_tokens')

    @cached_property
    def character_verified(self):

        esiapi_result, headers = esiapi.meta.get_verify(self.character.id, self)
        return esiapi_result if 'error' not in esiapi_result else None

    def refresh(self):

        token_data = refresh_token(self.refresh_token)
        if not token_data:
            self.expired = True
        else:
            self.update_from_json(token_data)

        self.save()

        return not self.expired

    @cached_property
    def scopes_verified(self):

        character_verified = self.character_verified

        if not character_verified:
            return None

        scopes = character_verified.get('Scopes', '')
        scopes = scopes.split()

        return scopes

    def update_from_json(self, token_data):

        self.access_token = token_data['access_token']
        self.refresh_token = token_data['refresh_token']
        self.token_type = token_data['token_type']

        expires_time = datetime.datetime.now().astimezone() + datetime.timedelta(seconds=1200)
        self.expires_in = expires_time
        self.expired = False


class CharacterTokenScope(models.Model):

    description = models.CharField(max_length=128)
    group = models.ForeignKey('CharacterTokenScopesGroup', on_delete=models.CASCADE, related_name='scopes')
    scope = models.CharField(db_index=True, max_length=128, unique=True)


class CharacterTokenScopesGroup(models.Model):
    name = models.CharField(db_index=True, max_length=32, unique=True)
