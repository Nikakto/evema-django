

def scopes_add(apps, scopes_groups=None, scopes=None):

    # create groups
    CharacterTokenScopesGroup = apps.get_model('character', 'CharacterTokenScopesGroup')
    if scopes_groups:
        CharacterTokenScopesGroup.objects.bulk_create(CharacterTokenScopesGroup(name=name) for name in scopes_groups)

    # create scopes
    if scopes:
        CharacterTokenScope = apps.get_model('character', 'CharacterTokenScope')

        scopes_to_create = []
        for kwargs in scopes:
            kwargs['group'] = CharacterTokenScopesGroup.objects.get(name=kwargs['group'])
            scopes_to_create.append(CharacterTokenScope(**kwargs))

        CharacterTokenScope.objects.bulk_create(scopes_to_create)


def scopes_remove(apps, scopes_groups=None, scopes=None):

    # remove scopes
    if scopes:
        CharacterTokenScope = apps.get_model('character', 'CharacterTokenScope')
        created_scopes = [scope['scope'] for scope in scopes]
        CharacterTokenScope.objects.filter(scope__in=created_scopes).delete()

    # remove groups
    if scopes_groups:
        CharacterTokenScopesGroup = apps.get_model('character', 'CharacterTokenScopesGroup')
        CharacterTokenScopesGroup.objects.filter(name__in=scopes_groups)
