import logging
from celery import group, shared_task

from character.models import Character
from corporation.models import Alliance, Corporation
from evema.celery import app

logger = logging.getLogger(__name__)


# ======================================================================================================================
# UPDATE CHARACTERS FROM ESI API
# ======================================================================================================================


@shared_task(default_retry_delay=60, time_limit=15)
def character_update_from_esiapi(character_id, raise_error=True, retry=True):

    character = Character.objects.filter(id=character_id).first() or Character(id=character_id)
    if not character.update_from_esiapi():

        if retry and character_update_from_esiapi.request.retries != character_update_from_esiapi.max_retries:
            character_update_from_esiapi.retry(countdown=60)

        if raise_error:
            raise ConnectionError(f'ESI API: Can\'t get character {character_id}')
        else:
            return None

    return character


@app.task
def characters_update_from_esiapi():

    characters_ids = Character.objects.values_list('id', flat=True)

    characters_ids.union(
        Alliance.objects.exclude(creator_id__in=characters_ids).values_list('creator_id', flat=True),
        Corporation.objects.exclude(ceo_id__in=characters_ids).values_list('ceo_id', flat=True),
        Corporation.objects.exclude(creator_id__in=characters_ids).values_list('creator_id', flat=True),
    )

    subtasks = group(character_update_from_esiapi.s(character_id) for character_id in characters_ids)
    subtasks.delay()

    return list(characters_ids)
