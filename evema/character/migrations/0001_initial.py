# Generated by Django 2.1.1 on 2018-09-08 18:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Character',
            fields=[
                ('id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('active', models.BooleanField(default=True)),
                ('name', models.CharField(max_length=128)),
                ('hash', models.CharField(max_length=128)),
                ('expires_on', models.DateTimeField(null=True)),
                ('public_token', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='CharacterToken',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('access_token', models.CharField(max_length=128)),
                ('expires_in', models.DateTimeField(auto_now_add=True)),
                ('expired', models.BooleanField(default=False)),
                ('refresh_token', models.CharField(max_length=128)),
                ('token_type', models.CharField(max_length=16)),
            ],
        ),
        migrations.AddField(
            model_name='character',
            name='token',
            field=models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, related_name='character', to='character.CharacterToken'),
        ),
        migrations.AddField(
            model_name='character',
            name='user',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='character', to=settings.AUTH_USER_MODEL),
        ),
    ]
