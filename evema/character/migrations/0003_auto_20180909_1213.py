# Generated by Django 2.1.1 on 2018-09-09 12:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('character', '0002_auto_20180909_1212'),
    ]

    operations = [
        migrations.RenameField(
            model_name='charactertoken',
            old_name='public_token',
            new_name='public',
        ),
    ]
