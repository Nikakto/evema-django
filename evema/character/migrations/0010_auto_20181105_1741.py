# Generated by Django 2.1.2 on 2018-11-05 17:41

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('character', '0009_auto_20181105_1741'),
        ('corporation', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='character',
            name='alliance',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='characters', to='corporation.Alliance'),
        ),
        migrations.AddField(
            model_name='character',
            name='ancestry_id',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='character',
            name='birthday',
            field=models.DateTimeField(default=datetime.datetime(2018, 11, 5, 17, 40, 38, 448487, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='character',
            name='bloodline_id',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='character',
            name='corporation',
            field=models.ForeignKey(db_constraint=False, default=0, on_delete=django.db.models.deletion.PROTECT, related_name='characters', to='corporation.Corporation'),
        ),
        migrations.AddField(
            model_name='character',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='character',
            name='faction_id',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='character',
            name='gender',
            field=models.CharField(default='man', max_length=16),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='character',
            name='race_id',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='character',
            name='security_status',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=4),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='character',
            name='synced_at',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='charactertoken',
            name='character',
            field=models.ForeignKey(default=95372979, on_delete=django.db.models.deletion.CASCADE, related_name='tokens', to='character.Character'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='character',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='character', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='charactertoken',
            name='token_type',
            field=models.CharField(default='Bearer', max_length=16),
        ),
    ]
