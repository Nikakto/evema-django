from rest_framework.test import APIClient, APISimpleTestCase

from rest_framework.reverse import reverse


class TestServiceHealth(APISimpleTestCase):

    client_class = APIClient
    url = reverse('service:health')

    def test_service_health__get__http_200(self):

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
