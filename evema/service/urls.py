from django.urls import path

from service.views import Health


urlpatterns = [
    path('health', Health.as_view(), name='health'),
]
