from django.utils import timezone
from rest_framework.test import APITestCase

from unittest import mock

from evema.utils.tests import alliance_create

ESI_API_ALLIANCE_DATE_FOUNDED = timezone.now().replace(microsecond=0) - timezone.timedelta(days=100)
ESI_API_ALLIANCE = {
  "creator_corporation_id": 98388312,
  "creator_id": 1597785719,
  "executor_corporation_id": 98500220,
  "name": "Pandemic Horde",
  "ticker": "REKTD"
}


class TestAllianceUpdateFromEsiAPi(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.alliance = alliance_create()

    def tearDown(self):
        self.alliance.delete()

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('esiapi.alliances.alliance')
    def test_corporation_update_from_esiapi__alliance__updated(self, mock_esiapi_alliance):

        mock_esiapi_alliance.return_value = ESI_API_ALLIANCE, {}

        last_synced_at_unexpected = timezone.now()
        self.alliance.synced_at = last_synced_at_unexpected
        self.alliance.alliance_id = 10
        self.alliance.faction_id = 11
        self.alliance.save()

        last_synced_at_result = self.alliance.update_from_esiapi()

        self.assertIsInstance(last_synced_at_result, timezone.datetime)
        self.assertNotEqual(last_synced_at_result, last_synced_at_unexpected)

        self.assertEqual(self.alliance.creator_corporation_id, ESI_API_ALLIANCE['creator_corporation_id'])
        self.assertEqual(self.alliance.creator_id, ESI_API_ALLIANCE['creator_id'])
        self.assertEqual(self.alliance.name, ESI_API_ALLIANCE['name'])
        self.assertEqual(self.alliance.ticker, ESI_API_ALLIANCE['ticker'])

        self.assertIsNone(self.alliance.date_founded)
        self.assertIsNone(self.alliance.faction_id)

    @mock.patch('esiapi.alliances.alliance')
    def test_corporation_update_from_esiapi__corporation_with_unrequired__changed(self, mock_esiapi_alliance):

        not_required_fields = {
            'faction_id': 2,
            'date_founded': ESI_API_ALLIANCE_DATE_FOUNDED.isoformat(),
        }

        mock_esiapi_alliance.return_value = {**ESI_API_ALLIANCE, **not_required_fields}, {}

        self.alliance.update_from_esiapi()

        self.assertEqual(self.alliance.date_founded, ESI_API_ALLIANCE_DATE_FOUNDED)
        self.assertEqual(self.alliance.faction_id, not_required_fields['faction_id'])

    @mock.patch('esiapi.alliances.alliance', mock.MagicMock(return_value=({'error': 'test_error'}, {})))
    def test_alliance_update_from_esiapi__error__last_update_not_changed(self):

        last_synced_at_expected = timezone.now()
        self.alliance.last_update = last_synced_at_expected
        self.alliance.save()

        last_update_result = self.alliance.update_from_esiapi()

        self.assertIsNone(last_update_result)

    def test_corporation_update_from_esiapi__foreign_keys_constraint_false_and_not_exist__tasks_created(self):
        pass
