from django.utils import timezone
from rest_framework.test import APITestCase

from unittest import mock

from evema.utils.tests import character_create, character_remove, corporation_create

ESI_API_CORPORATION_DATE_FOUNDED = timezone.now().replace(microsecond=0) - timezone.timedelta(days=100)
ESI_API_CORPORATION = {
    "ceo_id": 3004349,
    "creator_id": 1,
    "description": "test_description",
    "home_station_id": 1,
    "member_count": 861697,
    "name": "University of Caille",
    "shares": 100000000,
    "tax_rate": 0.11,
    "ticker": "UC"
}


class TestCorporationUpdateFromEsiAPi(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.corporation = corporation_create()

    def tearDown(self):
        self.corporation.delete()

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('esiapi.corporation.corporation')
    def test_corporation_update_from_esiapi__corporation__updated(self, mock_esiapi_corporation):

        mock_esiapi_corporation.return_value = ESI_API_CORPORATION, {}

        last_synced_at_unexpected = timezone.now()
        self.corporation.synced_at = last_synced_at_unexpected
        self.corporation.alliance_id = 10
        self.corporation.faction_id = 11
        self.corporation.url = 'http://corps.com'
        self.corporation.save()

        last_synced_at_result = self.corporation.update_from_esiapi()

        self.assertIsInstance(last_synced_at_result, timezone.datetime)
        self.assertNotEqual(last_synced_at_result, last_synced_at_unexpected)

        self.assertEqual(self.corporation.ceo_id, ESI_API_CORPORATION['ceo_id'])
        self.assertEqual(self.corporation.creator_id, ESI_API_CORPORATION['creator_id'])
        self.assertEqual(self.corporation.description, ESI_API_CORPORATION['description'])
        self.assertEqual(self.corporation.home_station_id, ESI_API_CORPORATION['home_station_id'])
        self.assertEqual(self.corporation.member_count, ESI_API_CORPORATION['member_count'])
        self.assertEqual(self.corporation.name, ESI_API_CORPORATION['name'])
        self.assertEqual(self.corporation.shares, ESI_API_CORPORATION['shares'])
        self.assertEqual(self.corporation.tax_rate, ESI_API_CORPORATION['tax_rate'])
        self.assertEqual(self.corporation.ticker, ESI_API_CORPORATION['ticker'])

        self.assertIsNone(self.corporation.alliance_id)
        self.assertIsNone(self.corporation.date_founded)
        self.assertIsNone(self.corporation.faction_id)
        self.assertIsNone(self.corporation.url)

    @mock.patch('esiapi.corporation.corporation')
    def test_corporation_update_from_esiapi__corporation_with_unrequired__changed(self, mock_esiapi_character):

        not_required_fields = {
            'alliance_id': 1001,
            'faction_id': 2,
            'date_founded': ESI_API_CORPORATION_DATE_FOUNDED.isoformat(),
            'url': 'http://corps.com',
        }
        mock_esiapi_character.return_value = {**ESI_API_CORPORATION, **not_required_fields}, {}

        last_synced_at_unexpected = timezone.now()
        self.corporation.synced_at = last_synced_at_unexpected
        self.corporation.save()

        last_synced_at_result = self.corporation.update_from_esiapi()

        self.assertIsInstance(last_synced_at_result, timezone.datetime)
        self.assertNotEqual(last_synced_at_result, last_synced_at_unexpected)

        self.assertEqual(self.corporation.alliance_id, not_required_fields['alliance_id'])
        self.assertEqual(self.corporation.faction_id, not_required_fields['faction_id'])
        self.assertEqual(self.corporation.date_founded, ESI_API_CORPORATION_DATE_FOUNDED)
        self.assertEqual(self.corporation.url, not_required_fields['url'])

    @mock.patch('esiapi.corporation.corporation', mock.MagicMock(return_value=({'error': 'test_error'}, {})))
    def test_corporation_update_from_esiapi__error__last_update_not_changed(self):

        last_synced_at_expected = timezone.now()
        self.corporation.last_update = last_synced_at_expected
        self.corporation.save()

        last_update_result = self.corporation.update_from_esiapi()

        self.assertIsNone(last_update_result)
