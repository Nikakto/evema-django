from rest_framework.test import APITestCase

from unittest import mock

from corporation.models import Corporation
from corporation.tasks import corporation_update_from_esiapi
from corporation.tests.models.corporation.test_update_from_esiapi import ESI_API_CORPORATION
from evema.utils.tests import corporation_create


class TestTaskCorporationUpdateFromEsiApi(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.corporation = corporation_create()

    def tearDown(self):
        self.corporation.delete()

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('esiapi.corporation.corporation', mock.MagicMock(return_value=(ESI_API_CORPORATION, {})))
    def test_task_corporation_update_from_esiapi__corporation_does_not_exist__Corporation(self):

        corporation_id = self.corporation.id + 10000
        self.assertIsNone(Corporation.objects.filter(id=corporation_id).first())

        corporation = corporation_update_from_esiapi(corporation_id)

        self.assertEqual(corporation.id, corporation_id)

        corporation.delete()

    @mock.patch('esiapi.corporation.corporation', mock.MagicMock(return_value=(ESI_API_CORPORATION, {})))
    def test_task_corporation_update_from_esiapi__corporation_exist__Corporation(self):

        corporation = corporation_update_from_esiapi(self.corporation.id)

        self.assertEqual(self.corporation, corporation)

    @mock.patch('corporation.tasks.corporation_update_from_esiapi.retry', mock.MagicMock())
    @mock.patch('corporation.models.Corporation.update_from_esiapi', mock.MagicMock(return_value=False))
    def test_task_corporation_update_from_esiapi__corporation_update_error__None(self):

        self.assertRaises(ConnectionError, corporation_update_from_esiapi, self.corporation.id)
