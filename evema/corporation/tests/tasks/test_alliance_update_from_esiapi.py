from rest_framework.test import APITestCase

from unittest import mock

from corporation.models import Alliance
from corporation.tasks import alliance_update_from_esiapi
from corporation.tests.models.alliance.test_update_from_esiapi import ESI_API_ALLIANCE
from evema.utils.tests import alliance_create


class TestTaskAllianceUpdateFromEsiApi(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.alliance = alliance_create()

    def tearDown(self):
        self.alliance.delete()

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('esiapi.alliances.alliance', mock.MagicMock(return_value=(ESI_API_ALLIANCE, {})))
    def test_task_alliance_update_from_esiapi__alliance_does_not_exist__Alliance(self):

        alliance_id = self.alliance.id + 10000
        self.assertIsNone(Alliance.objects.filter(id=alliance_id).first())

        alliance = alliance_update_from_esiapi(alliance_id)

        self.assertEqual(alliance.id, alliance_id)

        alliance.delete()

    @mock.patch('esiapi.alliances.alliance', mock.MagicMock(return_value=(ESI_API_ALLIANCE, {})))
    def test_task_alliance_update_from_esiapi__alliance_exist__Alliance(self):

        alliance = alliance_update_from_esiapi(self.alliance.id)

        self.assertEqual(self.alliance, alliance)

    @mock.patch('corporation.tasks.alliance_update_from_esiapi.retry', mock.MagicMock())
    @mock.patch('corporation.models.Alliance.update_from_esiapi', mock.MagicMock(return_value=False))
    def test_task_alliance_update_from_esiapi__alliance_update_error__None(self):

        self.assertRaises(ConnectionError, alliance_update_from_esiapi, self.alliance.id)
