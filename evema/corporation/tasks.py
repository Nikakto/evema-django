import logging
from celery import group, shared_task

from character.models import Character
from corporation.models import Alliance, Corporation
from evema.celery import app

logger = logging.getLogger(__name__)


# ======================================================================================================================
# UPDATE ALLIANCES FROM ESI API
# ======================================================================================================================

@shared_task(default_retry_delay=60, time_limit=15)
def alliance_update_from_esiapi(alliance_id):

    alliance = Alliance.objects.filter(id=alliance_id).first() or Alliance(id=alliance_id)
    if not alliance.update_from_esiapi():
        raise ConnectionError(f'ESI API: Can\'t get alliance {alliance_id}')

    return alliance


@app.task
def alliances_update_from_esiapi():

    alliances_ids = Alliance.objects.values_list('id', flat=True)

    alliances_ids = alliances_ids.union(
        Character.objects.exclude(alliance_id__in=alliances_ids).values_list('alliance_id', flat=True),
        Corporation.objects.exclude(alliance_id__in=alliances_ids).values_list('alliance_id', flat=True),
    )

    subtasks = group(alliance_update_from_esiapi.s(alliance_id) for alliance_id in alliances_ids)
    subtasks.delay()

    return list(alliances_ids)


# ======================================================================================================================
# UPDATE CORPORATIONS FROM ESI API
# ======================================================================================================================

@shared_task(default_retry_delay=60, time_limit=15)
def corporation_update_from_esiapi(corporation_id):

    corporation = Corporation.objects.filter(id=corporation_id).first() or Corporation(id=corporation_id)
    if not corporation.update_from_esiapi():
        raise ConnectionError(f'ESI API: Can\'t get corporation {corporation_id}')

    return corporation


@app.task
def corporations_update_from_esiapi():

    corporations_ids = Corporation.objects.values_list('id', flat=True)

    corporations_ids = corporations_ids.union(
        Character.objects.exclude(corporation_id__in=corporations_ids).values_list('corporation_id', flat=True),
        Alliance.objects.exclude(creator_corporation_id__in=corporations_ids).values_list('creator_corporation_id', flat=True),
        Alliance.objects.exclude(executor_corporation_id__in=corporations_ids).values_list('executor_corporation_id', flat=True),
    )

    subtasks = group(corporation_update_from_esiapi.s(corporation_id) for corporation_id in corporations_ids)
    subtasks.delay()

    return list(corporations_ids)
