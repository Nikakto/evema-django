# Generated by Django 2.1.2 on 2018-11-09 17:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corporation', '0003_auto_20181109_1746'),
    ]

    operations = [
        migrations.AlterField(
            model_name='corporation',
            name='shares',
            field=models.BigIntegerField(),
        ),
    ]
