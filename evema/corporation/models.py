from django.db import models
from django.utils import dateparse, timezone

import esiapi


class Alliance(models.Model):

    id = models.BigAutoField(primary_key=True)

    creator_corporation = models.ForeignKey('Corporation', db_constraint=False, on_delete=models.PROTECT, related_name='alliances_creator')
    creator = models.ForeignKey('character.Character', db_constraint=False, on_delete=models.PROTECT,  related_name='alliances_creator')
    date_founded = models.DateTimeField(null=True)
    executor_corporation = models.ForeignKey('Corporation', db_constraint=False, null=True, on_delete=models.PROTECT, related_name='alliances_executor')
    faction_id = models.IntegerField(null=True)
    name = models.CharField(max_length=128)
    synced_at = models.DateTimeField(null=True)
    ticker = models.CharField(max_length=32)

    def update_from_esiapi(self):

        alliance_data, headers = esiapi.alliances.alliance(self.id)

        if 'error' in alliance_data:
            return None

        # required fields
        self.creator_corporation_id = alliance_data['creator_corporation_id']
        self.creator_id = alliance_data['creator_id']
        self.name = alliance_data['name']
        self.ticker = alliance_data['ticker']

        # nullable fields
        self.executor_corporation_id = alliance_data.get('executor_corporation_id')
        self.faction_id = alliance_data.get('faction_id')

        if 'date_founded' in alliance_data:
            self.date_founded = dateparse.parse_datetime(alliance_data['date_founded'])

        self.synced_at = timezone.now()

        self.save()

        return self.synced_at


class Corporation(models.Model):

    id = models.BigAutoField(primary_key=True)

    alliance = models.ForeignKey('Alliance', db_constraint=False, null=True, on_delete=models.SET_NULL, related_name='corporations')
    ceo = models.ForeignKey('character.Character', db_constraint=False, on_delete=models.PROTECT, related_name='corporations_ceo')
    creator = models.ForeignKey('character.Character', db_constraint=False, on_delete=models.PROTECT, related_name='corporations_creator')
    date_founded = models.DateTimeField(null=True)
    description = models.TextField(blank=True)
    faction = models.IntegerField(null=True)
    home_station = models.ForeignKey('universe.Station', db_constraint=False, on_delete=models.PROTECT, related_name='corporations_home')
    member_count = models.IntegerField()
    name = models.CharField(max_length=128)
    shares = models.BigIntegerField()
    tax_rate = models.DecimalField(max_digits=5, decimal_places=4)
    ticker = models.CharField(max_length=32)
    url = models.CharField(max_length=256, null=True)

    synced_at = models.DateTimeField(null=True)

    def update_from_esiapi(self):

        corporation_data, headers = esiapi.corporation.corporation(self.id)

        if 'error' in corporation_data:
            return None

        # required fields
        self.ceo_id = corporation_data['ceo_id']
        self.creator_id = corporation_data['creator_id']
        self.description = corporation_data['description']
        self.home_station_id = corporation_data['home_station_id']
        self.member_count = corporation_data['member_count']
        self.name = corporation_data['name']
        self.shares = corporation_data['shares']
        self.tax_rate = corporation_data['tax_rate']
        self.ticker = corporation_data['ticker']

        # nullable fields
        self.alliance_id = corporation_data.get('alliance_id')
        self.faction_id = corporation_data.get('faction_id')
        self.url = corporation_data.get('url')

        if 'date_founded' in corporation_data:
            self.date_founded = dateparse.parse_datetime(corporation_data['date_founded'])

        self.synced_at = timezone.now()

        self.save()

        return self.synced_at
