from django.urls import path

from corporation.views import CorporationMarketOrdersView

urlpatterns = [
    path('orders/', CorporationMarketOrdersView.as_view(), name='orders'),
]
