from rest_framework.permissions import IsAuthenticated

from character.permissions import ReadCorporationOrdersV1
from market.views import MarketOrdersView


class CorporationMarketOrdersView(MarketOrdersView):

    permission_classes = (IsAuthenticated, ReadCorporationOrdersV1)

    def get_queryset(self):
        return super().get_queryset().filter(corporation_id=self.request.user.character.corporation_id)
