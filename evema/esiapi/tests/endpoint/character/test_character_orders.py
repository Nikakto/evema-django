from django.core.cache import caches
from rest_framework.test import APITestCase

from unittest import mock

from esiapi.endpoint.market import characters_orders
from evema.utils.tests import character_create, character_remove

cache = caches['esiapi']
esiapi_orders = ['order_1', 'order_2']


class TestEsiApiCharacterOrders(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):

        self.user, self.token, self.character, self.character_token = character_create()
        self.client.force_authenticate(user=self.user, token=self.token)

        self.redis_key = f'characters_orders__{self.character.id}_{self.character_token.access_token}'

    def tearDown(self):

        self.client.logout()
        character_remove(self.user, self.token, self.character, self.character_token)

        cache.clear()

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    @mock.patch('esiapi.endpoint.market.make_request', mock.MagicMock(return_value=(esiapi_orders, {})))
    def test_esiapi_character_orders__get__list_orders(self):

        orders_answer, headers = characters_orders(character_id=self.character.id, token=self.character_token)

        self.assertListEqual(sorted(orders_answer), sorted(esiapi_orders))

    @mock.patch('esiapi.endpoint.market.make_request', mock.MagicMock(return_value=({'error': 'test_error'}, {})))
    def test_esiapi_character_orders__esiapi_error__dict_with_error(self):

        orders_answer, headers = characters_orders(self.character.id, self.character_token)

        self.assertIn('error', orders_answer)
