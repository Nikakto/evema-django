from rest_framework.test import APISimpleTestCase

from esiapi.auth import refresh_token


class TestRefreshToken(APISimpleTestCase):

    def test_esiapi_auth_refresh_token__no_code__None(self):

        result = refresh_token(None)

        self.assertIsNone(result)
