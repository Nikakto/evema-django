# from django.core.cache import cache
# from django.utils import timezone
# from rest_framework.test import APITestCase
#
# from unittest import mock
#
# from esiapi.tools import make_request
# from evema.utils.tests import character_create, character_remove
#
#
# class TestMakeRequest(APITestCase):
#
#     url = '/health'
#
#     # ==================================================================================================================
#     # SET UP AND TEARDOWN
#     # ==================================================================================================================
#
#     def setUp(self):
#
#         self.user, self.token, self.character, self.character.token = character_create()
#         self.redis_key = f'esiapi_timeout'
#
#     def tearDown(self):
#
#         character_remove(self.user, self.token, self.character, self.character.token)
#         cache.clear()
#
#     # ==================================================================================================================
#     # TESTS
#     # ==================================================================================================================
#
#     @mock.patch('esiapi.tools.requests.request')
#     def test_make_request__get_error_500_retry_200__return_error(self, mock_request):
#
#         request_first = mock.MagicMock()
#         request_first.status_code = 500
#
#         request_second = mock.MagicMock()
#         request_second.status_code = 200
#         request_second.headers = {}
#         request_second.json.return_value = 'result'
#
#         mock_request.side_effect = (request_first, request_second)
#
#         result, headers = make_request(self.url)
#
#         self.assertEqual(result, 'result')
#
#     @mock.patch('esiapi.tools.requests.request')
#     def test_make_request__get_error_500_retry_500__return_error(self, mock_request):
#
#         mock_request.status_code = 500
#         mock_request.return_value = {'x-esi-error-limit-remain': 100}
#
#         result, headers = make_request(self.url)
#
#         self.assertIsInstance(headers, dict)
#         self.assertIn('error', result)
#
#         assert False
#
#     @mock.patch('esiapi.tools.requests.request')
#     def test_make_request__get_error_400__return_error(self, mock_request):
#
#         mock_request.status_code = 400
#         mock_request.return_value = {'error': 'test_error'}, {'x-esi-error-limit-remain': 100}
#
#         result, headers = make_request(self.url)
#
#         self.assertIsInstance(headers, dict)
#         self.assertIn('error', result)
#
#     @mock.patch('esiapi.tools.requests.request')
#     def test_make_request__get_error_limit__set_cache_timeout_return_error(self, mock_request):
#
#         mock_request.return_value = {'error': 'test_error'}, {'x-esi-error-limit-remain': 49}
#
#         result, headers = make_request(self.url)
#
#         self.assertIsInstance(headers, dict)
#         self.assertIn('error', result)
#
#     def test_make_request__cache_timeout__return_error(self):
#         raise RuntimeError('write test')
