from rest_framework.test import APITestCase

from unittest import mock

from corporation.models import Corporation
from esiapi import harvest
from evema.utils.tests import character_create, corporation_create


class TestHarvestOrdersCorporationOrders(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def test_harvest_orders_corporation_orders__corporation_not_found__message(self):

        corporation_id = Corporation.objects.count() and Corporation.objects.latest('id').id + 1

        result = harvest.orders.harvest_corporation(corporation_id=corporation_id)

        self.assertIsInstance(result, dict)
        self.assertIn('message', result)

    def test_harvest_orders_corporation_orders__corporation_characters_has_no_permission__message(self):

        corporation = corporation_create()

        user, token, character, character_token = character_create()
        character.corporation = corporation
        character.save()

        character_token.scopes = []
        character_token.save()

        result = harvest.orders.harvest_corporation(corporation_id=corporation.id)

        self.assertIsInstance(result, dict)
        self.assertIn('message', result)

    def test_harvest_orders_corporation_orders__corporation_characters_tokens_expired__message(self):

        corporation = corporation_create()

        user, token, character, character_token = character_create()
        character.corporation = corporation
        character.save()

        character_token.scopes = ['esi-markets.read_corporation_orders.v1']
        character_token.expired = True
        character_token.save()

        result = harvest.orders.harvest_corporation(corporation_id=corporation.id)

        self.assertIsInstance(result, dict)
        self.assertIn('message', result)

    @mock.patch('esiapi.market.corporation_orders', mock.MagicMock(return_value=([], {})))
    def test_harvest_orders_corporation_orders__corporation_characters_first_expired_second_ok__processing_corporation_once(self):

        corporation = corporation_create()

        # character 0

        user_0, token_0, character_0, character_token_0 = character_create(username='test_username_0')
        character_0.corporation = corporation
        character_0.save()

        character_token_0.scopes = ['esi-markets.read_corporation_orders.v1']
        character_token_0.expired = True
        character_token_0.save()

        # character 1

        user_1, token_1, character_1, character_token_1 = character_create(username='test_username_1')
        character_1.corporation = corporation
        character_1.save()

        character_token_1.scopes = ['esi-markets.read_corporation_orders.v1']
        character_token_1.expired = False
        character_token_1.save()

        with mock.patch('esiapi.harvest.orders.processing_corporation') as processing_corporation:
            result = harvest.orders.harvest_corporation(corporation_id=corporation.id)
            processing_corporation.assert_called_once()

        self.assertIsNone(result)

    def test_harvest_orders_corporation_orders__corporation_no_characters__message(self):

        corporation = corporation_create()

        result = harvest.orders.harvest_corporation(corporation_id=corporation.id)

        self.assertIsInstance(result, dict)
        self.assertIn('message', result)

    def test_harvest_orders_corporation_orders__esi_api_error__esiapi_answer(self):

        result_expected = {'error': 'test_error'}

        corporation = corporation_create()

        user_0, token_0, character_0, character_token_0 = character_create()
        character_0.corporation = corporation
        character_0.save()

        character_token_0.scopes = ['esi-markets.read_corporation_orders.v1']
        character_token_0.expired = False
        character_token_0.save()

        with mock.patch('esiapi.market.corporation_orders') as corporation_orders:
            corporation_orders.return_value = result_expected, {}
            result = harvest.orders.harvest_corporation(corporation_id=corporation.id)

        self.assertIsInstance(result, dict)
        self.assertIn('error', result)
        self.assertDictEqual(result, result_expected)

    @mock.patch('esiapi.market.corporation_orders', mock.MagicMock(return_value=([], {})))
    def test_harvest_orders_corporation_orders__characters_valid__processing_corporation_once(self):

        corporation = corporation_create()

        # character 0

        user_0, token_0, character_0, character_token_0 = character_create(username='test_username_0')
        character_0.corporation = corporation
        character_0.save()

        character_token_0.scopes = ['esi-markets.read_corporation_orders.v1']
        character_token_0.expired = False
        character_token_0.save()

        # character 1

        user_1, token_1, character_1, character_token_1 = character_create(username='test_username_1')
        character_1.corporation = corporation
        character_1.save()

        character_token_1.scopes = ['esi-markets.read_corporation_orders.v1']
        character_token_1.expired = False
        character_token_1.save()

        with mock.patch('esiapi.harvest.orders.processing_corporation') as processing_corporation:
            result = harvest.orders.harvest_corporation(corporation_id=corporation.id)
            processing_corporation.assert_called_once()

        self.assertIsNone(result)
