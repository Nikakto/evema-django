from django.utils import timezone
from rest_framework.test import APITestCase

from evema.utils.tests import character_create, corporation_create, market_order_create
from esiapi.harvest.orders import update
from market.models import MarketOrder
from market.tests.models.market_order.test_market_order_esiapi_parse import ESI_API_ORDER_DATA


class TestHarvestOrdersUpdate(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):

        self.esiapi_order_data = dict(ESI_API_ORDER_DATA)
        self.order = market_order_create(id=self.esiapi_order_data['order_id'])
        self.order.esiapi_update(self.esiapi_order_data)
        self.order.save()

    def tearDown(self):
        MarketOrder.objects.all().delete()

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_harvest_orders_update__nothing_changed__skip(self):

        result = update([self.esiapi_order_data])

        self.assertFalse(result)
        self.assertIsInstance(result, list)

    def test_harvest_orders_update__update_escrow__updated(self):

        escrow_expected = self.esiapi_order_data.get('escrow', 100) + 1

        self.esiapi_order_data['escrow'] = escrow_expected

        result = update([self.esiapi_order_data])
        self.order.refresh_from_db()

        self.assertTrue(result)
        self.assertEqual(self.order.escrow, escrow_expected)

    def test_harvest_orders_update__update_issued__updated(self):

        issued_expected = timezone.now().replace(microsecond=0)

        self.esiapi_order_data['issued'] = issued_expected.strftime('%Y-%m-%dT%H:%M:%SZ')

        result = update([self.esiapi_order_data])
        self.order.refresh_from_db()

        self.assertTrue(result)
        self.assertEqual(self.order.issued, issued_expected)
        self.assertEqual(self.order.expires_date, issued_expected + timezone.timedelta(days=self.order.duration))

    def test_harvest_orders_update__update_price__updated(self):

        price_expected = 12345.08

        self.esiapi_order_data['price'] = price_expected

        result = update([self.esiapi_order_data])
        self.order.refresh_from_db()

        self.assertTrue(result)
        self.assertEqual(float(self.order.price), price_expected)

    def test_harvest_orders_update__update_volume_remain__updated(self):

        volume_remain_expected = 12345

        self.esiapi_order_data['volume_remain'] = volume_remain_expected

        result = update([self.esiapi_order_data])
        self.order.refresh_from_db()

        self.assertTrue(result)
        self.assertEqual(self.order.volume_remain, volume_remain_expected)
