from django.utils import timezone
from rest_framework.test import APITestCase

from unittest import mock

from assets.models import ItemType
from esiapi import harvest
from evema.utils.tests import market_order_create
from market.models import MarketOrder
from market.tests.models.market_order.test_market_order_esiapi_parse import ESI_API_ORDER_DATA
from universe.models import Station, Region, System


class TestHarvestOrdersProcessingMarket(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.esiapi_market_order_data = dict(ESI_API_ORDER_DATA)

    def tearDown(self):
        MarketOrder.objects.all().delete()

    # ==================================================================================================================
    # TESTS FUNCTION
    # ==================================================================================================================

    @staticmethod
    def make_order(order_data):

        market_order = MarketOrder.esiapi_parse(order_data)
        market_order.region = Region.objects.first()

        updated_at = timezone.now() - timezone.timedelta(days=1)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            market_order.save()

        return market_order

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_harvest_order_processing_market__create__created(self):

        self.esiapi_market_order_data['type_id'] = ItemType.objects.first().id
        self.esiapi_market_order_data['location_id'] = Station.objects.first().id
        self.esiapi_market_order_data['system_id'] = System.objects.first().id

        harvest.orders.processing_market([self.esiapi_market_order_data], region_id=Region.objects.first().id)

        self.assertTrue(MarketOrder.objects.filter(id=self.esiapi_market_order_data['order_id'], public=True, removed__isnull=True))

    def test_harvest_order_processing_market__create_fk_error_item_type__skipped(self):

        self.esiapi_market_order_data['type_id'] = ItemType.objects.latest('id').id + 1

        harvest.orders.processing_market([self.esiapi_market_order_data], region_id=Region.objects.first().id)

        self.assertFalse(MarketOrder.objects.filter(id=self.esiapi_market_order_data['order_id']))

    def test_harvest_order_processing_market__create_fk_error_location_id__skipped(self):

        self.esiapi_market_order_data['location_id'] = Station.objects.latest('id').id + 1

        harvest.orders.processing_market([self.esiapi_market_order_data], region_id=Region.objects.first().id)

        self.assertFalse(MarketOrder.objects.filter(id=self.esiapi_market_order_data['order_id']))

    def test_harvest_order_processing_market__create_fk_error_system_id__skipped(self):

        self.esiapi_market_order_data['system_id'] = System.objects.latest('id').id + 1

        harvest.orders.processing_market([self.esiapi_market_order_data], region_id=Region.objects.first().id)

        self.assertFalse(MarketOrder.objects.filter(id=self.esiapi_market_order_data['order_id']))

    def test_harvest_order_processing_market__delete_public_False_updated_at_5_mins_ago__skipped(self):

        order = market_order_create(public=False)

        updated_at = timezone.now() - timezone.timedelta(minutes=5)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        harvest.orders.processing_market([], region_id=order.region_id)
        order.refresh_from_db()

        self.assertIsNone(order.removed)
        self.assertFalse(order.public)
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_market__delete_public_True_updated_at_4_mins_ago__skipped(self):

        order = market_order_create(public=True)

        updated_at = timezone.now() - timezone.timedelta(minutes=4)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        harvest.orders.processing_market([], region_id=order.region_id)
        order.refresh_from_db()

        self.assertIsNone(order.removed)
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_market__delete_public_True_updated_at_5_mins_ago__deleted(self):

        order = market_order_create(public=True)

        updated_at = timezone.now() - timezone.timedelta(minutes=5)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        harvest.orders.processing_market([], region_id=order.region_id)
        order.refresh_from_db()

        self.assertEqual(order.removed.replace(minute=0, second=0, microsecond=0),
                         timezone.now().replace(minute=0, second=0, microsecond=0))

    def test_harvest_order_processing_market__delete_market_region_id__other_region_skipped(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = True

        updated_at = timezone.now() - timezone.timedelta(minutes=5)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        harvest.orders.processing_market([self.esiapi_market_order_data], region_id=order.region_id + 1)
        order.refresh_from_db()

        self.assertIsNone(order.removed)
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_market__update_public_False_updated_at_0_mins_ago__public_True_and_skipped(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = False
        order.save()

        updated_at = order.updated_at

        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_market([self.esiapi_market_order_data], region_id=order.region_id)
        order.refresh_from_db()

        self.assertTrue(order.public)
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_market__update_public_True_updated_at_4_mins_ago__skipped(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = True

        updated_at = timezone.now() - timezone.timedelta(minutes=4)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_market([self.esiapi_market_order_data], region_id=order.region_id)
        order.refresh_from_db()

        self.assertTrue(order.public)

        self.assertNotEqual(order.volume_remain, self.esiapi_market_order_data['volume_remain'])
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_market__update_public_True_updated_at_5_mins_ago__updated(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = True

        updated_at = timezone.now() - timezone.timedelta(minutes=5)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_market([self.esiapi_market_order_data], region_id=order.region_id)
        order.refresh_from_db()

        self.assertTrue(order.public)

        self.assertEqual(order.volume_remain, self.esiapi_market_order_data['volume_remain'])
        self.assertNotEqual(order.updated_at, updated_at)
