"""
Please, do not laugh. I really make mistakes and should write tests :(
"""

from django.utils import timezone
from rest_framework.test import APITestCase

from evema.utils.tests import market_order_create
from esiapi import harvest
from market.models import MarketOrder


class TestHarvestOrdersDelete(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.order = market_order_create()

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_harvest_orders_delete__removed_None__skipped(self):

        self.order.removed = None
        self.order.save()

        harvest.orders.delete()

        self.assertTrue(MarketOrder.objects.filter(id=self.order.id).exists())

    def test_harvest_orders_delete__removed_2_hours_passed__deleted(self):

        self.order.removed = timezone.now() - timezone.timedelta(hours=2)
        self.order.save()

        harvest.orders.delete()

        self.assertFalse(MarketOrder.objects.filter(id=self.order.id).exists())

    def test_harvest_orders_delete__removed_2_hours_not_passed__skipped(self):

        self.order.removed = timezone.now()
        self.order.save()

        harvest.orders.delete()

        self.assertTrue(MarketOrder.objects.filter(id=self.order.id).exists())
