from rest_framework.test import APITestCase

from esiapi.harvest.orders import comparable
from market.tests.models.market_order.test_market_order_esiapi_parse import ESI_API_ORDER_DATA, ESI_API_ORDER_DATA_ISSUED


class TestHarvestOrdersComparable(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.esiapi_order_data = dict(ESI_API_ORDER_DATA)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_comparable__empty__empty(self):

        data = list(comparable([]))

        self.assertFalse(data)

    def test_comparable__market_order__comparable(self):

        data = list(comparable([self.esiapi_order_data, self.esiapi_order_data, self.esiapi_order_data]))
        _id, escrow, price, issued, volume_remain = data[0]

        # order is required. Look at esiapi.harvest.orders.update (price should be wrapped by float)
        self.assertEqual(_id, self.esiapi_order_data['order_id'])
        self.assertIsNone(escrow)
        self.assertEqual(price, self.esiapi_order_data['price'])
        self.assertEqual(issued, ESI_API_ORDER_DATA_ISSUED)
        self.assertEqual(volume_remain, self.esiapi_order_data['volume_remain'])

    def test_comparable__market_order_escrow__comparable(self):

        escrow_expected = self.esiapi_order_data.get('escrow', 100) + 1
        self.esiapi_order_data['escrow'] = escrow_expected

        data = list(comparable([self.esiapi_order_data, self.esiapi_order_data, self.esiapi_order_data]))
        _id, escrow, price, issued, volume_remain = data[0]

        self.assertEqual(escrow, escrow_expected)
