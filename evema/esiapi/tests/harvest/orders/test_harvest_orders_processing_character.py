from django.utils import timezone
from rest_framework.test import APITestCase

from unittest import mock

from assets.models import ItemType
from corporation.models import Corporation
from esiapi import harvest
from evema.utils.tests import character_create, character_remove, corporation_create, market_order_create
from market.models import MarketOrder
from market.tests.models.market_order.test_market_order_esiapi_parse import ESI_API_ORDER_DATA
from universe.models import Station, Region, System


class TestHarvestOrdersProcessingCharacter(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',

    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):

        self.corporation = corporation_create()

        self.user, self.token, self.character, self.character_token = character_create()
        self.character.corporation = self.corporation
        self.character.save()

        self.esiapi_market_order_data = dict(ESI_API_ORDER_DATA)
        self.esiapi_market_order_data['is_corporation'] = False

        del self.esiapi_market_order_data['system_id']

    def tearDown(self):

        MarketOrder.objects.all().delete()
        character_remove(self.user, self.token, self.character, self.character_token)

    # ==================================================================================================================
    # TESTS FUNCTION
    # ==================================================================================================================

    @staticmethod
    def make_order(order_data):

        market_order = MarketOrder.esiapi_parse(order_data)
        market_order.system = System.objects.first()
        market_order.region = market_order.system.region
        market_order.save()

        return market_order

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_harvest_order_processing_character__create__created(self):

        self.esiapi_market_order_data['type_id'] = ItemType.objects.first().id
        self.esiapi_market_order_data['location_id'] = Station.objects.first().id
        self.esiapi_market_order_data['system_id'] = System.objects.first().id

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)
        order = MarketOrder.objects.get(id=self.esiapi_market_order_data['order_id'], character_id=self.character.id)

        self.assertFalse(order.public)
        self.assertEqual(order.character, self.character)
        self.assertIsNone(order.removed)

    def test_harvest_order_processing_character__create_fk_error_item_type__skipped(self):

        self.esiapi_market_order_data['type_id'] = ItemType.objects.latest('id').id + 1

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)

        self.assertFalse(MarketOrder.objects.filter(id=self.esiapi_market_order_data['order_id']))

    def test_harvest_order_processing_character__create_fk_error_location_id__skipped(self):

        self.esiapi_market_order_data['location_id'] = Station.objects.latest('id').id + 1

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)

        self.assertFalse(MarketOrder.objects.filter(id=self.esiapi_market_order_data['order_id']))

    def test_harvest_order_processing_character__delete_public_False_updated_at_59_mins_ago__skipped(self):

        order = market_order_create(character_id=self.character.id, public=False)

        updated_at = timezone.now() - timezone.timedelta(minutes=59)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        harvest.orders.processing_character([], character_id=self.character.id)
        order.refresh_from_db()

        self.assertIsNone(order.removed)

    def test_harvest_order_processing_character__delete_public_True_updated_at_59_mins_ago__skipped(self):

        order = market_order_create(character_id=self.character.id, public=True)

        updated_at = timezone.now() - timezone.timedelta(minutes=59)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        harvest.orders.processing_character([], character_id=self.character.id)
        order.refresh_from_db()

        self.assertIsNone(order.removed)

    def test_harvest_order_processing_character__delete_public_False_updated_at_60_mins_ago__removed(self):

        order = market_order_create(character_id=self.character.id, public=False)

        updated_at = timezone.now() - timezone.timedelta(minutes=60)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        harvest.orders.processing_character([], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.removed.replace(minute=0, second=0, microsecond=0),
                         timezone.now().replace(minute=0, second=0, microsecond=0))

    def test_harvest_order_processing_character__delete_public_True_updated_at_60_mins_ago__removed(self):

        order = market_order_create(character_id=self.character.id, public=True)

        updated_at = timezone.now() - timezone.timedelta(minutes=60)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        harvest.orders.processing_character([], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.removed.replace(minute=0, second=0, microsecond=0),
                         timezone.now().replace(minute=0, second=0, microsecond=0))

    def test_harvest_order_processing_character__update_character_not_set_updated_at_0_mins_ago__set_character_and_skipped(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = True
        order.save()

        updated_at = order.updated_at

        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.character_id, self.character.id)
        self.assertIsNone(order.corporation_id)

        self.assertNotEqual(order.volume_remain, self.esiapi_market_order_data['volume_remain'])
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_character__update_corporation_not_set_updated_at_0_mins_ago__set_corporation_and_skipped(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = True
        order.save()

        updated_at = order.updated_at

        self.esiapi_market_order_data['is_corporation'] = True
        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.character_id, self.character.id)
        self.assertEqual(order.corporation_id, self.character.corporation.id)

        self.assertNotEqual(order.volume_remain, self.esiapi_market_order_data['volume_remain'])
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_character__update_corporation_not_set_corporation_not_exist__set_corporation_and_skipped(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = True
        order.save()

        self.character.corporation_id = Corporation.objects.latest('id').id + 1
        self.character.save()

        updated_at = order.updated_at

        self.esiapi_market_order_data['is_corporation'] = True
        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.character_id, self.character.id)
        self.assertEqual(order.corporation_id, self.character.corporation_id)

        self.assertNotEqual(order.volume_remain, self.esiapi_market_order_data['volume_remain'])
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_character__update_not_changed__skipped(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = True

        updated_at = timezone.now() - timezone.timedelta(minutes=60)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.character_id, self.character.id)
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_character__update_public_False_updated_at_59_mins_ago__skipped(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = False

        updated_at = timezone.now() - timezone.timedelta(minutes=59)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.character_id, self.character.id)
        self.assertNotEqual(order.volume_remain, self.esiapi_market_order_data['volume_remain'])
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_character__update_public_True_updated_at_59_mins_ago__skipped(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = True

        updated_at = timezone.now() - timezone.timedelta(minutes=59)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.character_id, self.character.id)
        self.assertNotEqual(order.volume_remain, self.esiapi_market_order_data['volume_remain'])
        self.assertEqual(order.updated_at, updated_at)

    def test_harvest_order_processing_character__update_public_False_updated_at_60_mins_ago__updated(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = False

        updated_at = timezone.now() - timezone.timedelta(minutes=60)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.character_id, self.character.id)
        self.assertEqual(order.volume_remain, self.esiapi_market_order_data['volume_remain'])

    def test_harvest_order_processing_character__update_public_True_updated_at_60_mins_ago__updated(self):

        order = self.make_order(self.esiapi_market_order_data)
        order.public = True

        updated_at = timezone.now() - timezone.timedelta(minutes=60)
        with mock.patch('django.utils.timezone.now', mock.MagicMock(return_value=updated_at)):
            order.save()

        self.esiapi_market_order_data['volume_remain'] -= 1

        harvest.orders.processing_character([self.esiapi_market_order_data], character_id=self.character.id)
        order.refresh_from_db()

        self.assertEqual(order.character_id, self.character.id)
        self.assertEqual(order.volume_remain, self.esiapi_market_order_data['volume_remain'])
