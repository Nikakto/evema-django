from django.conf import settings

import requests
import time
import urllib.parse as urlparse
from urllib.parse import urlencode


def auth_link(state=''):

    params = {
        'client_id': settings.EVE_OAUTH_CLIENT_ID,
        'redirect_uri': settings.EVE_OAUTH_REDIRECT_URI,
        'response_type': settings.EVE_OAUTH_RESPONSE_TYPE,
    }

    url_parsed = list(urlparse.urlparse(settings.EVE_OAUTH_LOGIN_URL))

    query = dict(urlparse.parse_qsl(url_parsed[4]))
    query.update(params)
    url_parsed[4] = urlencode(query)

    return urlparse.urlunparse(url_parsed)


def obtain_character(code, token_type='Bearer', host='login.eveonline.com'):

    headers = {
        'Authorization': '%s %s' % (token_type, code),
        'Host': host
    }

    request = requests.get(settings.EVE_OAUTH_VERIFY_URL, headers=headers)
    if request.status_code == 200:
        return request.json()
    else:
        return None


def refresh_token(code, host='login.eveonline.com', retry=1):

    if not code:
        return None

    auth = (settings.EVE_OAUTH_CLIENT_ID, settings.EVE_OAUTH_SECRET_KEY)

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Host': host,
    }

    payload = {
        "grant_type": 'refresh_token',
        "refresh_token": code,
    }

    response = requests.post(settings.EVE_OAUTH_TOKEN_URL, auth=auth, headers=headers, params=payload)
    if response.status_code == 200:
        return response.json()
    elif response.status_code >= 500 and retry < settings.OTHER_RETRY_LIMIT:
        time.sleep(1)
        return refresh_token(code=code, host=host, retry=retry+1)

    return None


def revoke_token(code, host='login.eveonline.com'):

    auth = (settings.EVE_OAUTH_CLIENT_ID, settings.EVE_OAUTH_SECRET_KEY)
    headers = {'Host': host}
    json_data = {
        "token_type_hint": "access_token",
        "code": code,
    }

    response = requests.post(settings.EVE_OAUTH_REVOKE_URL, auth=auth, json=json_data, headers=headers)
    return response.status_code == 200


def verify_token(code, host='login.eveonline.com'):

    auth = (settings.EVE_OAUTH_CLIENT_ID, settings.EVE_OAUTH_SECRET_KEY)
    headers = {'Host': host}
    json_data = {
        "grant_type": 'authorization_code',
        "code": code,
    }

    response = requests.post(settings.EVE_OAUTH_TOKEN_URL, auth=auth, json=json_data, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        return None
