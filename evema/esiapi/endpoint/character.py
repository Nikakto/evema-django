from django.conf import settings

from esiapi.tools import make_request


def character(character_id):

    formatter = {
        'character_id': character_id,
    }

    url = settings.ESI_API_ENDPOINTS['CHARACTER']['CHARACTER']
    url = url.format(**formatter)
    return make_request(url)


def portrait(character_id):

    formatter = {
        'character_id': character_id,
    }

    url = settings.ESI_API_ENDPOINTS['CHARACTER']['PORTRAIT']
    url = url.format(**formatter)
    return make_request(url)
