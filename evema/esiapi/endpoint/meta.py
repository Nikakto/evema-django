from django.conf import settings

from esiapi.tools import make_request


def get_verify(character_id, token=None):

    url = settings.ESI_API_ENDPOINTS['META']['VERIFY']
    url = url.format(character_id=character_id)
    return make_request(url, 'GET', token=token)
