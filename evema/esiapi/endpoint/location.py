from django.conf import settings

from esiapi.tools import make_request


def location(character_id, token):

    formatter = {
        'character_id': character_id,
    }

    url = settings.ESI_API_ENDPOINTS['LOCATION']['LOCATION']
    url = url.format(**formatter)
    return make_request(url, 'GET', token=token)


def online(character_id, token):

    formatter = {
        'character_id': character_id,
    }

    url = settings.ESI_API_ENDPOINTS['LOCATION']['ONLINE']
    url = url.format(**formatter)
    return make_request(url, 'GET', token=character_token)
