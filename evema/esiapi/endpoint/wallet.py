from django.conf import settings

from esiapi.tools import make_request


def wallet(character_id, token):

    url = settings.ESI_API_ENDPOINTS['WALLET']['WALLET']
    url = url.format(character_id=character_id)
    return make_request(url, 'GET', token=token)
