from django.conf import settings

from esiapi.tools import make_request


def corporation(corporation_id):

    formatter = {
        'corporation_id': corporation_id,
    }

    url = settings.ESI_API_ENDPOINTS['CORPORATION']['CORPORATION']
    url = url.format(**formatter)
    return make_request(url)


def icons(corporation_id):

    formatter = {
        'corporation_id': corporation_id,
    }

    url = settings.ESI_API_ENDPOINTS['CORPORATION']['ICONS']
    url = url.format(**formatter)
    return make_request(url)


def names():

    url = settings.ESI_API_ENDPOINTS['CORPORATION']['NAMES']
    return make_request(url)
