from django.conf import settings

from esiapi.tools import make_request


def characters_orders(character_id, token=None):

    url = settings.ESI_API_ENDPOINTS['MARKET']['CHARACTER_ORDERS']
    url = url.format(character_id=character_id)
    return make_request(url, 'GET', token=token)


def corporation_orders(corporation_id, token=None):

    url = settings.ESI_API_ENDPOINTS['MARKET']['CORPORATION_ORDERS']
    url = url.format(corporation_id=corporation_id)
    return make_request(url, 'GET', token=token)


def market_group(market_group_id):

    url = settings.ESI_API_ENDPOINTS['MARKET']['MARKET_GROUP']
    url = url.format(market_group_id=market_group_id)
    return make_request(url, 'GET')


def market_groups():

    url = settings.ESI_API_ENDPOINTS['MARKET']['MARKET_GROUPS']
    return make_request(url, 'GET')


def orders(region_id, item_type_id=None, order_type='all', *, page=1):

    params = {
        'page': page,
        'order_type': order_type,
    }

    if item_type_id:
        params['type_id'] = item_type_id

    url = settings.ESI_API_ENDPOINTS['MARKET']['ORDERS']
    url = url.format(region_id=region_id)
    return make_request(url, 'GET', **params)
