from django.conf import settings

from esiapi.tools import make_request, pages


def category(category_id, ):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['CATEGORY']
    url = url.format(category_id=category_id)
    return make_request(url, 'GET')


def categories():

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['CATEGORIES']
    return make_request(url, 'GET')


def constellation(constellation_id, ):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['CONSTELLATION']
    url = url.format(constellation_id=constellation_id)
    return make_request(url, 'GET')


def constellations():

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['CONSTELLATIONS']
    return make_request(url, 'GET')


def group(group_id):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['GROUP']
    url = url.format(group_id=group_id)
    return make_request(url, 'GET')


@pages
def groups(page=1):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['GROUPS']
    return make_request(url, 'GET', page=page)


def region(region_id, ):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['REGION']
    url = url.format(region_id=region_id)
    return make_request(url, 'GET')


def regions():

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['REGIONS']
    return make_request(url, 'GET')


def stargate(stargate_id):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['STARGATE']
    url = url.format(stargate_id=stargate_id)
    return make_request(url, 'GET')


def station(station_id):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['STATION']
    url = url.format(station_id=station_id)
    return make_request(url, 'GET')


def structure(structure_id, token):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['STRUCTURE']
    url = url.format(structure_id=structure_id)
    return make_request(url, 'GET', token=token)


def structures():

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['STRUCTURES']
    return make_request(url, 'GET')


def system(system_id):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['SYSTEM']
    url = url.format(system_id=system_id)
    return make_request(url, 'GET')


def systems():

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['SYSTEMS']
    return make_request(url, 'GET')


def type(type_id):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['TYPE']
    url = url.format(type_id=type_id)
    return make_request(url, 'GET')


@pages
def types(page=1):

    url = settings.ESI_API_ENDPOINTS['UNIVERSE']['TYPES']
    return make_request(url, 'GET', page=page)
