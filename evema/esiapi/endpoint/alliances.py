from django.conf import settings

from esiapi.tools import make_request


def alliance(alliance_id):

    formatter = {
        'alliance_id': alliance_id,
    }

    url = settings.ESI_API_ENDPOINTS['ALLIANCES']['ALLIANCE']
    url = url.format(**formatter)
    return make_request(url)
