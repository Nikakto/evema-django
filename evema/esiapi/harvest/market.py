from django.db import transaction

import tqdm

import esiapi
from esiapi.harvest._tools import bar_format, harvest_threading, harvesting
from market.models import MarketGroup


@harvesting
def groups():

    print('\n\nHarvesting market groups:', flush=True)

    # collect
    print('%-46s' % 'Get list', end='', flush=True)
    market_groups_list, headers = esiapi.market.market_groups()
    print('Done', flush=True)

    # harvest
    harvest_data = harvest_threading(esiapi.market.market_group, market_groups_list)

    # processing

    _tqdm = tqdm.tqdm(harvest_data, unit='groups', desc='Processing', bar_format=bar_format, ncols=120)
    for data, headers in _tqdm:

        market_group = MarketGroup()
        market_group.id = data['market_group_id']
        market_group.name = data['name']
        market_group.description = data['description']
        market_group.parent_group_id = data.get('parent_group_id')

        market_group.save()

    # save to db
    print('%-46s' % 'Save to db:', end='', flush=True)
    transaction.commit()
    print('Done', flush=True)
