from django.conf import settings
from django.db import connection
from django.db.models import Case, Q, Value, When
from django.utils import dateparse, timezone

import itertools
import logging
import time
from multiprocessing.pool import ThreadPool

import esiapi
from assets.models import ItemType
from character.models import Character
from corporation.models import Corporation
from esiapi.harvest._tools import bar_format, harvest_timer, harvest_progressbar
from market.models import MarketOrder
from universe.models import Region, Station, System

logger = logging.getLogger(__name__)


@harvest_timer
def harvest_character(character_id):

    print(f'\n\nHarvesting character <{character_id}>:', flush=True)

    character = Character.objects.filter(id=character_id).prefetch_related('token').first()
    if not character:
        message = f'Character <id={character_id}> not found'
        print(message, flush=True)
        return {'message': message}

    if not character.token:
        message = f'Character <id={character_id}> "{character.name}" hasn\'t token'
        print(message, flush=True)
        return {'message': message}

    if character.token.expired:
        message = f'Character <id={character_id}> "{character.name}" token expired'
        print(message, flush=True)
        return {'message': message}

    if 'esi-markets.read_character_orders.v1' not in character.token.scopes:
        message = f'Character <id={character_id}> "{character.name}" token hasn\'t permission \'esi-markets.read_character_orders.v1\''
        print(message, flush=True)
        return {'message': message}

    result, headers = esiapi.market.characters_orders(character.id, character.token)
    if 'error' in result:

        if character.token.expired:
            message = f'Character <id={character_id}> "{character.name}" token expired'
            print(message, flush=True)
            return {'message': message}

        return result

    processing_character(result, character_id)


@harvest_timer
def harvest_corporation(corporation_id):

    print(f'\n\nHarvesting corporation <{corporation_id}>:', flush=True)

    corporation = Corporation.objects.filter(id=corporation_id).first()
    if not corporation:
        message = f'Corporation <id={corporation_id}> not found'
        print(message, flush=True)
        return {'message': message}

    # ordering only for tests
    characters = corporation.characters.\
        filter(token__expired=False, token__scopes__contains=['esi-markets.read_corporation_orders.v1']).\
        order_by('id').\
        prefetch_related('token')

    for character in characters:

        result, headers = esiapi.market.corporation_orders(corporation.id, character.token)
        if 'error' in result:

            if character.token.expired:
                message = f'Character <id={character.id}> "{character.name}" token expired'
                print(message, flush=True)
                continue

            return result

        processing_corporation(result, corporation_id)
        return

    message = f'Corporation <id={corporation_id}> "{corporation.name}" has no characters to load it'
    print(message, flush=True)
    return {'message': message}


@harvest_timer
def harvest_market(region_id):

    region = Region.objects.filter(id=region_id).first()
    if not region:
        message = 'Unknown region id {region_id}'
        print(message, flush=True)
        return {'error': message}

    print(f'\n\nHarvesting orders <{region.name}>:', flush=True)

    # pages
    print('%-46s' % 'Get count of pages', end='', flush=True)
    orders_list, headers = esiapi.market.orders(region_id=region_id, page=1)
    pages = int(headers.get('x-pages', 0)) if orders_list else 0
    print(pages, flush=True)

    if not pages:
        message = 'No orders.'
        print(message)
        return {'error': message}

    # load pages
    pool = ThreadPool(processes=settings.OTHER_REQUEST_THREADS)
    harvest_threads = [pool.apply_async(region_page, kwds={'region_id': region_id, 'page': page}) for page in range(1, pages+1)]
    harvest_progressbar(harvest_threads, unit='requests', desc='Load orders from ESI API and process:', bar_format=bar_format, ncols=120)
    harvest_data = list(itertools.chain(*[thread.get() for thread in harvest_threads]))
    pool.close()

    # process orders
    processing_market(orders_data=harvest_data, region_id=region_id)


# ======================================================================================================================
# PROCESSING FUNCTIONS
# ======================================================================================================================


def processing(orders_data, to_create, to_update, to_delete, public=False):

    orders_to_create = [order_data for order_data in orders_data if order_data['order_id'] in to_create]
    orders_created = insert(orders_to_create, public=public)

    order_ids_not_created = set(to_create) - set(order.id for order in orders_created)
    print('%-45s' % 'Unrelated:', len(order_ids_not_created), flush=True)

    orders_to_update = [order_data for order_data in orders_data if order_data['order_id'] in to_update]
    update(orders_to_update)

    print('%-46s' % 'Remove old orders', end='', flush=True)
    count = MarketOrder.objects.filter(id__in=to_delete, removed__isnull=True).update(removed=timezone.now())
    print(f'{count}', flush=True)


def processing_character(orders_data, character_id):

    orders_ids = [order_data['order_id'] for order_data in orders_data]

    orders_db_query = MarketOrder.objects.filter(Q(id__in=orders_ids) | Q(character_id=character_id))
    orders_ids_db = orders_db_query.values_list('id', flat=True)

    updated_at = timezone.now() - timezone.timedelta(minutes=60)
    orders_ids_db_immutable = orders_db_query.filter(updated_at__gte=updated_at).values_list('id', flat=True)

    to_create = set(orders_ids) - set(orders_ids_db)
    to_update = set(orders_ids_db) - set(to_create) - set(orders_ids_db_immutable)
    to_delete = set(orders_ids_db) - set(orders_ids) - set(orders_ids_db_immutable)

    processing(orders_data, to_create, to_update, to_delete, public=False)

    # set character and corporation if need
    print('-- Update foreign keys --', flush=True)

    _character = Character.objects.get(id=character_id)

    print('%-46s' % 'Character', end='', flush=True)
    count = MarketOrder.objects.filter(id__in=orders_ids).exclude(character=_character).update(character=_character)
    print(f'{count}', flush=True)

    print('%-46s' % 'Corporation', end='', flush=True)
    to_corporation = [order_data['order_id'] for order_data in orders_data if order_data['is_corporation']]
    count = MarketOrder.objects.filter(id__in=to_corporation).\
        exclude(corporation_id=_character.corporation_id).\
        update(corporation_id=_character.corporation_id)
    print(f'{count}', flush=True)


def processing_corporation(orders_data, corporation_id):

    orders_ids = [order_data['order_id'] for order_data in orders_data]

    orders_db_query = MarketOrder.objects.filter(Q(id__in=orders_ids) | Q(corporation_id=corporation_id))
    orders_ids_db = orders_db_query.values_list('id', flat=True)

    updated_at = timezone.now() - timezone.timedelta(minutes=60)
    orders_ids_db_immutable = orders_db_query.filter(updated_at__gte=updated_at).values_list('id', flat=True)

    to_create = set(orders_ids) - set(orders_ids_db)
    to_update = set(orders_ids_db) - set(to_create) - set(orders_ids_db_immutable)
    to_delete = set(orders_ids_db) - set(orders_ids) - set(orders_ids_db_immutable)

    processing(orders_data, to_create, to_update, to_delete, public=False)

    # set character and corporation if need
    print('-- Update foreign keys --', flush=True)

    print('%-46s' % 'Character', end='', flush=True)
    conditions = [When(id=order_data['order_id'], then=Value(order_data.get('issued_by'))) for order_data in orders_data]
    count = orders_db_query.filter(character_id__isnull=True).update(character_id=Case(*conditions, default=None))
    print(f'{count}', flush=True)

    print('%-46s' % 'Corporation', end='', flush=True)
    count = MarketOrder.objects.filter(id__in=orders_ids).exclude(corporation_id__isnull=False).update(corporation_id=corporation_id)
    print(f'{count}', flush=True)


def processing_market(orders_data, region_id):

    orders_ids = [order['order_id'] for order in orders_data]

    orders_db_query = MarketOrder.objects.filter(region_id=region_id)
    orders_ids_db = orders_db_query.values_list('id', flat=True)

    updated_at = timezone.now() - timezone.timedelta(minutes=5)
    orders_ids_db_immutable = orders_db_query.filter(updated_at__gte=updated_at).values_list('id', flat=True)
    orders_ids_db_not_public = orders_db_query.filter(public=False).values_list('id', flat=True)

    to_create = set(orders_ids) - set(orders_ids_db)
    to_update = set(orders_ids) - set(to_create) - set(orders_ids_db_immutable)
    to_delete = set(orders_ids_db) - set(orders_ids) - set(orders_ids_db_immutable) - set(orders_ids_db_not_public)

    processing(orders_data, to_create, to_update, to_delete, public=True)

    print('-- Update public flag --', flush=True)

    print('%-46s' % 'Public', end='', flush=True)
    count = MarketOrder.objects.filter(id__in=orders_ids, public=False).update(public=True)
    print(f'{count}', flush=True)

# ======================================================================================================================
# FUNCTIONS BY HARVESTING STEPS
# ======================================================================================================================


def comparable(orders_data):

    for order_data in orders_data:

        _id = order_data['order_id']
        escrow = order_data.get('escrow')
        price = order_data['price']
        volume_remain = order_data['volume_remain']
        issued = dateparse.parse_datetime(order_data['issued'])

        yield _id, escrow, price, issued, volume_remain


def region_page(region_id, page, retry=3):
    """
    Load orders pages
    :param region_id: esi api region id to harvest orders
    :param page: esi api page of region id orders
    :param retry: count of retry to load page
    """

    result, headers = esiapi.market.orders(region_id, page=page)
    if 'error' in result:

        logger.warning(f"ESI API: {result.get('error')}", exc_info=True, extra={
            'headers': headers,
            'result': result,
            'region_id': region_id,
            'page': page,
            'retry': retry,
        })

        if not retry:
            return []

        print(f'Error while loading region_id={region_id} page={page} but can retry: ({retry})')
        time.sleep(60)
        return region_page(region_id=region_id, page=page, retry=retry-1)

    return result


# ======================================================================================================================
# ORDERS DATABASE: INSERT, UPDATE, DELETE
# ======================================================================================================================


def delete():
    """
    Service function to real delete all orders with removed=True.
    Delete only orders removed two hours ago minimum. Two hours - x2 time of cached user orders (best case 1h 10m.)
    We want it (wait 2 hours) because if remove order from market, it can recreate for user from esi api cache
    """

    removed_datetime = timezone.now() - timezone.timedelta(hours=2)
    count, info = MarketOrder.objects.filter(removed__lte=removed_datetime).delete()

    return count


def insert(orders_data, public=False):

    print('%-46s' % 'Inserted:', end='', flush=True)

    # -- PREPARE --

    item_types_db = ItemType.objects.filter(id__in=[order['type_id'] for order in orders_data]).distinct('id').values_list('id', flat=True)
    stations_db = Station.objects.filter(id__in=[order['location_id'] for order in orders_data]).distinct('id').values_list('id', flat=True)

    systems_map = dict(System.objects.filter(stations__id__in=stations_db).values_list('stations__id', 'id'))
    regions_map = dict(Region.objects.filter(systems__id__in=systems_map.values()).values_list('systems__id', 'id'))

    # -- CONVERT --

    orders_to_create_dict = {}

    item_type_errors = set()
    station_errors = set()

    for order_data in orders_data:

        item_type_valid = order_data['type_id'] in item_types_db
        station_valid = order_data['location_id'] in stations_db

        if not item_type_valid:
            item_type_errors.add(order_data['type_id'])

        if not station_valid:
            station_errors.add(order_data['location_id'])

        if item_type_valid and station_valid:
            market_order = MarketOrder.esiapi_parse(order_data, public)
            market_order.system_id = market_order.system_id or systems_map[market_order.station_id]
            market_order.region_id = market_order.region_id or regions_map[market_order.system_id]
            orders_to_create_dict[market_order.id] = market_order

    # -- SAVE --

    orders_to_create = orders_to_create_dict.values()
    created = MarketOrder.objects.bulk_create(orders_to_create, batch_size=10000)
    print(len(created), flush=True)

    logger.warning(f"ESI API: UNRELATED ORDERS", exc_info=True, extra={
        'item_type_id': item_type_errors,
        'station_id': station_errors,
    })

    return orders_to_create


def update(orders_data):

    print('%-46s' % 'Updated:', end='', flush=True)

    order_ids = [order_data['order_id'] for order_data in orders_data]

    fields = 'id', 'escrow', 'price', 'issued', 'volume_remain'
    orders_data_db_values_list = MarketOrder.objects.filter(id__in=order_ids).values_list(*fields)
    orders_data_db = set((_id, escrow and float(escrow), float(price), *data) for _id, escrow, price, *data in orders_data_db_values_list)

    orders_data_to_update = set(comparable(orders_data)) - orders_data_db
    orders_ids_to_update = [_id for _id, *data in orders_data_to_update]
    orders_db_to_update = MarketOrder.objects.filter(id__in=orders_ids_to_update)

    orders_data_map = {order_data['order_id']: order_data for order_data in orders_data}
    orders_to_update = [market_order for market_order in orders_db_to_update
                        if market_order.esiapi_update(orders_data_map[market_order.id])]

    # -- SAVE --

    # TODO django 2.2 uncomment to bulk_update
    # MarketOrder.objects.bulk_update(orders_to_update, fields=fields[1:], batch_size=1000)
    # print(len(orders_to_update), flush=True)

    if orders_to_update:

        values = tuple(
            '(' +
            f'{order.id}, ' +
            f'{order.character_id or "NULL"}, ' +
            f'{order.escrow or "NULL"}, ' +
            f'\'{order.expires_date.isoformat()}\', ' +
            f'\'{order.issued.isoformat()}\', ' +
            f'{order.price}, ' +
            f'{order.volume_remain}' +
            ')'
            for order in orders_to_update
        )

        update_query = f'''
            UPDATE market_marketorder
                SET character_id = new.character_id::bigint,
                    escrow = new.escrow::int,
                    expires_date = new.expires_date::timestamp,
                    issued = new.issued::timestamp,
                    price = new.price,
                    updated_at = '{timezone.now().isoformat()}',
                    volume_remain = new.volume_remain
                FROM (
                    VALUES {','.join(values)}
                ) as new(id, character_id, escrow, expires_date, issued, price, volume_remain)
                WHERE market_marketorder.id = new.id
            '''

        with connection.cursor() as cursor:
            cursor.execute(update_query)

    print(len(orders_to_update), flush=True)

    return orders_to_update
