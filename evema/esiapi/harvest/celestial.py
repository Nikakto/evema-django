from django.db import transaction

import tqdm
from itertools import chain

import esiapi
from assets.models import ItemType
from character.models import CharacterToken
from esiapi.harvest._tools import bar_format, harvest_log_error, harvest_threading
from universe.models import Station, System


def stations():

    token = CharacterToken.objects.\
        filter(expired=False, public=True, scopes__contains=['esi-markets.read_character_orders.v1']).first()

    if not token or not token.refresh():
        raise PermissionError('''Can't find non expired public token to harvest structures''')

    def _get_stations_list():

        structures_ids, header = esiapi.universe.structures()
        if 'error' in structures_ids:
            raise ConnectionError('''Can't get list of structures''')

        # do not use Station model to get station_ids
        # harvested systems contains npc stations. Station model contain already harvested stations and structures
        stations_ids = System.objects.filter(stations_ids__len__gt=0).values_list('stations_ids', flat=True)

        return structures_ids + list(chain(*stations_ids))

    def _get_station_info(station_id):

        structure = (station_id > 10 ** 12)
        if not structure:
            result, headers = esiapi.universe.station(station_id)

        else:

            result, headers = esiapi.universe.structure(station_id, token)
            if not result.get('error'):
                result['station_id'] = station_id
                result['system_id'] = result['solar_system_id']

        if result.get('error'):

            harvest_log_error('stations_harvesting.log', error=result, data=f'station_id={station_id}')
            return None

        return result, headers

    print('\n\nHarvesting stations:', flush=True)

    # collect
    print('%-46s' % 'Get list', end='', flush=True)
    stations_list = _get_stations_list()
    print('Done', flush=True)

    # harvest
    harvest_data = harvest_threading(_get_station_info, stations_list)

    # processing
    region_of_system = {system.id: system.region_id for system in System.objects.all()}
    system_ids = System.objects.values_list('id', flat=True)
    type_ids = ItemType.objects.values_list('id', flat=True)

    _tqdm = tqdm.tqdm(harvest_data, unit='stations', desc='Processing', bar_format=bar_format, ncols=120)
    for data, headers in _tqdm:

        fk_errors = []

        if data['type_id'] not in type_ids:
            fk_errors.append(f'type_id ({data["type_id"]})')

        if data['system_id'] not in system_ids:
            fk_errors.append(f'system_id ({data["system_id"]})')

        if fk_errors:
            harvest_log_error('station.log', ','.join(fk_errors), data)
            continue

        station = Station()
        station.id = data['station_id']
        station.access = (data['station_id'] < 10 ** 12)  # default for structure - false
        station.name = data['name']
        station.item_type_id = data['type_id']
        station.max_dockable_ship_volume = data.get('max_dockable_ship_volume')
        station.office_rental_cost = data.get('office_rental_cost')
        station.reprocessing_efficiency = data.get('reprocessing_efficiency')
        station.reprocessing_stations_take = data.get('reprocessing_stations_take')
        station.services = data.get('services')
        station.structure = (data['station_id'] > 10 ** 12)
        station.system_id = data['system_id']

        station.region_id = region_of_system[station.system_id]

        station.save()

    # save to db
    print('%-46s' % 'Save to db:', end='', flush=True)
    transaction.commit()
    print('Done', flush=True)


if __name__ == '__main__':
    stations()
