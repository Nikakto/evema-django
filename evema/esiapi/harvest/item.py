from django.db import transaction

import tqdm

import esiapi
from assets.models import ItemCategory, ItemGroup, ItemType
from esiapi.harvest._tools import bar_format, harvest_log_error, harvest_threading, harvesting
from market.models import MarketGroup


@harvesting
def categories():

    print('\n\nHarvesting item categories:', flush=True)

    # collect
    print('%-46s' % 'Get list', end='', flush=True)
    categories_list, headers = esiapi.universe.categories()
    if 'error' in categories_list:
        print('Error', flush=True)
        return

    print('Done', flush=True)

    # harvest
    harvest_data = harvest_threading(esiapi.universe.category, categories_list)

    # processing
    _tqdm = tqdm.tqdm(harvest_data, unit='categories', desc='Processing', bar_format=bar_format, ncols=120)
    for data, headers in _tqdm:

        category = ItemCategory()
        category.id = data['category_id']
        category.name = data['name']
        category.published = data['published']

        category.save()

    # save to db
    print('%-46s' % 'Save to db:', end='', flush=True)
    transaction.commit()
    print('Done', flush=True)


@harvesting
def groups():

    print('\n\nHarvesting item groups:', flush=True)

    # collect
    print('%-46s' % 'Get list', end='', flush=True)
    groups_list, headers = esiapi.universe.groups()
    if 'error' in groups_list:
        print('Error', flush=True)
        return

    print('Done', flush=True)

    # harvest
    harvest_data = harvest_threading(esiapi.universe.group, groups_list)

    # processing
    category_ids = ItemCategory.objects.values_list('id', flat=True)

    _tqdm = tqdm.tqdm(harvest_data, unit='groups', desc='Processing', bar_format=bar_format, ncols=120)
    for data, headers in _tqdm:

        fk_errors = []

        if data['category_id'] not in category_ids:
            fk_errors.append(f'category_id ({data["category_id"]})')

        if fk_errors:
            harvest_log_error('item_group.log', ','.join(fk_errors), data)
            continue

        group = ItemGroup()
        group.id = data['group_id']
        group.name = data['name']
        group.published = data['published']
        group.item_category_id = data['category_id']

        group.save()

    # save to db
    print('%-46s' % 'Save to db:', end='', flush=True)
    transaction.commit()
    print('Done', flush=True)


@harvesting
def types():

    print('\n\nHarvesting item types:', flush=True)

    # collect
    print('%-46s' % 'Get list', end='', flush=True)
    types_list, headers = esiapi.universe.types()
    if 'error' in types_list:
        print('Error', flush=True)
        return

    print('Done', flush=True)

    # harvest
    harvest_data = harvest_threading(esiapi.universe.type, types_list)

    # processing
    group_ids = ItemGroup.objects.values_list('id', flat=True)
    market_group_ids = MarketGroup.objects.values_list('id', flat=True)

    _tqdm = tqdm.tqdm(harvest_data, unit='types', desc='Processing', bar_format=bar_format, ncols=120)
    for data, headers in _tqdm:

        fk_errors = []

        if data['group_id'] not in group_ids:
            fk_errors.append(f'group_id ({data["group_id"]})')

        if data.get('market_group_id') and data['market_group_id'] not in market_group_ids:
            fk_errors.append(f'market_group_id ({data["market_group_id"]})')

        if fk_errors:
            harvest_log_error('item_type.log', ','.join(fk_errors), data)
            continue

        item_type = ItemType()
        item_type.id = data['type_id']
        item_type.name = data['name']
        item_type.description = data['description']
        item_type.published = data['published']
        item_type.group_id = data['group_id']
        item_type.market_group_id = data.get('market_group_id')

        group = ItemGroup.objects.filter(id=data['group_id']).first()
        item_type.item_category_id = group.item_category_id

        item_type.save()

    # save to db
    print('%-46s' % 'Save to db:', end='', flush=True)
    transaction.commit()
    print('Done', flush=True)
