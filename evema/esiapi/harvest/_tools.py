from django.conf import settings
from django.db import transaction

import datetime
import os
import time
import tqdm
from functools import wraps
from multiprocessing.pool import ThreadPool

bar_format = '{desc:<45} {percentage:<6.2f}% |{bar}| {n_fmt:>6}/{total_fmt:<6} {rate_fmt:30} {elapsed:5}'


def harvesting(harvester):

    @wraps(harvester)
    def wrapper(*args, **kwargs):

        transaction.set_autocommit(False)

        try:
            result = harvester(*args, **kwargs)
        except Exception:
            transaction.rollback()
            raise
        else:
            transaction.commit()  # You should commit manual at func.
        finally:
            transaction.set_autocommit(True)

        return result

    return wrapper


def harvest_log_error(logfile='log', error='UNNAMED ERROR', data='', verbose=True):

    error = f'{datetime.datetime.now().isoformat()} ERROR: {error} DATA: {data}'

    path = os.path.join(settings.OTHER_LOG_FOLDER, datetime.date.today().isoformat())
    if not os.path.exists(path):
        os.makedirs(path)

    file = os.path.join(path, logfile)
    with open(file, 'a') as f:
        f.write(f'{error}\n')


def harvest_progressbar(threads_list, **kwargs):

    with tqdm.tqdm(iterable=threads_list, **kwargs) as bar:
        ready = 0
        while ready < len(threads_list):
            ready = sum(thread.ready() for thread in threads_list)
            bar.n = ready
            bar.refresh()
            time.sleep(0.1)


def harvest_threading(func, data_list):

    pool = ThreadPool(processes=settings.OTHER_REQUEST_THREADS)
    harvest_threads = [pool.apply_async(func, args=[_id]) for _id in data_list]
    harvest_progressbar(harvest_threads, unit='requests', desc='Get information from ESI API', bar_format=bar_format, ncols=120)
    return [thread.get() for thread in harvest_threads if thread.get()]


def harvest_threading_model(func, instances):
    """
    Calling function for each instance of instances
    :param func: name of function
    :param instances: instances to call func
    :return: list of result applying func of instances
    """

    pool = ThreadPool(processes=settings.OTHER_REQUEST_THREADS)
    harvest_threads = [pool.apply_async(getattr(instance, func)) for instance in instances]
    harvest_progressbar(harvest_threads, unit='requests', desc='Get information from ESI API', bar_format=bar_format, ncols=120)
    return [thread.get() for thread in harvest_threads if thread.get()]


def harvest_timer(func):

    def wrapper(*args, **kwargs):

        _start = time.time()

        result = func(*args, **kwargs)

        _finish = time.time() - _start
        print(f'Elapsed time: {_finish:.2f}s')

        return result

    return wrapper