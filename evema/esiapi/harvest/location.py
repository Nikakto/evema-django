from django.db import transaction

import tqdm

import esiapi
from esiapi.harvest._tools import bar_format, harvesting, harvest_log_error, harvest_threading
from universe.models import Constellation, System, Region


@harvesting
def constellations():

    print('\n\nHarvesting constellations:', flush=True)

    # collect
    print('%-46s' % 'Get list', end='', flush=True)
    constellations_list, headers = esiapi.universe.constellations()
    print('Done', flush=True)

    # harvest
    harvest_data = harvest_threading(esiapi.universe.constellation, constellations_list)

    # processing
    region_ids = Region.objects.values_list('id', flat=True)

    _tqdm = tqdm.tqdm(harvest_data, unit='constellations', desc='Processing', bar_format=bar_format, ncols=120)
    for data, headers in _tqdm:

        fk_errors = []
        if data['region_id'] not in region_ids:
            fk_errors.append(f'region_id ({data["region_id"]})')

        if fk_errors:
            harvest_log_error('constellation.log', ','.join(fk_errors), data)
            continue

        constellation = Constellation()
        constellation.id = data['constellation_id']
        constellation.name = data['name']
        constellation.region_id = data['region_id']

        constellation.save()

    # save to db
    print('%-46s' % 'Save to db:', end='', flush=True)
    transaction.commit()
    print('Done', flush=True)


@harvesting
def systems():

    print('\n\nHarvesting systems:', flush=True)

    # collect
    print('%-46s' % 'Get list', end='', flush=True)
    systems_list, headers = esiapi.universe.systems()
    print('Done', flush=True)

    # harvest
    harvest_data = harvest_threading(esiapi.universe.system, systems_list)

    # processing
    constellation_ids = Constellation.objects.values_list('id', flat=True)

    _tqdm = tqdm.tqdm(harvest_data, unit='systems', desc='Processing', bar_format=bar_format, ncols=120)
    for data, headers in _tqdm:

        fk_errors = []

        if data['constellation_id'] not in constellation_ids:
            fk_errors.append(f'constellation_id ({data["constellation_id"]})')

        if fk_errors:
            harvest_log_error('system.log', ','.join(fk_errors), data)
            continue

        system = System()
        system.id = data['system_id']
        system.constellation_id = data['constellation_id']
        system.name = data['name']
        system.security_status = round(data['security_status'], 1)
        system.security_class = data.get('security_class', '')
        system.stargates_ids = data.get('stargates', [])
        system.stations_ids = data.get('stations', [])

        constellation = Constellation.objects.filter(id=data['constellation_id']).first()
        system.region_id = constellation.region_id

        system.save()

    # save to db
    print('%-46s' % 'Save to db:', end='', flush=True)
    transaction.commit()
    print('Done', flush=True)


@harvesting
def regions():

    print('\n\nHarvesting regions:', flush=True)

    # collect
    print('%-46s' % 'Get list', end='', flush=True)
    regions_list, headers = esiapi.universe.regions()
    print('Done', flush=True)

    # harvest
    harvest_data = harvest_threading(esiapi.universe.region, regions_list)

    # processing
    _tqdm = tqdm.tqdm(harvest_data, unit='regions', desc='Processing', bar_format=bar_format, ncols=120)
    for data, headers in _tqdm:

        region = Region()
        region.id = data['region_id']
        region.name = data['name']
        region.description = data.get('description', '')

        region.save()

    # save to db
    print('%-46s' % 'Save to db:', end='', flush=True)
    transaction.commit()
    print('Done', flush=True)
