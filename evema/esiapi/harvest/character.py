from django.db import transaction

from character.models import Character
from esiapi.harvest._tools import harvest_threading_model, harvesting


@harvesting
def characters():
    """
    ESI API character does not return character_id.
    So, Character model has function to updating from ESI API.
    This harvester just call this function for all character with not expired token
    """

    print('\n\nHarvesting characters:', flush=True)

    characters = Character.objects.filter(token__expired=False).order_by('id')

    # harvesting
    # NOTE. update_from_esi call Model.save(), so we are not required to call save for each character
    harvest_threading_model('update_from_esiapi', characters)

    # save to db
    print('%-46s' % 'Save to db:', end='', flush=True)
    transaction.commit()
    print('Done', flush=True)
