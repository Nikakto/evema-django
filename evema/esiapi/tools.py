from django.conf import settings

import logging
import requests
import time
from functools import wraps

logger = logging.getLogger(__name__)


def make_request(url, method='GET', token=None, retry=1, limit=True, **kwargs) -> tuple:
    """
    Make request to esi api
    :param url: url for request
    :param method: GET, POST, PUT etc.
    :param token: CharacterToken instance
    :param retry: number of current retrying
    :param limit: enable ESI API error limit. That mean x-esi-error-limit-remain ignored and do force request
    :param kwargs: any kwargs for esi api method (page, item_id, station_id etc)
    :return: tuple of (result, headers). Headers is required to get page for paginated methods
    """

    params = dict(kwargs)

    if token:

        params['token'] = token.access_token

        if token.expired:
            if token.character.user.auth_token:
                token.character.user.auth_token.delete()
            return {'error': 'Token expired', 'sso_status': 400}, {}

    response = requests.request(method, url, params=params)
    if response.status_code >= 500:

        if retry < settings.OTHER_RETRY_LIMIT:
            time.sleep(1) if response.status_code == 503 else time.sleep(0.1)
            return make_request(url, method, token, retry=retry + 1, **kwargs)
        else:
            return response.json(), dict(response.headers.lower_items())

    else:

        result = response.json()

        # TODO remove loger and uncomment when 1055 fixed
        # result_invalid = token and isinstance(result, dict) and result.get('sso_status') == 400
        result_invalid = token and isinstance(result, dict) and result.get('sso_status') in [400, 401]
        if retry < settings.OTHER_RETRY_LIMIT and result_invalid:

            if result.get('sso_status') == 401:
                logger.warning(f"ESI API: {result.get('error')}", exc_info=True, extra={
                    'error': 'https://github.com/esi/esi-issues/issues/1055',
                    'result': result,
                })

            if not token.expired and token.refresh():
                return make_request(url, method, token, retry=retry + 1, **kwargs)
            else:
                token.expired = True
                token.save()

        return result, dict(response.headers.lower_items())


def pages(source):

    @wraps(source)
    def wrapper(*args, **kwargs):

        if not args and not kwargs.get('page') or kwargs.get('page', 0) < 0:

            headers = {}
            page = 1
            result = []
            xpages = 1

            while page <= xpages:

                page_result, headers = source(page=page, *args, **kwargs)
                if isinstance(page_result, dict) and page_result.get('error'):
                    result = page_result
                    break

                result.extend(page_result)
                xpages = int(headers.get('x-pages', 0))
                page += 1

        else:
            result, headers = source(*args, **kwargs)

        return result, headers

    return wrapper
