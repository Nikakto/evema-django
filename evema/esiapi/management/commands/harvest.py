from django.core.management.base import BaseCommand, CommandError

import time

from character.models import Character
from corporation.models import Corporation
from esiapi import harvest
from universe.models import Region

harvest_available = {

    # location
    'constellations': harvest.location.constellations,
    'regions': harvest.location.regions,
    'systems': harvest.location.systems,

    # market groups (need for items)
    'market_groups': harvest.market.groups,

    # item
    'item_categories': harvest.item.categories,
    'item_groups': harvest.item.groups,
    'item_types': harvest.item.types,

    # celestial
    'stations': harvest.celestial.stations,

    # orders
    'orders': harvest.orders.harvest_market,
    'orders_characters': harvest.orders.harvest_character,
    'orders_corporations': harvest.orders.harvest_corporation,

    # other
    'characters': harvest.character.characters,

}


class Command(BaseCommand):

    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('data', type=str)
        parser.add_argument('--name', type=str, dest='name', help='Only orders. Name of region, character or corporation',)

    def handle(self, *args, **options):

        print(args, options)

        data = options.get('data')
        if data == 'all':
            self.harvest_all()

        elif data not in harvest_available:
            raise CommandError('''Harvester's target should be one of:''', ', '.join(harvest_available.keys()))

        elif data == 'orders':

            region_name = options.get('name')
            if not region_name:
                self.harvest_orders()
            else:
                region = Region.objects.filter(name__icontains=region_name).first()
                if not region:
                    print(f'''Can't find region with name {region_name}''')
                    return

                func = harvest_available[data]
                func(region_id=region.id)

        elif data == 'orders_characters':

            character_name = options.get('name')
            if not character_name:
                self.harvest_orders_characters()
            else:
                character = Character.objects.filter(name__icontains=character_name).first()
                if not character:
                    print(f'''Can't find character with name {character_name}''')
                    return

                func = harvest_available[data]
                func(character_id=character.id)

        elif data == 'orders_corporations':

            corporation_name = options.get('name')
            if not corporation_name:
                self.harvest_orders_corporation()
            else:
                corporation = Corporation.objects.filter(name__icontains=corporation_name).first()
                if not corporation:
                    print(f'''Can't find corporation with name {corporation_name}''')
                    return

                func = harvest_available[data]
                func(corporation_id=corporation.id)

        else:
            func = harvest_available[data]
            func()

    def harvest_all(self):

        # location
        harvest.location.regions()
        harvest.location.constellations()
        harvest.location.systems()

        # market
        harvest.market.groups()

        # item
        harvest.item.categories()
        harvest.item.groups()
        harvest.item.types()

        # celestial
        harvest.celestial.stations()

        # orders
        self.harvest_orders()

    def harvest_orders(self):

        start = time.time()

        regions = Region.objects.all()
        for region in regions:
            harvest.orders.harvest_market(region_id=region.id)

        finish = time.time() - start
        print(f'Collected orders all regions: {finish}s')

    def harvest_orders_characters(self):

        start = time.time()

        characters = Character.objects.all()
        for character in characters:
            harvest.orders.harvest_character(character_id=character.id)

        finish = time.time() - start
        print(f'Collected orders all characters: {finish}s')

    def harvest_orders_corporation(self):

        start = time.time()

        corporations = Corporation.objects.all()
        for corporation in corporations:
            harvest.orders.harvest_corporation(corporation_id=corporation.id)

        finish = time.time() - start
        print(f'Collected orders all corporations: {finish}s')
