from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from evema.utils.pagination import SmallResultsSetPagination
from assets.models import ItemType


class TestItemTypeList(APITestCase):

    fixtures = [
        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',
    ]

    client = APIClient()
    url = reverse('assets:items')

    def setUp(self):

        # in class django connected real database.
        self.pages_count = ItemType.objects.count() // SmallResultsSetPagination.page_size + 1

    def test_item_type_list__get_default__list(self):

        response = self.client.get(self.url)

        # check answer item
        item_type_answer = response.data[0]
        item_type_real = ItemType.objects.get(id=item_type_answer['id'])

        self.assertEqual(item_type_answer['id'], item_type_real.id)
        self.assertEqual(item_type_answer['item_category'], item_type_real.item_category_id)
        self.assertEqual(item_type_answer['description'], item_type_real.description)
        self.assertEqual(item_type_answer['group'], item_type_real.group_id)
        self.assertEqual(item_type_answer['market_group'], item_type_real.market_group_id)
        self.assertEqual(item_type_answer['name'], item_type_real.name)

        # check ids
        ids_answer = [item_type['id'] for item_type in response.data]
        self.assertEqual(len(set(ids_answer)), SmallResultsSetPagination.page_size)

        # check pagination
        self.assertEqual(response['x-page-current'], '1')
        self.assertEqual(response['x-pages'], str(self.pages_count))

    def test_item_type__get_page_last__list_page_last(self):

        data = {'page': self.pages_count}
        response = self.client.get(self.url, data)

        # check pagination
        self.assertEqual(response.data[-1]['id'], ItemType.objects.last().id)
        self.assertEquals(len(response.data), ItemType.objects.count() % SmallResultsSetPagination.page_size)

        # check pagination
        self.assertEqual(response['x-page-current'], str(self.pages_count))
        self.assertEqual(response['x-pages'], str(self.pages_count))

    def test_item_type__get_page_invalid__404(self):

        data = {'page': self.pages_count + 1}
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_item_type__get_page_size__list_100(self):

        data = {'page_size': 100}
        total_pages = ItemType.objects.count() // 100 + 1
        response = self.client.get(self.url, data)

        item_types_ids_answer = [int(system['id']) for system in response.data]
        item_types_real = list(ItemType.objects.filter(id__in=item_types_ids_answer).order_by('id').values_list('id', flat=True))

        self.assertListEqual(item_types_ids_answer, item_types_real)
        self.assertEqual(response['x-pages'], str(total_pages))

    def test_item_type__get_search_special__get_valid_item_types(self):

        search_word = 'search_012'

        data = {'page_size': 100, 'search': search_word}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        rank_prev = None
        for station in response.data:

            self.assertIn(search_word.lower(), station['name'].lower())

            if rank_prev:
                self.assertLessEqual(len(search_word) / len(station['name']), rank_prev)

            rank_prev = len(search_word) / len(station['name'])

    def test_item_type__get_search_special_with_space__get_valid_item_types(self):

        search_word = 'search me'

        data = {'page_size': 100, 'search': search_word}
        response = self.client.get(self.url, data)

        self.assertTrue(response.data)

        for system in response.data:
            self.assertIn(search_word.lower(), system['name'].lower())
