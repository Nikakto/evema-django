from django.db.models import Max
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from assets.models import ItemType


class TestItemType(APITestCase):

    fixtures = [
        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',
    ]

    client = APIClient()

    def test_item_type__get_exist__item_type(self):

        item_type_expected = ItemType.objects.first()

        data = {'pk': item_type_expected.id}
        url = reverse('assets:item', kwargs=data)
        response = self.client.get(url)

        self.assertEqual(response.data['id'], item_type_expected.id)
        self.assertEqual(response.data['item_category'], item_type_expected.item_category_id)
        self.assertEqual(response.data['description'], item_type_expected.description)
        self.assertEqual(response.data['group'], item_type_expected.group_id)
        self.assertEqual(response.data['market_group'], item_type_expected.market_group_id)
        self.assertEqual(response.data['name'], item_type_expected.name)
        self.assertIn('image', response.data)

    def test_region__get_invalid__404(self):

        region_expected = ItemType.objects.all().aggregate(max_id=Max('id'))

        data = {'pk': region_expected['max_id'] + 1}
        url = reverse('assets:item', kwargs=data)
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
