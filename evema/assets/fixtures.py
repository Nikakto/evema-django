# function as this file should be called in evema.managment.fixtures command
# functions generating fixtures of models

from assets.models import ItemCategory, ItemGroup, ItemType
from evema.utils.fixtures import make_fixture


def generate_item_categories():

    item_categories = [
        ItemCategory(id=1, name='test category 1', published=True),
        ItemCategory(id=1, name='test category 2', published=True),
        ItemCategory(id=1, name='test category not published', published=False),
    ]

    path = './assets/fixtures/item_categories.json'
    make_fixture(item_categories, path)

    return item_categories


def generate_item_groups(item_categories):

    groups_per_category = 2
    item_group_name = 'item_group_{id}'

    item_group_id = 1
    item_groups = []
    for item_category in item_categories:

        region_constellations = [
            ItemGroup(id=item_group_id + i,
                      item_category=item_category,
                      name=item_group_name.format(id=item_group_id),
                      published=i)
            for i in range(groups_per_category)
        ]

        item_groups.extend(region_constellations)
        item_group_id += groups_per_category

    path = './assets/fixtures/item_groups.json'
    make_fixture(item_groups, path)

    return item_groups


def generate_item_types(item_groups, item_market_groups):

    item_id = 1
    item_types = []

    for item_group in item_groups:
        for market_group in item_market_groups:

            # zero and second group has no items (zero hasn't items and groups, second hasn't only items)
            if market_group.id == 0 or market_group.id == 2:
                continue

            for i in range(3):

                item_types.append(
                    ItemType(
                        id=item_id,
                        item_category=item_group.item_category,
                        group=item_group,
                        market_group=market_group,
                        name=f'item_type_{item_id}',
                        published=i > 0,
                    )
                )

                item_id += 1

    # item_types for search ranks
    item_types_search_ranked = item_types[-10:]
    for index, station in enumerate(item_types_search_ranked):
        postfix = ''.join(str(i) for i in range(index))
        station.name = f'search_{postfix}'

    # stations for search with space
    item_type = item_types[-1]
    item_type.name = 'SEARCH ME PLEASE WITH SPACE'

    path = './assets/fixtures/item_types.json'
    make_fixture(item_types, path)

    return item_types
