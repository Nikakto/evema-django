from django.db import models


class ItemCategory(models.Model):

    id = models.BigAutoField(primary_key=True)

    name = models.CharField(max_length=64)
    published = models.BooleanField(default=False)


class ItemGroup(models.Model):

    id = models.BigAutoField(primary_key=True)

    name = models.CharField(max_length=128)
    published = models.BooleanField(default=False)
    item_category = models.ForeignKey('ItemCategory', on_delete=models.CASCADE, related_name='item_groups')


class ItemType(models.Model):

    id = models.BigAutoField(primary_key=True)

    item_category = models.ForeignKey('ItemCategory', on_delete=models.CASCADE, related_name='item_types')
    description = models.TextField()
    group = models.ForeignKey('ItemGroup', on_delete=models.CASCADE, related_name='item_types')
    market_group = models.ForeignKey('market.MarketGroup', null=True, on_delete=models.PROTECT, related_name='item_types')
    name = models.CharField(max_length=128)
    published = models.BooleanField(default=False)

    @property
    def image_url(self):

        result = {
            32: f'https://image.eveonline.com/Type/{self.id}_32.png',
            64: f'https://image.eveonline.com/Type/{self.id}_64.png',
        }

        return result

