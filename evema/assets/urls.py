from django.urls import path


from assets.views import ItemTypeDetail, ItemTypeList

urlpatterns = [
    path('items/', ItemTypeList.as_view(), name='items'),
    path('items/<int:pk>/', ItemTypeDetail.as_view(), name='item'),
]
