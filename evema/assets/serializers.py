from rest_framework import serializers

from assets.models import ItemType


class ItemTypeSerializer(serializers.ModelSerializer):

    image = serializers.DictField(child=serializers.CharField(), source='image_url', read_only=True)

    class Meta:
        model = ItemType
        fields = '__all__'
