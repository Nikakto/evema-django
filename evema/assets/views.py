from django.db.models import ExpressionWrapper, FloatField
from django.db.models.functions import Length
from rest_framework.generics import ListAPIView, RetrieveAPIView

from assets.models import ItemType
from assets.serializers import ItemTypeSerializer
from evema.utils.pagination import SmallResultsSetPagination


class ItemTypeDetail(RetrieveAPIView):

    queryset = ItemType.objects.all()
    serializer_class = ItemTypeSerializer


class ItemTypeList(ListAPIView):

    pagination_class = SmallResultsSetPagination
    serializer_class = ItemTypeSerializer

    def get_queryset(self):

        queryset = ItemType.objects.order_by('id').all()

        search = self.request.query_params.get('search')
        if not search:
            return queryset

        search_rank_expression = ExpressionWrapper(float(len(search)) / Length('name'), output_field=FloatField())
        return queryset.annotate(rank=search_rank_expression).filter(rank__gt=0.10, name__icontains=search).order_by('-rank')
