# function as this file should be called in evema.managment.fixtures command
# functions generating fixtures of models

from django.utils import timezone

import datetime

from evema.utils.fixtures import make_fixture
from market.models import MarketGroup, MarketOrder


def generate_market_groups():

    market_group_parents = 3
    market_group_description = 'market_group_{id} for test.'
    market_group_name = 'market_group_{id}'

    market_groups = [
        MarketGroup(id=i, description=market_group_description.format(id=i), name=market_group_name.format(id=i))
        for i in range(market_group_parents)
    ]

    # make some child groups
    market_group_id = market_group_parents
    for market_group in list(market_groups):

        # two first groups have not children groups
        if market_group.id > 1:
            for i in range(5):

                market_groups.append(
                    MarketGroup(
                        id=market_group_id,
                        description=market_group_description.format(id=market_group_id),
                        name=market_group_name.format(id=market_group_id),
                        parent_group=market_group)
                )

                market_group_id += 1

    path = './market/fixtures/market_groups.json'
    make_fixture(market_groups, path)

    return market_groups


def generate_market_orders(stations, item_types):

    orders = []
    order_id = 1

    # make one sell order with id=1 to filtering it easy
    orders.append(
        MarketOrder(
            id=order_id,
            duration=90,
            expires_date=timezone.now() + datetime.timedelta(days=90),
            is_buy_order=stations[0].structure,
            issued=timezone.now(),
            item_type=item_types[0],
            min_volume=1,
            price=order_id*100,
            range='system',
            region_id=stations[0].region_id,
            station=stations[0],
            system_id=stations[0].system_id,
            updated_at=timezone.now() - timezone.timedelta(days=1),
            volume_total=order_id,
            volume_remain=int(order_id/2),
        )
    )

    order_id += 1

    # make one orders shouldn't be listed (look up it by station_id == 1 and expires_date < datetime.now())
    orders.append(
        MarketOrder(
            id=order_id,
            duration=90,
            expires_date=timezone.now() - datetime.timedelta(days=1),
            is_buy_order=stations[0].structure,
            item_type=item_types[0],
            issued=timezone.now(),
            min_volume=1,
            price=order_id * 100,
            range='system',
            region_id=stations[0].region_id,
            station=stations[0],
            system_id=stations[0].system_id,
            updated_at=timezone.now() - timezone.timedelta(days=1),
            volume_total=order_id,
            volume_remain=int(order_id / 2),
        )
    )

    order_id += 1

    # make orders for each station (for count of orders, for filters by station, regions, sorting)
    # this orders has another item_id (for filtering by it)
    for station in stations:

        orders.append(
            MarketOrder(
                id=order_id,
                duration=90,
                expires_date=timezone.now() + datetime.timedelta(days=10000),
                is_buy_order=station.structure,
                issued=timezone.now(),
                item_type=item_types[1],
                min_volume=1,
                price=order_id*100,
                range='system',
                region_id=station.region_id,
                station=station,
                system_id=station.system_id,
                updated_at=timezone.now() - timezone.timedelta(days=1),
                volume_total=order_id,
                volume_remain=int(order_id/2),
            )
        )

        order_id += 1

    path = './market/fixtures/market_orders.json'
    make_fixture(orders, path)

    return orders
