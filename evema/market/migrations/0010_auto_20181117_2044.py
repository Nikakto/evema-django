# Generated by Django 2.2 on 2018-11-17 20:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0009_marketorder_corporation'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='marketorder',
            name='expires_date',
        ),
        migrations.RemoveField(
            model_name='marketorder',
            name='region',
        ),
        migrations.RemoveField(
            model_name='marketorder',
            name='structure',
        ),
        migrations.AddField(
            model_name='marketorder',
            name='escrow',
            field=models.DecimalField(decimal_places=2, max_digits=32, null=True),
        ),
        migrations.AddField(
            model_name='marketorder',
            name='public',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='marketorder',
            name='corporation',
            field=models.ForeignKey(db_constraint=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='orders', to='corporation.Corporation'),
        ),
    ]
