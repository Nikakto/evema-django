from django.urls import path

from market.views import MarketMenuView, MarketOrdersView


urlpatterns = [
    path('menu/', MarketMenuView.as_view(), name='menu'),
    path('orders/', MarketOrdersView.as_view(), name='orders'),
]
