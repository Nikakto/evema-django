from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView
from rest_framework.filters import OrderingFilter
from rest_framework_extensions.cache.decorators import cache_response

from market.filters import MarketOrderFilter
from market.models import MarketGroup, MarketOrder
from market.serializers import MarketMenuGroupSerializer, MarketOrderSerializer


class MarketMenuView(ListAPIView):

    pagination_class = None
    queryset = MarketGroup.objects.filter(parent_group_id=None).has_children().order_by('name')
    serializer_class = MarketMenuGroupSerializer

    @cache_response(60 * 60 * 24)
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class MarketOrdersView(ListAPIView):

    filterset_class = MarketOrderFilter
    filter_backends = (DjangoFilterBackend, OrderingFilter, )
    ordering = ('price',)
    ordering_fields = ('expires_date', 'price', 'region', 'system', 'station', 'volume_remain', )
    serializer_class = MarketOrderSerializer

    def get_queryset(self):
        return MarketOrder.objects.filter(removed__isnull=True).prefetch_related('region', 'station', 'system')
