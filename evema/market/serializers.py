from rest_framework import serializers

from market.models import MarketGroup, MarketOrder


class MarketMenuGroupSerializer(serializers.ModelSerializer):

    groups = serializers.SerializerMethodField(source='market_groups')
    items = serializers.SerializerMethodField(source='item_types')

    class Meta:
        model = MarketGroup
        fields = ('id', 'groups', 'items', 'name', 'parent_group_id')

    @staticmethod
    def get_groups(instance):
        serializer = MarketMenuGroupSerializer(instance.market_groups.has_children().order_by('name'), many=True)
        return serializer.data

    @staticmethod
    def get_items(instance):
        return instance.item_types.filter(published=True).order_by('name').values('id', 'name')


class MarketOrderSerializer(serializers.ModelSerializer):

    character = serializers.SerializerMethodField()
    corporation = serializers.SerializerMethodField()
    region_name = serializers.CharField(source='station.region.name')
    station_name = serializers.CharField(source='station.name')
    structure = serializers.BooleanField(source='station.structure')
    system_name = serializers.CharField(source='system.name')

    class Meta:
        model = MarketOrder
        exclude = ('removed', )

    def get_user_character(self):

        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        if user is None or not user.is_authenticated:
            return None

        return user.character

    def get_character(self, obj):

        character = self.get_user_character()
        if character and (character.id == obj.character_id or character.corporation_id == obj.corporation_id):
            return obj.character_id

        return None

    def get_corporation(self, obj):

        character = self.get_user_character()
        if character and character.corporation_id == obj.corporation_id:
            return obj.corporation_id

        return None
