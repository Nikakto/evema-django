from django.core.management.base import BaseCommand

from esiapi import harvest


class Command(BaseCommand):

    def handle(self, *args, **kwargs):

        count = harvest.orders.delete()

        self.stdout.write(f'Removed old orders: {count}')
