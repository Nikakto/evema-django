from django_filters.rest_framework import FilterSet, RangeFilter

from market.models import MarketOrder


class MarketOrderFilter(FilterSet):

    price = RangeFilter()
    volume_remain = RangeFilter()

    class Meta:
        model = MarketOrder
        exclude = ('character', 'corporation', )
        fields = '__all__'
