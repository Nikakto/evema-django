from django.utils import timezone
from rest_framework.test import APITestCase

from evema.utils.tests import character_create
from market.models import MarketOrder

ESI_API_ORDER_DATA_ISSUED = timezone.now().replace(microsecond=0)
ESI_API_ORDER_DATA_TIME_DELTA = 90

ESI_API_ORDER_DATA = {
    'duration': ESI_API_ORDER_DATA_TIME_DELTA,
    'is_buy_order': False,
    'issued': ESI_API_ORDER_DATA_ISSUED.strftime('%Y-%m-%dT%H:%M:%SZ'),
    'location_id': 60001801,
    'min_volume': 1,
    'order_id': 5233442817,
    'price': 8000.0,
    'range': 'region',
    'system_id': 30000162,
    'type_id': 251,
    'volume_remain': 4994,
    'volume_total': 5000
}


class TestMarketOrderParseEsiApi(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.esiapi_market_order_data = dict(ESI_API_ORDER_DATA)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_parse_esiapi__market_order__all(self):

        order_result = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        self.assertEqual(order_result.id, self.esiapi_market_order_data['order_id'])

        self.assertEqual(order_result.duration, self.esiapi_market_order_data['duration'])
        self.assertEqual(order_result.item_type_id, self.esiapi_market_order_data['type_id'])
        self.assertEqual(order_result.is_buy_order, self.esiapi_market_order_data['is_buy_order'])
        self.assertEqual(order_result.issued, ESI_API_ORDER_DATA_ISSUED)
        self.assertEqual(order_result.min_volume, self.esiapi_market_order_data['min_volume'])
        self.assertAlmostEqual(float(order_result.price), self.esiapi_market_order_data['price'], 2)
        self.assertEqual(order_result.range, self.esiapi_market_order_data['range'])
        self.assertEqual(order_result.station_id, self.esiapi_market_order_data['location_id'])
        self.assertEqual(order_result.system_id, self.esiapi_market_order_data['system_id'])
        self.assertEqual(order_result.volume_remain, self.esiapi_market_order_data['volume_remain'])
        self.assertEqual(order_result.volume_total, self.esiapi_market_order_data['volume_total'])

        self.assertEqual(order_result.expires_date,
                         ESI_API_ORDER_DATA_ISSUED + timezone.timedelta(days=ESI_API_ORDER_DATA_TIME_DELTA))

        self.assertIsNone(order_result.character_id)
        self.assertIsNone(order_result.corporation_id)
        self.assertIsNone(order_result.escrow)

    def test_parse_esiapi__escrow__escrow(self):

        self.esiapi_market_order_data['escrow'] = 100

        order_result = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        self.assertEqual(order_result.escrow, self.esiapi_market_order_data['escrow'])

    def test_parse_esiapi__issued_by__issued_by(self):

        self.esiapi_market_order_data['issued_by'] = 1

        order_result = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        self.assertEqual(order_result.character_id, self.esiapi_market_order_data['issued_by'])

    def test_parse_esiapi__region_id_missed__system_id_and_region_id_is_None(self):

        if 'region_id' in self.esiapi_market_order_data:
            del self.esiapi_market_order_data['region_id']

        self.esiapi_market_order_data['system_id'] = 100

        order_result = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        self.assertEqual(order_result.system_id, self.esiapi_market_order_data['system_id'])
        self.assertIsNone(order_result.region_id)

    def test_parse_esiapi__system_id_missed__region_id_parsed_system_id_is_None(self):

        if 'system_id' in self.esiapi_market_order_data:
            del self.esiapi_market_order_data['system_id']

        self.esiapi_market_order_data['region_id'] = 123

        order_result = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        self.assertEqual(order_result.region_id, self.esiapi_market_order_data['region_id'])
        self.assertIsNone(order_result.system_id)
