from django.utils import timezone
from rest_framework.test import APITestCase

from evema.utils.tests import character_create, corporation_create
from market.models import MarketOrder

ESI_API_ORDER_DATA_ISSUED = timezone.now().replace(microsecond=0)
ESI_API_ORDER_DATA_TIME_DELTA = 90

ESI_API_ORDER_DATA = {
    'duration': ESI_API_ORDER_DATA_TIME_DELTA,
    'is_buy_order': False,
    'issued': ESI_API_ORDER_DATA_ISSUED.strftime('%Y-%m-%dT%H:%M:%SZ'),
    'location_id': 60001801,
    'min_volume': 1,
    'order_id': 5233442817,
    'price': 8000.0,
    'range': 'region',
    'system_id': 30000162,
    'type_id': 251,
    'volume_remain': 4994,
    'volume_total': 5000
}


class TestMarketOrderParseEsiApi(APITestCase):

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        self.esiapi_market_order_data = dict(ESI_API_ORDER_DATA)

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_parse_esiapi__equals__False(self):

        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertFalse(updated)

    def test_parse_esiapi__update_id__False(self):

        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        self.esiapi_market_order_data['order_id'] = market_order.id + 1

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertFalse(updated)

    def test_parse_esiapi__update_id_and_issued___False(self):

        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        self.esiapi_market_order_data['order_id'] = market_order.id + 1
        self.esiapi_market_order_data['issued'] = timezone.now().replace(microsecond=0).strftime('%Y-%m-%dT%H:%M:%SZ')

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertFalse(updated)

    def test_parse_esiapi__update_escrow___True(self):

        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        self.esiapi_market_order_data['escrow'] = self.esiapi_market_order_data.get('escrow', 100) + 1

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertTrue(updated)

    def test_parse_esiapi__update_issued__True(self):

        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        issued_expected = timezone.now().replace(microsecond=0)
        self.esiapi_market_order_data['issued'] = issued_expected.strftime('%Y-%m-%dT%H:%M:%SZ')

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertTrue(updated)
        self.assertEqual(market_order.issued, issued_expected)

    def test_parse_esiapi__update_issued_by__True(self):

        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        character_id_expected = 12131564
        self.esiapi_market_order_data['issued_by'] = character_id_expected

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertTrue(updated)
        self.assertEqual(market_order.character_id, character_id_expected)

    def test_parse_esiapi__update_price__True(self):

        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        price_expected = market_order.price + 1
        self.esiapi_market_order_data['price'] = price_expected

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertTrue(updated)
        self.assertEqual(market_order.price, price_expected)

    def test_parse_esiapi__update_volume_remain__True(self):

        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        volume_remain = market_order.volume_remain + 1
        self.esiapi_market_order_data['volume_remain'] = volume_remain

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertTrue(updated)
        self.assertEqual(market_order.volume_remain, volume_remain)

    def test_parse_esiapi__character_None_to_Character__True(self):

        expected_character_id = 134234

        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)

        self.esiapi_market_order_data['issued_by'] = expected_character_id

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertTrue(updated)
        self.assertEqual(market_order.character_id, expected_character_id)

    def test_parse_esiapi__character_Character_to_None__False(self):

        user, token, character, character_token = character_create()
        market_order = MarketOrder.esiapi_parse(self.esiapi_market_order_data)
        market_order.character_id = character.id

        updated = market_order.esiapi_update(self.esiapi_market_order_data)

        self.assertFalse(updated)
        self.assertIsNotNone(market_order.character_id)
