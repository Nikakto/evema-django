from django.db.models import Count
from rest_framework.test import APITestCase

from market.models import MarketGroup


class TestMarketGroupHasChildren(APITestCase):

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',
        'market_orders',
    ]

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_market_group_has_children__has_group_only_child_groups__has_groups_without_items_but_with_groups(self):

        groups_ids = MarketGroup.objects.\
            annotate(item_types_count=Count('item_types')).\
            filter(parent_group_id=None, item_types_count__gt=0).\
            values_list('id', flat=True)

        result = MarketGroup.objects.filter(parent_group_id=None).has_children().values_list('id', flat=True)
        self.assertTrue(result)

        msg = 'result should contain groups without items but with child groups'
        self.assertGreaterEqual(set(result), set(groups_ids), msg=msg)

    # TODO fix it (may be by serializer ?)
    # def test_market_group_has_children__no_empty_child__has_not_child_groups_without_child(self):
    #
    #     root_group = MarketGroup.objects.filter(parent_group_id=None).has_children().first()
    #
    #     empty_child_group = MarketGroup(description='empty child root', name='empty_child_root', parent_group=root_group)
    #     empty_child_group.save()
    #
    #     empty_child_group_for_child = MarketGroup(description='empty child', name='empty_child', parent_group=empty_child_group)
    #     empty_child_group_for_child.save()
    #
    #     result = MarketGroup.objects.filter(parent_group_id=root_group.id).has_children().values_list('id', flat=True)
    #     self.assertTrue(result)
    #
    #     self.assertNotIn(empty_child_group.id, result, msg='Should not contains groups that children not has items')

    def test_market_group_has_children__has_not_empty_groups__no_empty_groups(self):

        def _test_node(node_to_check):
            self.assertTrue(node_to_check.market_groups.all() or node_to_check.item_types.all())
            [_test_node(node) for node in node_to_check.market_groups.all()]

        result = MarketGroup.objects.filter(parent_group_id=None).has_children()
        self.assertTrue(result)

        for node in result:
            _test_node(node)
