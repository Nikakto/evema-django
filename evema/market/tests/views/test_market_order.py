from django.urls import reverse
from django.utils import dateparse, timezone
from rest_framework import status
from rest_framework.test import APITestCase

import datetime

from evema.utils.pagination import MainPagination
from evema.utils.tests import character_create, character_remove, corporation_create
from market.models import MarketOrder


class TestMarketOrders(APITestCase):

    url = reverse('market:orders')

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',
        'market_orders',
    ]

    def setUp(self):

        # in class django connected real database.
        self.pages_count = MarketOrder.objects.count() // MainPagination.page_size + 1

    def tearDown(self):
        MarketOrder.objects.update(removed=None)

    def test_market_orders_list__get_authenticated_character_field__character_id(self):

        user, token, character, character_token = character_create()
        self.client.force_authenticate(user=user, token=token)

        market_order = MarketOrder.objects.first()
        market_order.character = character
        market_order.save()

        MarketOrder.objects.exclude(id=market_order.id).update(removed=timezone.now())

        response = self.client.get(self.url)
        market_order_answer = response.json()[0]
        market_order.refresh_from_db()

        self.assertEqual(market_order_answer['id'], market_order.id)
        self.assertEqual(market_order.character, character)
        self.assertEqual(market_order_answer['character'], character.id)

        character_remove(user, token, character, character_token)

    def test_market_orders_list__get_authenticated_character_field_not_this_character__None(self):

        user, token, character, character_token = character_create()
        self.client.force_authenticate(user=user, token=token)

        user_other, token_other, character_other, character_token_other = character_create(username='test_username_2')
        market_order = MarketOrder.objects.first()
        market_order.character = character_other
        market_order.save()

        MarketOrder.objects.exclude(id=market_order.id).update(removed=timezone.now())

        response = self.client.get(self.url)
        market_order_answer = response.json()[0]
        market_order.refresh_from_db()

        self.assertEqual(market_order_answer['id'], market_order.id)
        self.assertEqual(market_order.character, character_other)
        self.assertIsNone(market_order_answer['character'])

        character_remove(user, token, character, character_token)
        character_remove(user_other, token_other, character_other, character_token_other)

    def test_market_orders_list__get_authenticated_corporation_field__corporation_id(self):

        corporation = corporation_create()

        user, token, character, character_token = character_create()
        character.corporation = corporation
        character.save()

        self.client.force_authenticate(user=user, token=token)

        market_order = MarketOrder.objects.first()
        market_order.corporation = corporation
        market_order.save()

        MarketOrder.objects.exclude(id=market_order.id).update(removed=timezone.now())

        response = self.client.get(self.url)
        market_order_answer = response.json()[0]
        market_order.refresh_from_db()

        self.assertEqual(market_order_answer['id'], market_order.id)
        self.assertEqual(market_order.corporation, corporation)
        self.assertEqual(market_order_answer['corporation'], corporation.id)

        character_remove(user, token, character, character_token)

    def test_market_orders_list__get_authenticated_corporation_field_not_this_corporation__None(self):

        corporation = corporation_create()

        user, token, character, character_token = character_create()
        character.corporation_id = corporation.id + 1
        character.save()

        self.client.force_authenticate(user=user, token=token)

        market_order = MarketOrder.objects.first()
        market_order.corporation = corporation
        market_order.save()

        MarketOrder.objects.exclude(id=market_order.id).update(removed=timezone.now())

        response = self.client.get(self.url)
        market_order_answer = response.json()[0]
        market_order.refresh_from_db()

        self.assertEqual(market_order_answer['id'], market_order.id)
        self.assertEqual(market_order.corporation, corporation)
        self.assertIsNone(market_order_answer['corporation'])

        character_remove(user, token, character, character_token)

    def test_market_orders_list__get_default__list(self):

        response = self.client.get(self.url)

        # check answer item
        market_order_answer = response.json()[0]
        market_order_real = MarketOrder.objects.get(id=market_order_answer['id'])

        self.assertEqual(market_order_answer['duration'], market_order_real.duration)
        self.assertEqual(market_order_answer['is_buy_order'], market_order_real.is_buy_order)
        self.assertEqual(market_order_answer['item_type'], market_order_real.item_type_id)
        self.assertEqual(market_order_answer['min_volume'], market_order_real.min_volume)
        self.assertEqual(market_order_answer['region'], market_order_real.system.region_id)
        self.assertEqual(market_order_answer['station'], market_order_real.station_id)
        self.assertEqual(market_order_answer['structure'], market_order_real.station.structure)
        self.assertEqual(market_order_answer['system'], market_order_real.system_id)
        self.assertEqual(market_order_answer['volume_total'], market_order_real.volume_total)
        self.assertEqual(market_order_answer['volume_remain'], market_order_real.volume_remain)

        self.assertAlmostEqual(float(market_order_answer['price']), market_order_real.price, 2)

        self.assertEqual(market_order_answer['region_name'], market_order_real.station.region.name)
        self.assertEqual(market_order_answer['station_name'], market_order_real.station.name)
        self.assertEqual(market_order_answer['system_name'], market_order_real.system.name)

        self.assertEqual(dateparse.parse_datetime(market_order_answer['expires_date']),
                         market_order_real.issued + datetime.timedelta(days=market_order_real.duration))

        # check ids
        ids_answer = [item_type['id'] for item_type in response.data]
        self.assertEqual(len(set(ids_answer)), MainPagination.page_size)

        # check default sort by price
        self.assertListEqual(response.data, sorted(response.data, key=lambda order: float(order['price'])))

        # check pagination
        self.assertEqual(response['x-page-current'], '1')
        self.assertEqual(response['x-pages'], str(self.pages_count))

    def test_market_orders_list__get_default_character_filed__character_is_None(self):

        user, token, character, character_token = character_create()

        market_order = MarketOrder.objects.first()
        market_order.character = character
        market_order.save()

        MarketOrder.objects.exclude(id=market_order.id).update(removed=timezone.now())

        response = self.client.get(self.url)
        market_order_answer = response.json()[0]
        market_order.refresh_from_db()

        self.assertEqual(market_order_answer['id'], market_order.id)
        self.assertEqual(market_order.character, character)
        self.assertIsNone(market_order_answer['character'])

        character_remove(user, token, character, character_token)

    def test_market_orders_list__get_default_corporation_field__corporation_is_None(self):

        corporation = corporation_create()

        market_order = MarketOrder.objects.first()
        market_order.corporation = corporation
        market_order.save()

        MarketOrder.objects.exclude(id=market_order.id).update(removed=timezone.now())

        response = self.client.get(self.url)
        market_order_answer = response.json()[0]
        market_order.refresh_from_db()

        self.assertEqual(market_order_answer['id'], market_order.id)
        self.assertEqual(market_order.corporation, corporation)
        self.assertIsNone(market_order_answer['corporation'])

    def test_market_orders_list__get_default_removed__not_returned(self):

        response = self.client.get(self.url)
        result = response.json()

        self.assertNotIn('removed', result[0])

    def test_market_orders_list__get_page_last__list_page_last(self):

        expected_orders_count = MarketOrder.objects.count() % MainPagination.page_size

        data = {'page': self.pages_count}
        response = self.client.get(self.url, data)
        result = response.json()

        # check pagination items is last
        self.assertEqual(result[-1]['id'], MarketOrder.objects.order_by('price').last().id)
        self.assertEquals(len(response.data), expected_orders_count)

        # check pagination
        self.assertEqual(response['x-page-current'], str(self.pages_count))
        self.assertEqual(response['x-pages'], str(self.pages_count))

    def test_market_orders__get_page_invalid__404(self):

        data = {'page': self.pages_count + 1}
        response = self.client.get(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_market_orders__filter_by_item_type__only_item_id(self):

        item_type_id = 1

        orders_count_expected = MarketOrder.objects.filter(item_type_id=item_type_id).count()

        data = {'item_type': item_type_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.json())
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_market_orders__filter_by_order_is_buy__only_buy_orders(self):

        orders_count_expected = MarketOrder.objects.filter(is_buy_order=True).count()

        data = {'is_buy_order': True}
        response = self.client.get(self.url, data)

        self.assertTrue(response.json())
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_market_orders__filter_by_order_is_not_buy__only_sell_orders(self):

        orders_count_expected = MarketOrder.objects.filter(is_buy_order=False).count()

        data = {'is_buy_order': False}
        response = self.client.get(self.url, data)

        self.assertTrue(response.json())
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_market_orders__filter_by_character__not_filtered(self):

        order = MarketOrder.objects.first()
        order.character_id = 1
        order.save()

        data = {'character': order.character_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.json())
        self.assertEqual(response['X-Data-Count'], str(MarketOrder.objects.count()))

    def test_market_orders__filter_by_corporation__not_filtered(self):

        order = MarketOrder.objects.first()
        order.corporation_id = 1
        order.save()

        data = {'corporation': order.corporation_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.json())
        self.assertEqual(response['X-Data-Count'], str(MarketOrder.objects.count()))

    def test_market_orders__filter_by_region__only_region_id(self):

        region_id = 1

        orders_count_expected = MarketOrder.objects.filter(system__region__id=region_id).count()

        data = {'region': region_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.json())
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_market_orders__filter_by_station__only_station_id(self):

        station_id = 1

        orders_count_expected = MarketOrder.objects.filter(station_id=station_id).count()

        data = {'station': station_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.json())
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_market_orders__filter_by_system__only_system_id(self):

        system_id = 1

        orders_count_expected = MarketOrder.objects.filter(system_id=system_id).count()

        data = {'system': system_id}
        response = self.client.get(self.url, data)

        self.assertTrue(response.json())
        self.assertEqual(response['X-Data-Count'], str(orders_count_expected))

    def test_market_orders__ordering_ascending_volume_remain__ascending(self):

        data = {'ordering': 'volume_remain'}
        response = self.client.get(self.url, data)
        result = response.json()

        self.assertTrue(result)
        self.assertListEqual(result, sorted(result, key=lambda item: item['volume_remain']))

    def test_market_orders__ordering_descending_volume_remain__descending(self):

        data = {'ordering': '-volume_remain'}
        response = self.client.get(self.url, data)
        result = response.json()

        self.assertTrue(result)
        self.assertListEqual(result, sorted(response.data, key=lambda item: item['volume_remain'], reverse=True))

    def test_market_orders__ordering_character__ordering_default_price(self):

        orders_ids = MarketOrder.objects.values_list('id', flat=True)[::10]
        MarketOrder.objects.filter(id__in=orders_ids).update(character_id=1)

        data = {'ordering': 'character'}
        response = self.client.get(self.url, data)
        result = response.json()

        self.assertTrue(result)
        self.assertListEqual(result, sorted(result, key=lambda item: float(item['price'])))

    def test_market_orders__ordering_corporation__ordering_default_price(self):

        orders_ids = MarketOrder.objects.values_list('id', flat=True)[::10]
        MarketOrder.objects.filter(id__in=orders_ids).update(corporation_id=1)

        data = {'ordering': 'corporation'}
        response = self.client.get(self.url, data)
        result = response.json()

        self.assertTrue(result)
        self.assertListEqual(result, sorted(result, key=lambda item: float(item['price'])))

    def test_market_orders__ordering_region__descending(self):
        # descending is important. All orders default sorted by all foreign keys (generated fixtures)

        data = {'ordering': '-region'}
        response = self.client.get(self.url, data)
        result = response.json()

        self.assertTrue(result)
        self.assertListEqual(result, sorted(result, key=lambda item: item['region'], reverse=True))

    def test_market_orders__removed_true__empty(self):

        MarketOrder.objects.update(removed=timezone.now())

        data = {'ordering': '-region'}
        response = self.client.get(self.url, data)
        result = response.json()

        self.assertFalse(result)
        self.assertIsInstance(result, list)
