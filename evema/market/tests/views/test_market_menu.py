from django.core.cache import cache
from django.db.models import Count
from django.urls import reverse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.test import APITestCase

from unittest import mock

from assets.models import ItemType
from market.models import MarketGroup


class TestMarketMenu(APITestCase):

    url = reverse('market:menu')

    fixtures = [

        'regions',
        'constellations',
        'systems',

        'market_groups',

        'item_categories',
        'item_groups',
        'item_types',

        'stations',
        'market_orders',
    ]

    # ==================================================================================================================
    # SET UP AND TEARDOWN
    # ==================================================================================================================

    def setUp(self):
        cache.clear()

    # ==================================================================================================================
    # TESTS
    # ==================================================================================================================

    def test_market_menu__get_menu_no_cache__full_menu(self):
        """
        Recursive check all nodes for menu.
        I am really very lazy to check each case. So, wrote this test as main. Create new only for bug.
        """

        def _test_node(node_check):

            # check root node data
            node_db = MarketGroup.objects.get(id=node_check['id'])

            self.assertEqual(node_check['name'], node_db.name)
            self.assertEqual(node_check['parent_group_id'], node_db.parent_group_id)

            # check nodes exist
            node_check_market_groups = [group['id'] for group in node_check['groups']]
            child_nodes_db = MarketGroup.objects.filter(id__in=node_check_market_groups).has_children().values_list('id', flat=True)
            self.assertListEqual(sorted(node_check_market_groups), sorted(child_nodes_db))

            # check child item_types
            node_check_item_types = [item['id'] for item in node_check['items']]
            item_db_data = ItemType.objects.filter(id__in=node_check_item_types, published=True).order_by('id').values('id', 'name')
            self.assertListEqual(sorted(node_check['items'], key=lambda el: el['id']), list(item_db_data))

            # check child market_groups
            if not node_check['groups']:
                self.assertFalse(MarketGroup.objects.has_children().filter(parent_group_id=node_check['id']))

            for node_child in node_check['groups']:
                _test_node(node_child)

        response = self.client.get(self.url)
        result = response.json()

        self.assertTrue(result)

        for node in result:
            _test_node(node)

    @mock.patch('market.views.ListAPIView.list')
    def test_market_menu__get_menu_cache__cache_data(self, mock_list):

        expected_result, unexpected_result = Response('cache is working'), Response('cache is not working')
        mock_list.side_effect = [expected_result, unexpected_result]

        response_no_cache = self.client.get(self.url)
        response_cached = self.client.get(self.url)

        self.assertEqual(response_no_cache.json(), expected_result.json())
        self.assertEqual(response_no_cache.json(), response_cached.json())

    @mock.patch('market.views.ListAPIView.list')
    def test_market_menu__get_menu_error__second_time_is_not_error(self, mock_list):

        error = Response(data='error', status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        expected_result = Response('error ignored')
        mock_list.side_effect = [error, expected_result]

        response_error = self.client.get(self.url)
        response_normal = self.client.get(self.url)

        self.assertEqual(response_error.status_code, error.status_code)
        self.assertEqual(response_normal.json(), expected_result.json())

    def test_market_menu__get_menu_has_not_empty_groups__no_empty_groups(self):

        def _test_node(node_to_check):

            self.assertTrue(node_to_check['groups'] or node_to_check['items'], msg='node should has items or groups')

            if node_to_check['groups']:
                for node in node_to_check['groups']:
                    _test_node(node)

        response = self.client.get(self.url)
        result = response.json()

        self.assertTrue(result)

        for node in result:
            _test_node(node)

    def test_market_menu__get_menu_has_not_unpublished_items__no_unpublished_items(self):

        def _test_node(node_to_check):

            items_id = [item['id'] for item in node_to_check['items']]
            self.assertEqual(len(items_id), ItemType.objects.filter(id__in=items_id, published=True).count())

            if node_to_check['groups']:
                for node in node_to_check['groups']:
                    _test_node(node)

        response = self.client.get(self.url)
        result = response.json()

        self.assertTrue(result)

        for node in result:
            _test_node(node)

    def test_market_menu__get_menu_has_group_only_child_groups__has_groups_without_items_but_with_groups(self):

        groups_ids = MarketGroup.objects.\
            annotate(item_types_count=Count('item_types')).\
            filter(parent_group_id=None, item_types_count__gt=0).\
            values_list('id', flat=True)

        response = self.client.get(self.url)
        result = response.json()

        msg = 'result should contain groups without items but with child groups'
        self.assertGreaterEqual(set(group['id'] for group in result), set(groups_ids), msg=msg)
