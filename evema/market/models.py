from django.db import models
from django.db.models import Count, Q
from django.utils import dateparse, timezone


class MarketGroup(models.Model):

    class MarketGroupManager(models.QuerySet):

        def has_children(self):
            queryset = self.annotate(item_types_count=Count('item_types'), market_groups_count=Count('market_groups'))
            queryset = queryset.filter(Q(item_types_count__gt=0) | Q(market_groups_count__gt=0))
            return queryset

    id = models.BigAutoField(primary_key=True)

    name = models.CharField(max_length=64)
    description = models.TextField()
    parent_group = models.ForeignKey('MarketGroup', db_constraint=False, null=True, on_delete=models.DO_NOTHING, related_name='market_groups')

    objects = MarketGroupManager.as_manager()


class MarketOrder(models.Model):

    id = models.BigAutoField(primary_key=True)

    character = models.ForeignKey('character.Character', db_constraint=False, null=True, on_delete=models.SET_NULL, related_name='orders')
    corporation = models.ForeignKey('corporation.Corporation', db_constraint=False, null=True, on_delete=models.CASCADE, related_name='orders')
    duration = models.IntegerField()
    expires_date = models.DateTimeField(default=timezone.now)
    escrow = models.DecimalField(decimal_places=2, max_digits=32, null=True)
    is_buy_order = models.BooleanField(default=False)
    issued = models.DateTimeField(default=timezone.now)
    item_type = models.ForeignKey('assets.ItemType', on_delete=models.CASCADE, related_name='orders')
    min_volume = models.IntegerField()
    price = models.DecimalField(decimal_places=2, max_digits=32)
    public = models.BooleanField(default=False)
    range = models.CharField(max_length=16)
    region = models.ForeignKey('universe.Region', on_delete=models.CASCADE, related_name='orders')
    removed = models.DateTimeField(null=True)
    station = models.ForeignKey('universe.Station', on_delete=models.CASCADE, related_name='orders')
    system = models.ForeignKey('universe.System', on_delete=models.CASCADE, related_name='orders')
    updated_at = models.DateTimeField(auto_now=True)
    volume_remain = models.IntegerField()
    volume_total = models.IntegerField()

    @staticmethod
    def esiapi_parse(order_data, public=False):
        """
        Serializer is too slow for parsing huge list of orders
        :param order_data: data about order from esi api
        :param public: order can show to all users
        :return MarketOrder instance
        """

        order = MarketOrder()
        order.id = order_data['order_id']

        order.duration = order_data['duration']
        order.is_buy_order = order_data['is_buy_order']
        order.item_type_id = order_data['type_id']
        order.min_volume = order_data['min_volume']
        order.price = float(order_data['price'])
        order.range = order_data['range']
        order.station_id = order_data['location_id']
        order.volume_remain = order_data['volume_remain']
        order.volume_total = order_data['volume_total']

        order.issued = dateparse.parse_datetime(order_data['issued']).replace(microsecond=0)
        order.expires_date = order.issued + timezone.timedelta(int(order_data['duration']))

        # nullable

        order.character_id = order_data.get('issued_by')
        order.escrow = order_data.get('escrow')
        order.region_id = order_data.get('region_id')  # character order contain it
        order.system_id = order_data.get('system_id')  # market order contain it

        order.public = public

        return order

    def esiapi_update(self, order_data):

        if self.id != order_data['order_id']:
            return False

        _before = (float(self.price), self.character_id, self.escrow, self.volume_remain, self.issued, self.public)

        self.character_id = order_data.get('issued_by', self.character_id)
        self.escrow = order_data.get('escrow', self.escrow)
        self.price = order_data['price']
        self.volume_remain = order_data['volume_remain']

        self.issued = dateparse.parse_datetime(order_data['issued']).replace(microsecond=0)
        self.expires_date = self.issued + timezone.timedelta(days=int(order_data['duration']))

        _after = (float(self.price), self.character_id, self.escrow, self.volume_remain, self.issued, self.public)

        return _before != _after
