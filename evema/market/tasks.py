import logging
from celery import group, shared_task

import esiapi
from character.models import Character
from corporation.models import Corporation
from esiapi import harvest
from evema.celery import app
from universe.models import Region

logger = logging.getLogger(__name__)


@shared_task(time_limit=60)
def market_orders_clean():
    return esiapi.harvest.orders.delete()


# ======================================================================================================================
# LOAD CHARACTER'S ORDERS
# ======================================================================================================================


@shared_task(default_retry_delay=60 * 5, time_limit=30)
def market_orders_character(character_id):

    result = harvest.orders.harvest_character(character_id=character_id)
    if result and 'error' in result:
        raise ConnectionError(f'ESI API: Can\'t get orders for character {character_id}')

    return result


@app.task(max_retries=0)
def market_orders_characters():

    characters_ids = Character.objects.\
        filter(token__expired=False, token__scopes__contains=['esi-markets.read_character_orders.v1']).\
        values_list('id', flat=True)

    market_orders_tasks = group(market_orders_character.s(character_id) for character_id in characters_ids)
    market_orders_tasks.delay()

    return list(characters_ids)


# ======================================================================================================================
# LOAD CORPORATION'S ORDERS
# ======================================================================================================================


@shared_task(default_retry_delay=60 * 5, time_limit=30)
def market_orders_corporation(corporation_id):

    result = harvest.orders.harvest_corporation(corporation_id=corporation_id)
    if result and 'error' in result:
        raise ConnectionError(f'ESI API: Can\'t get orders for corporation {corporation_id}')

    return result


@app.task(max_retries=0)
def market_orders_corporations():

    corporations_ids = Corporation.objects.values_list('id', flat=True)

    market_orders_tasks = group(market_orders_corporation.s(corporation_id) for corporation_id in corporations_ids)
    market_orders_tasks.delay()

    return list(corporations_ids)


# ======================================================================================================================
# LOAD PUBLIC ORDERS
# ======================================================================================================================


@app.task(max_retries=0)
def market_orders_regions():

    regions_ids = Region.objects.values_list('id', flat=True)

    market_orders_tasks = group(market_orders_region.s(region_id) for region_id in regions_ids)
    market_orders_tasks.delay()

    return list(regions_ids)


@shared_task(default_retry_delay=60, time_limit=300)
def market_orders_region(region_id):

    result, headers = esiapi.market.orders(region_id, page=1)

    if 'error' in result:
        raise ConnectionError(f'ESI API: Can\'t get orders for region {region_id}')

    harvest.orders.harvest_market(region_id=region_id)
