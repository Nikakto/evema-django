#!/bin/bash

sudo docker exec -t postgres pg_dump -U evema -T market_marketorder evema_django > /home/nikakto/dumps/dump_`date +%Y_%m_%d_%H_%M_%S`.tar.gz
sudo docker stop evema_django evema_vue celery
sudo docker rm evema_django evema_vue celery
sudo docker rmi nikakto/evema-django:latest nikakto/evema-vue:latest
sudo docker-compose up --timeout 120 -d